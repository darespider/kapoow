//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ThumbnailReadListDto {
  /// Returns a new [ThumbnailReadListDto] instance.
  ThumbnailReadListDto({
    required this.id,
    required this.readListId,
    required this.type,
    required this.selected,
  });

  String id;

  String readListId;

  String type;

  bool selected;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ThumbnailReadListDto &&
          other.id == id &&
          other.readListId == readListId &&
          other.type == type &&
          other.selected == selected;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (readListId.hashCode) +
      (type.hashCode) +
      (selected.hashCode);

  @override
  String toString() =>
      'ThumbnailReadListDto[id=$id, readListId=$readListId, type=$type, selected=$selected]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'readListId'] = readListId;
    _json[r'type'] = type;
    _json[r'selected'] = selected;
    return _json;
  }

  /// Returns a new [ThumbnailReadListDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ThumbnailReadListDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ThumbnailReadListDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ThumbnailReadListDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ThumbnailReadListDto(
        id: mapValueOfType<String>(json, r'id')!,
        readListId: mapValueOfType<String>(json, r'readListId')!,
        type: mapValueOfType<String>(json, r'type')!,
        selected: mapValueOfType<bool>(json, r'selected')!,
      );
    }
    return null;
  }

  static List<ThumbnailReadListDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ThumbnailReadListDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ThumbnailReadListDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ThumbnailReadListDto> mapFromJson(dynamic json) {
    final map = <String, ThumbnailReadListDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ThumbnailReadListDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ThumbnailReadListDto-objects as value to a dart map
  static Map<String, List<ThumbnailReadListDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ThumbnailReadListDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ThumbnailReadListDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'readListId',
    'type',
    'selected',
  };
}
