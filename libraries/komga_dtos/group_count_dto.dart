//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class GroupCountDto {
  /// Returns a new [GroupCountDto] instance.
  GroupCountDto({
    required this.group,
    required this.count,
  });

  String group;

  int count;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GroupCountDto && other.group == group && other.count == count;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (group.hashCode) + (count.hashCode);

  @override
  String toString() => 'GroupCountDto[group=$group, count=$count]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'group'] = group;
    _json[r'count'] = count;
    return _json;
  }

  /// Returns a new [GroupCountDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static GroupCountDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "GroupCountDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "GroupCountDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return GroupCountDto(
        group: mapValueOfType<String>(json, r'group')!,
        count: mapValueOfType<int>(json, r'count')!,
      );
    }
    return null;
  }

  static List<GroupCountDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <GroupCountDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = GroupCountDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, GroupCountDto> mapFromJson(dynamic json) {
    final map = <String, GroupCountDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = GroupCountDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of GroupCountDto-objects as value to a dart map
  static Map<String, List<GroupCountDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<GroupCountDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = GroupCountDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'group',
    'count',
  };
}
