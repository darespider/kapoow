//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class BookDto {
  /// Returns a new [BookDto] instance.
  BookDto({
    required this.id,
    required this.seriesId,
    required this.seriesTitle,
    required this.libraryId,
    required this.name,
    required this.url,
    required this.number,
    required this.created,
    required this.lastModified,
    required this.fileLastModified,
    required this.sizeBytes,
    required this.size,
    required this.media,
    required this.metadata,
    this.readProgress,
    required this.deleted,
    required this.fileHash,
  });

  String id;

  String seriesId;

  String seriesTitle;

  String libraryId;

  String name;

  String url;

  int number;

  DateTime created;

  DateTime lastModified;

  DateTime fileLastModified;

  int sizeBytes;

  String size;

  MediaDto media;

  BookMetadataDto metadata;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  ReadProgressDto? readProgress;

  bool deleted;

  String fileHash;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookDto &&
          other.id == id &&
          other.seriesId == seriesId &&
          other.seriesTitle == seriesTitle &&
          other.libraryId == libraryId &&
          other.name == name &&
          other.url == url &&
          other.number == number &&
          other.created == created &&
          other.lastModified == lastModified &&
          other.fileLastModified == fileLastModified &&
          other.sizeBytes == sizeBytes &&
          other.size == size &&
          other.media == media &&
          other.metadata == metadata &&
          other.readProgress == readProgress &&
          other.deleted == deleted &&
          other.fileHash == fileHash;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (seriesId.hashCode) +
      (seriesTitle.hashCode) +
      (libraryId.hashCode) +
      (name.hashCode) +
      (url.hashCode) +
      (number.hashCode) +
      (created.hashCode) +
      (lastModified.hashCode) +
      (fileLastModified.hashCode) +
      (sizeBytes.hashCode) +
      (size.hashCode) +
      (media.hashCode) +
      (metadata.hashCode) +
      (readProgress == null ? 0 : readProgress!.hashCode) +
      (deleted.hashCode) +
      (fileHash.hashCode);

  @override
  String toString() =>
      'BookDto[id=$id, seriesId=$seriesId, seriesTitle=$seriesTitle, libraryId=$libraryId, name=$name, url=$url, number=$number, created=$created, lastModified=$lastModified, fileLastModified=$fileLastModified, sizeBytes=$sizeBytes, size=$size, media=$media, metadata=$metadata, readProgress=$readProgress, deleted=$deleted, fileHash=$fileHash]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'seriesId'] = seriesId;
    _json[r'seriesTitle'] = seriesTitle;
    _json[r'libraryId'] = libraryId;
    _json[r'name'] = name;
    _json[r'url'] = url;
    _json[r'number'] = number;
    _json[r'created'] = created.toUtc().toIso8601String();
    _json[r'lastModified'] = lastModified.toUtc().toIso8601String();
    _json[r'fileLastModified'] = fileLastModified.toUtc().toIso8601String();
    _json[r'sizeBytes'] = sizeBytes;
    _json[r'size'] = size;
    _json[r'media'] = media;
    _json[r'metadata'] = metadata;
    if (readProgress != null) {
      _json[r'readProgress'] = readProgress;
    }
    _json[r'deleted'] = deleted;
    _json[r'fileHash'] = fileHash;
    return _json;
  }

  /// Returns a new [BookDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static BookDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "BookDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "BookDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return BookDto(
        id: mapValueOfType<String>(json, r'id')!,
        seriesId: mapValueOfType<String>(json, r'seriesId')!,
        seriesTitle: mapValueOfType<String>(json, r'seriesTitle')!,
        libraryId: mapValueOfType<String>(json, r'libraryId')!,
        name: mapValueOfType<String>(json, r'name')!,
        url: mapValueOfType<String>(json, r'url')!,
        number: mapValueOfType<int>(json, r'number')!,
        created: mapDateTime(json, r'created', '')!,
        lastModified: mapDateTime(json, r'lastModified', '')!,
        fileLastModified: mapDateTime(json, r'fileLastModified', '')!,
        sizeBytes: mapValueOfType<int>(json, r'sizeBytes')!,
        size: mapValueOfType<String>(json, r'size')!,
        media: MediaDto.fromJson(json[r'media'])!,
        metadata: BookMetadataDto.fromJson(json[r'metadata'])!,
        readProgress: ReadProgressDto.fromJson(json[r'readProgress']),
        deleted: mapValueOfType<bool>(json, r'deleted')!,
        fileHash: mapValueOfType<String>(json, r'fileHash')!,
      );
    }
    return null;
  }

  static List<BookDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <BookDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BookDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, BookDto> mapFromJson(dynamic json) {
    final map = <String, BookDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of BookDto-objects as value to a dart map
  static Map<String, List<BookDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<BookDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'seriesId',
    'seriesTitle',
    'libraryId',
    'name',
    'url',
    'number',
    'created',
    'lastModified',
    'fileLastModified',
    'sizeBytes',
    'size',
    'media',
    'metadata',
    'deleted',
    'fileHash',
  };
}
