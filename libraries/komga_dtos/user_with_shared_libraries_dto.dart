//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class UserWithSharedLibrariesDto {
  /// Returns a new [UserWithSharedLibrariesDto] instance.
  UserWithSharedLibrariesDto({
    required this.id,
    required this.email,
    this.roles = const [],
    required this.sharedAllLibraries,
    this.sharedLibraries = const [],
  });

  String id;

  String email;

  List<String> roles;

  bool sharedAllLibraries;

  List<SharedLibraryDto> sharedLibraries;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserWithSharedLibrariesDto &&
          other.id == id &&
          other.email == email &&
          other.roles == roles &&
          other.sharedAllLibraries == sharedAllLibraries &&
          other.sharedLibraries == sharedLibraries;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (email.hashCode) +
      (roles.hashCode) +
      (sharedAllLibraries.hashCode) +
      (sharedLibraries.hashCode);

  @override
  String toString() =>
      'UserWithSharedLibrariesDto[id=$id, email=$email, roles=$roles, sharedAllLibraries=$sharedAllLibraries, sharedLibraries=$sharedLibraries]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'email'] = email;
    _json[r'roles'] = roles;
    _json[r'sharedAllLibraries'] = sharedAllLibraries;
    _json[r'sharedLibraries'] = sharedLibraries;
    return _json;
  }

  /// Returns a new [UserWithSharedLibrariesDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static UserWithSharedLibrariesDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "UserWithSharedLibrariesDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "UserWithSharedLibrariesDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return UserWithSharedLibrariesDto(
        id: mapValueOfType<String>(json, r'id')!,
        email: mapValueOfType<String>(json, r'email')!,
        roles: json[r'roles'] is List
            ? (json[r'roles'] as List).cast<String>()
            : const [],
        sharedAllLibraries: mapValueOfType<bool>(json, r'sharedAllLibraries')!,
        sharedLibraries:
            SharedLibraryDto.listFromJson(json[r'sharedLibraries'])!,
      );
    }
    return null;
  }

  static List<UserWithSharedLibrariesDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <UserWithSharedLibrariesDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = UserWithSharedLibrariesDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, UserWithSharedLibrariesDto> mapFromJson(dynamic json) {
    final map = <String, UserWithSharedLibrariesDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = UserWithSharedLibrariesDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of UserWithSharedLibrariesDto-objects as value to a dart map
  static Map<String, List<UserWithSharedLibrariesDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<UserWithSharedLibrariesDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = UserWithSharedLibrariesDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'email',
    'roles',
    'sharedAllLibraries',
    'sharedLibraries',
  };
}
