//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class SeriesMetadataUpdateDto {
  /// Returns a new [SeriesMetadataUpdateDto] instance.
  SeriesMetadataUpdateDto({
    this.status,
    this.statusLock,
    this.title,
    this.titleLock,
    this.titleSort,
    this.titleSortLock,
    this.summary,
    this.summaryLock,
    this.publisher,
    this.publisherLock,
    this.readingDirectionLock,
    this.ageRatingLock,
    this.language,
    this.languageLock,
    this.genresLock,
    this.tagsLock,
    this.totalBookCountLock,
    this.sharingLabelsLock,
    this.tags = const {},
    this.readingDirection,
    this.ageRating,
    this.genres = const {},
    this.totalBookCount,
    this.sharingLabels = const {},
  });

  SeriesMetadataUpdateDtoStatusEnum? status;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? statusLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? title;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? titleLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? titleSort;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? titleSortLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? summary;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? summaryLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? publisher;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? publisherLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? readingDirectionLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? ageRatingLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? language;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? languageLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? genresLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? tagsLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? totalBookCountLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? sharingLabelsLock;

  Set<String> tags;

  SeriesMetadataUpdateDtoReadingDirectionEnum? readingDirection;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? ageRating;

  Set<String> genres;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? totalBookCount;

  Set<String> sharingLabels;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SeriesMetadataUpdateDto &&
          other.status == status &&
          other.statusLock == statusLock &&
          other.title == title &&
          other.titleLock == titleLock &&
          other.titleSort == titleSort &&
          other.titleSortLock == titleSortLock &&
          other.summary == summary &&
          other.summaryLock == summaryLock &&
          other.publisher == publisher &&
          other.publisherLock == publisherLock &&
          other.readingDirectionLock == readingDirectionLock &&
          other.ageRatingLock == ageRatingLock &&
          other.language == language &&
          other.languageLock == languageLock &&
          other.genresLock == genresLock &&
          other.tagsLock == tagsLock &&
          other.totalBookCountLock == totalBookCountLock &&
          other.sharingLabelsLock == sharingLabelsLock &&
          other.tags == tags &&
          other.readingDirection == readingDirection &&
          other.ageRating == ageRating &&
          other.genres == genres &&
          other.totalBookCount == totalBookCount &&
          other.sharingLabels == sharingLabels;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (status == null ? 0 : status!.hashCode) +
      (statusLock == null ? 0 : statusLock!.hashCode) +
      (title == null ? 0 : title!.hashCode) +
      (titleLock == null ? 0 : titleLock!.hashCode) +
      (titleSort == null ? 0 : titleSort!.hashCode) +
      (titleSortLock == null ? 0 : titleSortLock!.hashCode) +
      (summary == null ? 0 : summary!.hashCode) +
      (summaryLock == null ? 0 : summaryLock!.hashCode) +
      (publisher == null ? 0 : publisher!.hashCode) +
      (publisherLock == null ? 0 : publisherLock!.hashCode) +
      (readingDirectionLock == null ? 0 : readingDirectionLock!.hashCode) +
      (ageRatingLock == null ? 0 : ageRatingLock!.hashCode) +
      (language == null ? 0 : language!.hashCode) +
      (languageLock == null ? 0 : languageLock!.hashCode) +
      (genresLock == null ? 0 : genresLock!.hashCode) +
      (tagsLock == null ? 0 : tagsLock!.hashCode) +
      (totalBookCountLock == null ? 0 : totalBookCountLock!.hashCode) +
      (sharingLabelsLock == null ? 0 : sharingLabelsLock!.hashCode) +
      (tags.hashCode) +
      (readingDirection == null ? 0 : readingDirection!.hashCode) +
      (ageRating == null ? 0 : ageRating!.hashCode) +
      (genres.hashCode) +
      (totalBookCount == null ? 0 : totalBookCount!.hashCode) +
      (sharingLabels.hashCode);

  @override
  String toString() =>
      'SeriesMetadataUpdateDto[status=$status, statusLock=$statusLock, title=$title, titleLock=$titleLock, titleSort=$titleSort, titleSortLock=$titleSortLock, summary=$summary, summaryLock=$summaryLock, publisher=$publisher, publisherLock=$publisherLock, readingDirectionLock=$readingDirectionLock, ageRatingLock=$ageRatingLock, language=$language, languageLock=$languageLock, genresLock=$genresLock, tagsLock=$tagsLock, totalBookCountLock=$totalBookCountLock, sharingLabelsLock=$sharingLabelsLock, tags=$tags, readingDirection=$readingDirection, ageRating=$ageRating, genres=$genres, totalBookCount=$totalBookCount, sharingLabels=$sharingLabels]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    if (status != null) {
      _json[r'status'] = status;
    }
    if (statusLock != null) {
      _json[r'statusLock'] = statusLock;
    }
    if (title != null) {
      _json[r'title'] = title;
    }
    if (titleLock != null) {
      _json[r'titleLock'] = titleLock;
    }
    if (titleSort != null) {
      _json[r'titleSort'] = titleSort;
    }
    if (titleSortLock != null) {
      _json[r'titleSortLock'] = titleSortLock;
    }
    if (summary != null) {
      _json[r'summary'] = summary;
    }
    if (summaryLock != null) {
      _json[r'summaryLock'] = summaryLock;
    }
    if (publisher != null) {
      _json[r'publisher'] = publisher;
    }
    if (publisherLock != null) {
      _json[r'publisherLock'] = publisherLock;
    }
    if (readingDirectionLock != null) {
      _json[r'readingDirectionLock'] = readingDirectionLock;
    }
    if (ageRatingLock != null) {
      _json[r'ageRatingLock'] = ageRatingLock;
    }
    if (language != null) {
      _json[r'language'] = language;
    }
    if (languageLock != null) {
      _json[r'languageLock'] = languageLock;
    }
    if (genresLock != null) {
      _json[r'genresLock'] = genresLock;
    }
    if (tagsLock != null) {
      _json[r'tagsLock'] = tagsLock;
    }
    if (totalBookCountLock != null) {
      _json[r'totalBookCountLock'] = totalBookCountLock;
    }
    if (sharingLabelsLock != null) {
      _json[r'sharingLabelsLock'] = sharingLabelsLock;
    }
    _json[r'tags'] = tags;
    if (readingDirection != null) {
      _json[r'readingDirection'] = readingDirection;
    }
    if (ageRating != null) {
      _json[r'ageRating'] = ageRating;
    }
    _json[r'genres'] = genres;
    if (totalBookCount != null) {
      _json[r'totalBookCount'] = totalBookCount;
    }
    _json[r'sharingLabels'] = sharingLabels;
    return _json;
  }

  /// Returns a new [SeriesMetadataUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static SeriesMetadataUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "SeriesMetadataUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "SeriesMetadataUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return SeriesMetadataUpdateDto(
        status: SeriesMetadataUpdateDtoStatusEnum.fromJson(json[r'status']),
        statusLock: mapValueOfType<bool>(json, r'statusLock'),
        title: mapValueOfType<String>(json, r'title'),
        titleLock: mapValueOfType<bool>(json, r'titleLock'),
        titleSort: mapValueOfType<String>(json, r'titleSort'),
        titleSortLock: mapValueOfType<bool>(json, r'titleSortLock'),
        summary: mapValueOfType<String>(json, r'summary'),
        summaryLock: mapValueOfType<bool>(json, r'summaryLock'),
        publisher: mapValueOfType<String>(json, r'publisher'),
        publisherLock: mapValueOfType<bool>(json, r'publisherLock'),
        readingDirectionLock:
            mapValueOfType<bool>(json, r'readingDirectionLock'),
        ageRatingLock: mapValueOfType<bool>(json, r'ageRatingLock'),
        language: mapValueOfType<String>(json, r'language'),
        languageLock: mapValueOfType<bool>(json, r'languageLock'),
        genresLock: mapValueOfType<bool>(json, r'genresLock'),
        tagsLock: mapValueOfType<bool>(json, r'tagsLock'),
        totalBookCountLock: mapValueOfType<bool>(json, r'totalBookCountLock'),
        sharingLabelsLock: mapValueOfType<bool>(json, r'sharingLabelsLock'),
        tags: json[r'tags'] is Set
            ? (json[r'tags'] as Set).cast<String>()
            : const {},
        readingDirection: SeriesMetadataUpdateDtoReadingDirectionEnum.fromJson(
            json[r'readingDirection']),
        ageRating: mapValueOfType<int>(json, r'ageRating'),
        genres: json[r'genres'] is Set
            ? (json[r'genres'] as Set).cast<String>()
            : const {},
        totalBookCount: mapValueOfType<int>(json, r'totalBookCount'),
        sharingLabels: json[r'sharingLabels'] is Set
            ? (json[r'sharingLabels'] as Set).cast<String>()
            : const {},
      );
    }
    return null;
  }

  static List<SeriesMetadataUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <SeriesMetadataUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = SeriesMetadataUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, SeriesMetadataUpdateDto> mapFromJson(dynamic json) {
    final map = <String, SeriesMetadataUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesMetadataUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of SeriesMetadataUpdateDto-objects as value to a dart map
  static Map<String, List<SeriesMetadataUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<SeriesMetadataUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesMetadataUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{};
}

class SeriesMetadataUpdateDtoStatusEnum {
  /// Instantiate a new enum with the provided [value].
  const SeriesMetadataUpdateDtoStatusEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ENDED = SeriesMetadataUpdateDtoStatusEnum._(r'ENDED');
  static const ONGOING = SeriesMetadataUpdateDtoStatusEnum._(r'ONGOING');
  static const ABANDONED = SeriesMetadataUpdateDtoStatusEnum._(r'ABANDONED');
  static const HIATUS = SeriesMetadataUpdateDtoStatusEnum._(r'HIATUS');

  /// List of all possible values in this [enum][SeriesMetadataUpdateDtoStatusEnum].
  static const values = <SeriesMetadataUpdateDtoStatusEnum>[
    ENDED,
    ONGOING,
    ABANDONED,
    HIATUS,
  ];

  static SeriesMetadataUpdateDtoStatusEnum? fromJson(dynamic value) =>
      SeriesMetadataUpdateDtoStatusEnumTypeTransformer().decode(value);

  static List<SeriesMetadataUpdateDtoStatusEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <SeriesMetadataUpdateDtoStatusEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = SeriesMetadataUpdateDtoStatusEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [SeriesMetadataUpdateDtoStatusEnum] to String,
/// and [decode] dynamic data back to [SeriesMetadataUpdateDtoStatusEnum].
class SeriesMetadataUpdateDtoStatusEnumTypeTransformer {
  factory SeriesMetadataUpdateDtoStatusEnumTypeTransformer() =>
      _instance ??= const SeriesMetadataUpdateDtoStatusEnumTypeTransformer._();

  const SeriesMetadataUpdateDtoStatusEnumTypeTransformer._();

  String encode(SeriesMetadataUpdateDtoStatusEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a SeriesMetadataUpdateDtoStatusEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  SeriesMetadataUpdateDtoStatusEnum? decode(dynamic data,
      {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'ENDED':
          return SeriesMetadataUpdateDtoStatusEnum.ENDED;
        case r'ONGOING':
          return SeriesMetadataUpdateDtoStatusEnum.ONGOING;
        case r'ABANDONED':
          return SeriesMetadataUpdateDtoStatusEnum.ABANDONED;
        case r'HIATUS':
          return SeriesMetadataUpdateDtoStatusEnum.HIATUS;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [SeriesMetadataUpdateDtoStatusEnumTypeTransformer] instance.
  static SeriesMetadataUpdateDtoStatusEnumTypeTransformer? _instance;
}

class SeriesMetadataUpdateDtoReadingDirectionEnum {
  /// Instantiate a new enum with the provided [value].
  const SeriesMetadataUpdateDtoReadingDirectionEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const LEFT_TO_RIGHT =
      SeriesMetadataUpdateDtoReadingDirectionEnum._(r'LEFT_TO_RIGHT');
  static const RIGHT_TO_LEFT =
      SeriesMetadataUpdateDtoReadingDirectionEnum._(r'RIGHT_TO_LEFT');
  static const VERTICAL =
      SeriesMetadataUpdateDtoReadingDirectionEnum._(r'VERTICAL');
  static const WEBTOON =
      SeriesMetadataUpdateDtoReadingDirectionEnum._(r'WEBTOON');

  /// List of all possible values in this [enum][SeriesMetadataUpdateDtoReadingDirectionEnum].
  static const values = <SeriesMetadataUpdateDtoReadingDirectionEnum>[
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT,
    VERTICAL,
    WEBTOON,
  ];

  static SeriesMetadataUpdateDtoReadingDirectionEnum? fromJson(dynamic value) =>
      SeriesMetadataUpdateDtoReadingDirectionEnumTypeTransformer()
          .decode(value);

  static List<SeriesMetadataUpdateDtoReadingDirectionEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <SeriesMetadataUpdateDtoReadingDirectionEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = SeriesMetadataUpdateDtoReadingDirectionEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [SeriesMetadataUpdateDtoReadingDirectionEnum] to String,
/// and [decode] dynamic data back to [SeriesMetadataUpdateDtoReadingDirectionEnum].
class SeriesMetadataUpdateDtoReadingDirectionEnumTypeTransformer {
  factory SeriesMetadataUpdateDtoReadingDirectionEnumTypeTransformer() =>
      _instance ??=
          const SeriesMetadataUpdateDtoReadingDirectionEnumTypeTransformer._();

  const SeriesMetadataUpdateDtoReadingDirectionEnumTypeTransformer._();

  String encode(SeriesMetadataUpdateDtoReadingDirectionEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a SeriesMetadataUpdateDtoReadingDirectionEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  SeriesMetadataUpdateDtoReadingDirectionEnum? decode(dynamic data,
      {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'LEFT_TO_RIGHT':
          return SeriesMetadataUpdateDtoReadingDirectionEnum.LEFT_TO_RIGHT;
        case r'RIGHT_TO_LEFT':
          return SeriesMetadataUpdateDtoReadingDirectionEnum.RIGHT_TO_LEFT;
        case r'VERTICAL':
          return SeriesMetadataUpdateDtoReadingDirectionEnum.VERTICAL;
        case r'WEBTOON':
          return SeriesMetadataUpdateDtoReadingDirectionEnum.WEBTOON;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [SeriesMetadataUpdateDtoReadingDirectionEnumTypeTransformer] instance.
  static SeriesMetadataUpdateDtoReadingDirectionEnumTypeTransformer? _instance;
}
