//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class TachiyomiReadProgressUpdateV2Dto {
  /// Returns a new [TachiyomiReadProgressUpdateV2Dto] instance.
  TachiyomiReadProgressUpdateV2Dto({
    required this.lastBookNumberSortRead,
  });

  double lastBookNumberSortRead;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TachiyomiReadProgressUpdateV2Dto &&
          other.lastBookNumberSortRead == lastBookNumberSortRead;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (lastBookNumberSortRead.hashCode);

  @override
  String toString() =>
      'TachiyomiReadProgressUpdateV2Dto[lastBookNumberSortRead=$lastBookNumberSortRead]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'lastBookNumberSortRead'] = lastBookNumberSortRead;
    return _json;
  }

  /// Returns a new [TachiyomiReadProgressUpdateV2Dto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static TachiyomiReadProgressUpdateV2Dto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "TachiyomiReadProgressUpdateV2Dto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "TachiyomiReadProgressUpdateV2Dto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return TachiyomiReadProgressUpdateV2Dto(
        lastBookNumberSortRead:
            mapValueOfType<double>(json, r'lastBookNumberSortRead')!,
      );
    }
    return null;
  }

  static List<TachiyomiReadProgressUpdateV2Dto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <TachiyomiReadProgressUpdateV2Dto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = TachiyomiReadProgressUpdateV2Dto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, TachiyomiReadProgressUpdateV2Dto> mapFromJson(
      dynamic json) {
    final map = <String, TachiyomiReadProgressUpdateV2Dto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TachiyomiReadProgressUpdateV2Dto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of TachiyomiReadProgressUpdateV2Dto-objects as value to a dart map
  static Map<String, List<TachiyomiReadProgressUpdateV2Dto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<TachiyomiReadProgressUpdateV2Dto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TachiyomiReadProgressUpdateV2Dto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'lastBookNumberSortRead',
  };
}
