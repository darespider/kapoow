//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ReadListRequestResultBookDto {
  /// Returns a new [ReadListRequestResultBookDto] instance.
  ReadListRequestResultBookDto({
    required this.book,
    required this.errorCode,
  });

  ReadListRequestBookDto book;

  String errorCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReadListRequestResultBookDto &&
          other.book == book &&
          other.errorCode == errorCode;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (book.hashCode) + (errorCode.hashCode);

  @override
  String toString() =>
      'ReadListRequestResultBookDto[book=$book, errorCode=$errorCode]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'book'] = book;
    _json[r'errorCode'] = errorCode;
    return _json;
  }

  /// Returns a new [ReadListRequestResultBookDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ReadListRequestResultBookDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ReadListRequestResultBookDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ReadListRequestResultBookDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ReadListRequestResultBookDto(
        book: ReadListRequestBookDto.fromJson(json[r'book'])!,
        errorCode: mapValueOfType<String>(json, r'errorCode')!,
      );
    }
    return null;
  }

  static List<ReadListRequestResultBookDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ReadListRequestResultBookDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ReadListRequestResultBookDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ReadListRequestResultBookDto> mapFromJson(dynamic json) {
    final map = <String, ReadListRequestResultBookDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListRequestResultBookDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ReadListRequestResultBookDto-objects as value to a dart map
  static Map<String, List<ReadListRequestResultBookDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ReadListRequestResultBookDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListRequestResultBookDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'book',
    'errorCode',
  };
}
