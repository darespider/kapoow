//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class PageDto {
  /// Returns a new [PageDto] instance.
  PageDto({
    required this.number,
    required this.fileName,
    required this.mediaType,
    this.width,
    this.height,
    this.sizeBytes,
    required this.size,
  });

  int number;

  String fileName;

  String mediaType;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? width;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? height;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? sizeBytes;

  String size;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PageDto &&
          other.number == number &&
          other.fileName == fileName &&
          other.mediaType == mediaType &&
          other.width == width &&
          other.height == height &&
          other.sizeBytes == sizeBytes &&
          other.size == size;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (number.hashCode) +
      (fileName.hashCode) +
      (mediaType.hashCode) +
      (width == null ? 0 : width!.hashCode) +
      (height == null ? 0 : height!.hashCode) +
      (sizeBytes == null ? 0 : sizeBytes!.hashCode) +
      (size.hashCode);

  @override
  String toString() =>
      'PageDto[number=$number, fileName=$fileName, mediaType=$mediaType, width=$width, height=$height, sizeBytes=$sizeBytes, size=$size]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'number'] = number;
    _json[r'fileName'] = fileName;
    _json[r'mediaType'] = mediaType;
    if (width != null) {
      _json[r'width'] = width;
    }
    if (height != null) {
      _json[r'height'] = height;
    }
    if (sizeBytes != null) {
      _json[r'sizeBytes'] = sizeBytes;
    }
    _json[r'size'] = size;
    return _json;
  }

  /// Returns a new [PageDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static PageDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "PageDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "PageDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return PageDto(
        number: mapValueOfType<int>(json, r'number')!,
        fileName: mapValueOfType<String>(json, r'fileName')!,
        mediaType: mapValueOfType<String>(json, r'mediaType')!,
        width: mapValueOfType<int>(json, r'width'),
        height: mapValueOfType<int>(json, r'height'),
        sizeBytes: mapValueOfType<int>(json, r'sizeBytes'),
        size: mapValueOfType<String>(json, r'size')!,
      );
    }
    return null;
  }

  static List<PageDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <PageDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = PageDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, PageDto> mapFromJson(dynamic json) {
    final map = <String, PageDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of PageDto-objects as value to a dart map
  static Map<String, List<PageDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<PageDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'number',
    'fileName',
    'mediaType',
    'size',
  };
}
