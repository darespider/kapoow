//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class PageHashUnknownDto {
  /// Returns a new [PageHashUnknownDto] instance.
  PageHashUnknownDto({
    required this.hash,
    required this.mediaType,
    this.size,
    required this.matchCount,
  });

  String hash;

  String mediaType;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? size;

  int matchCount;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PageHashUnknownDto &&
          other.hash == hash &&
          other.mediaType == mediaType &&
          other.size == size &&
          other.matchCount == matchCount;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (hash.hashCode) +
      (mediaType.hashCode) +
      (size == null ? 0 : size!.hashCode) +
      (matchCount.hashCode);

  @override
  String toString() =>
      'PageHashUnknownDto[hash=$hash, mediaType=$mediaType, size=$size, matchCount=$matchCount]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'hash'] = hash;
    _json[r'mediaType'] = mediaType;
    if (size != null) {
      _json[r'size'] = size;
    }
    _json[r'matchCount'] = matchCount;
    return _json;
  }

  /// Returns a new [PageHashUnknownDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static PageHashUnknownDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "PageHashUnknownDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "PageHashUnknownDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return PageHashUnknownDto(
        hash: mapValueOfType<String>(json, r'hash')!,
        mediaType: mapValueOfType<String>(json, r'mediaType')!,
        size: mapValueOfType<int>(json, r'size'),
        matchCount: mapValueOfType<int>(json, r'matchCount')!,
      );
    }
    return null;
  }

  static List<PageHashUnknownDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <PageHashUnknownDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = PageHashUnknownDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, PageHashUnknownDto> mapFromJson(dynamic json) {
    final map = <String, PageHashUnknownDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashUnknownDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of PageHashUnknownDto-objects as value to a dart map
  static Map<String, List<PageHashUnknownDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<PageHashUnknownDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashUnknownDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'hash',
    'mediaType',
    'matchCount',
  };
}
