//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class TransientBookDto {
  /// Returns a new [TransientBookDto] instance.
  TransientBookDto({
    required this.id,
    required this.name,
    required this.url,
    required this.fileLastModified,
    required this.sizeBytes,
    required this.size,
    required this.status,
    required this.mediaType,
    this.pages = const [],
    this.files = const [],
    required this.comment,
  });

  String id;

  String name;

  String url;

  DateTime fileLastModified;

  int sizeBytes;

  String size;

  String status;

  String mediaType;

  List<PageDto> pages;

  List<String> files;

  String comment;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TransientBookDto &&
          other.id == id &&
          other.name == name &&
          other.url == url &&
          other.fileLastModified == fileLastModified &&
          other.sizeBytes == sizeBytes &&
          other.size == size &&
          other.status == status &&
          other.mediaType == mediaType &&
          other.pages == pages &&
          other.files == files &&
          other.comment == comment;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (name.hashCode) +
      (url.hashCode) +
      (fileLastModified.hashCode) +
      (sizeBytes.hashCode) +
      (size.hashCode) +
      (status.hashCode) +
      (mediaType.hashCode) +
      (pages.hashCode) +
      (files.hashCode) +
      (comment.hashCode);

  @override
  String toString() =>
      'TransientBookDto[id=$id, name=$name, url=$url, fileLastModified=$fileLastModified, sizeBytes=$sizeBytes, size=$size, status=$status, mediaType=$mediaType, pages=$pages, files=$files, comment=$comment]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'name'] = name;
    _json[r'url'] = url;
    _json[r'fileLastModified'] = fileLastModified.toUtc().toIso8601String();
    _json[r'sizeBytes'] = sizeBytes;
    _json[r'size'] = size;
    _json[r'status'] = status;
    _json[r'mediaType'] = mediaType;
    _json[r'pages'] = pages;
    _json[r'files'] = files;
    _json[r'comment'] = comment;
    return _json;
  }

  /// Returns a new [TransientBookDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static TransientBookDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "TransientBookDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "TransientBookDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return TransientBookDto(
        id: mapValueOfType<String>(json, r'id')!,
        name: mapValueOfType<String>(json, r'name')!,
        url: mapValueOfType<String>(json, r'url')!,
        fileLastModified: mapDateTime(json, r'fileLastModified', '')!,
        sizeBytes: mapValueOfType<int>(json, r'sizeBytes')!,
        size: mapValueOfType<String>(json, r'size')!,
        status: mapValueOfType<String>(json, r'status')!,
        mediaType: mapValueOfType<String>(json, r'mediaType')!,
        pages: PageDto.listFromJson(json[r'pages'])!,
        files: json[r'files'] is List
            ? (json[r'files'] as List).cast<String>()
            : const [],
        comment: mapValueOfType<String>(json, r'comment')!,
      );
    }
    return null;
  }

  static List<TransientBookDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <TransientBookDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = TransientBookDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, TransientBookDto> mapFromJson(dynamic json) {
    final map = <String, TransientBookDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TransientBookDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of TransientBookDto-objects as value to a dart map
  static Map<String, List<TransientBookDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<TransientBookDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TransientBookDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'name',
    'url',
    'fileLastModified',
    'sizeBytes',
    'size',
    'status',
    'mediaType',
    'pages',
    'files',
    'comment',
  };
}
