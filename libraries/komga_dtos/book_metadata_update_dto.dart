//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class BookMetadataUpdateDto {
  /// Returns a new [BookMetadataUpdateDto] instance.
  BookMetadataUpdateDto({
    this.title,
    this.titleLock,
    this.summaryLock,
    this.number,
    this.numberLock,
    this.numberSort,
    this.numberSortLock,
    this.releaseDateLock,
    this.authorsLock,
    this.tagsLock,
    this.isbnLock,
    this.linksLock,
    this.tags = const {},
    this.links = const [],
    this.authors = const [],
    this.summary,
    this.releaseDate,
    this.isbn,
  });

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? title;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? titleLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? summaryLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? number;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? numberLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  double? numberSort;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? numberSortLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? releaseDateLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? authorsLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? tagsLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? isbnLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? linksLock;

  Set<String> tags;

  List<WebLinkUpdateDto> links;

  List<AuthorUpdateDto> authors;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? summary;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  DateTime? releaseDate;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? isbn;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookMetadataUpdateDto &&
          other.title == title &&
          other.titleLock == titleLock &&
          other.summaryLock == summaryLock &&
          other.number == number &&
          other.numberLock == numberLock &&
          other.numberSort == numberSort &&
          other.numberSortLock == numberSortLock &&
          other.releaseDateLock == releaseDateLock &&
          other.authorsLock == authorsLock &&
          other.tagsLock == tagsLock &&
          other.isbnLock == isbnLock &&
          other.linksLock == linksLock &&
          other.tags == tags &&
          other.links == links &&
          other.authors == authors &&
          other.summary == summary &&
          other.releaseDate == releaseDate &&
          other.isbn == isbn;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (title == null ? 0 : title!.hashCode) +
      (titleLock == null ? 0 : titleLock!.hashCode) +
      (summaryLock == null ? 0 : summaryLock!.hashCode) +
      (number == null ? 0 : number!.hashCode) +
      (numberLock == null ? 0 : numberLock!.hashCode) +
      (numberSort == null ? 0 : numberSort!.hashCode) +
      (numberSortLock == null ? 0 : numberSortLock!.hashCode) +
      (releaseDateLock == null ? 0 : releaseDateLock!.hashCode) +
      (authorsLock == null ? 0 : authorsLock!.hashCode) +
      (tagsLock == null ? 0 : tagsLock!.hashCode) +
      (isbnLock == null ? 0 : isbnLock!.hashCode) +
      (linksLock == null ? 0 : linksLock!.hashCode) +
      (tags.hashCode) +
      (links.hashCode) +
      (authors.hashCode) +
      (summary == null ? 0 : summary!.hashCode) +
      (releaseDate == null ? 0 : releaseDate!.hashCode) +
      (isbn == null ? 0 : isbn!.hashCode);

  @override
  String toString() =>
      'BookMetadataUpdateDto[title=$title, titleLock=$titleLock, summaryLock=$summaryLock, number=$number, numberLock=$numberLock, numberSort=$numberSort, numberSortLock=$numberSortLock, releaseDateLock=$releaseDateLock, authorsLock=$authorsLock, tagsLock=$tagsLock, isbnLock=$isbnLock, linksLock=$linksLock, tags=$tags, links=$links, authors=$authors, summary=$summary, releaseDate=$releaseDate, isbn=$isbn]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    if (title != null) {
      _json[r'title'] = title;
    }
    if (titleLock != null) {
      _json[r'titleLock'] = titleLock;
    }
    if (summaryLock != null) {
      _json[r'summaryLock'] = summaryLock;
    }
    if (number != null) {
      _json[r'number'] = number;
    }
    if (numberLock != null) {
      _json[r'numberLock'] = numberLock;
    }
    if (numberSort != null) {
      _json[r'numberSort'] = numberSort;
    }
    if (numberSortLock != null) {
      _json[r'numberSortLock'] = numberSortLock;
    }
    if (releaseDateLock != null) {
      _json[r'releaseDateLock'] = releaseDateLock;
    }
    if (authorsLock != null) {
      _json[r'authorsLock'] = authorsLock;
    }
    if (tagsLock != null) {
      _json[r'tagsLock'] = tagsLock;
    }
    if (isbnLock != null) {
      _json[r'isbnLock'] = isbnLock;
    }
    if (linksLock != null) {
      _json[r'linksLock'] = linksLock;
    }
    _json[r'tags'] = tags;
    _json[r'links'] = links;
    _json[r'authors'] = authors;
    if (summary != null) {
      _json[r'summary'] = summary;
    }
    if (releaseDate != null) {
      _json[r'releaseDate'] = dateFormatter.format(releaseDate!.toUtc());
    }
    if (isbn != null) {
      _json[r'isbn'] = isbn;
    }
    return _json;
  }

  /// Returns a new [BookMetadataUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static BookMetadataUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "BookMetadataUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "BookMetadataUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return BookMetadataUpdateDto(
        title: mapValueOfType<String>(json, r'title'),
        titleLock: mapValueOfType<bool>(json, r'titleLock'),
        summaryLock: mapValueOfType<bool>(json, r'summaryLock'),
        number: mapValueOfType<String>(json, r'number'),
        numberLock: mapValueOfType<bool>(json, r'numberLock'),
        numberSort: mapValueOfType<double>(json, r'numberSort'),
        numberSortLock: mapValueOfType<bool>(json, r'numberSortLock'),
        releaseDateLock: mapValueOfType<bool>(json, r'releaseDateLock'),
        authorsLock: mapValueOfType<bool>(json, r'authorsLock'),
        tagsLock: mapValueOfType<bool>(json, r'tagsLock'),
        isbnLock: mapValueOfType<bool>(json, r'isbnLock'),
        linksLock: mapValueOfType<bool>(json, r'linksLock'),
        tags: json[r'tags'] is Set
            ? (json[r'tags'] as Set).cast<String>()
            : const {},
        links: WebLinkUpdateDto.listFromJson(json[r'links']) ?? const [],
        authors: AuthorUpdateDto.listFromJson(json[r'authors']) ?? const [],
        summary: mapValueOfType<String>(json, r'summary'),
        releaseDate: mapDateTime(json, r'releaseDate', ''),
        isbn: mapValueOfType<String>(json, r'isbn'),
      );
    }
    return null;
  }

  static List<BookMetadataUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <BookMetadataUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BookMetadataUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, BookMetadataUpdateDto> mapFromJson(dynamic json) {
    final map = <String, BookMetadataUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookMetadataUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of BookMetadataUpdateDto-objects as value to a dart map
  static Map<String, List<BookMetadataUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<BookMetadataUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookMetadataUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{};
}
