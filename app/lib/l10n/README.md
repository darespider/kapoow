# Kapoow l10n conventions

## ARB files

> [General Syntax Info](https://localizely.com/flutter-arb/)

- naming: `[feature][ui_dir][file][identifier]`
    ```json
    "sessionScreenInputServerTitle": "Server Connection Info",
    "@sessionScreenInputServerTitle": {
        "description": "Title for Input Server Screen",
        "context": "InputServerScreen"
    },
    ```
    this translation will be used on the `Session` feature in the `Screens` directory in the `InputServer` file in their title