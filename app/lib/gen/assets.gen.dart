/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart' as _svg;
import 'package:vector_graphics/vector_graphics.dart' as _vg;

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// Directory path: assets/images/cover_placeholder
  $AssetsImagesCoverPlaceholderGen get coverPlaceholder =>
      const $AssetsImagesCoverPlaceholderGen();

  /// Directory path: assets/images/loader
  $AssetsImagesLoaderGen get loader => const $AssetsImagesLoaderGen();

  /// Directory path: assets/images/onboarding
  $AssetsImagesOnboardingGen get onboarding =>
      const $AssetsImagesOnboardingGen();

  /// Directory path: assets/images/search
  $AssetsImagesSearchGen get search => const $AssetsImagesSearchGen();

  /// Directory path: assets/images/splash
  $AssetsImagesSplashGen get splash => const $AssetsImagesSplashGen();
}

class $AssetsMockResponsesGen {
  const $AssetsMockResponsesGen();

  /// File path: assets/mock_responses/api_v1_age-ratings.json
  String get apiV1AgeRatings => 'assets/mock_responses/api_v1_age-ratings.json';

  /// File path: assets/mock_responses/api_v1_books.json
  String get apiV1Books => 'assets/mock_responses/api_v1_books.json';

  /// File path: assets/mock_responses/api_v1_books_0APVQJXSY5C7J.json
  String get apiV1Books0APVQJXSY5C7J =>
      'assets/mock_responses/api_v1_books_0APVQJXSY5C7J.json';

  /// File path: assets/mock_responses/api_v1_books_0APVQJXSY5C7J_next.json
  String get apiV1Books0APVQJXSY5C7JNext =>
      'assets/mock_responses/api_v1_books_0APVQJXSY5C7J_next.json';

  /// File path: assets/mock_responses/api_v1_books_0APVQJXSY5C7J_pages.json
  String get apiV1Books0APVQJXSY5C7JPages =>
      'assets/mock_responses/api_v1_books_0APVQJXSY5C7J_pages.json';

  /// File path: assets/mock_responses/api_v1_books_0APVQJXSY5C7J_thumbnail.jpeg
  AssetGenImage get apiV1Books0APVQJXSY5C7JThumbnail => const AssetGenImage(
      'assets/mock_responses/api_v1_books_0APVQJXSY5C7J_thumbnail.jpeg');

  /// File path: assets/mock_responses/api_v1_books_ondeck.json
  String get apiV1BooksOndeck =>
      'assets/mock_responses/api_v1_books_ondeck.json';

  /// File path: assets/mock_responses/api_v1_collections.json
  String get apiV1Collections =>
      'assets/mock_responses/api_v1_collections.json';

  /// File path: assets/mock_responses/api_v1_collections_0AEA769D9Z133.json
  String get apiV1Collections0AEA769D9Z133 =>
      'assets/mock_responses/api_v1_collections_0AEA769D9Z133.json';

  /// File path: assets/mock_responses/api_v1_collections_0AEA769D9Z133_series.json
  String get apiV1Collections0AEA769D9Z133Series =>
      'assets/mock_responses/api_v1_collections_0AEA769D9Z133_series.json';

  /// File path: assets/mock_responses/api_v1_collections_0AEA769D9Z133_thumbnail.jpeg
  AssetGenImage get apiV1Collections0AEA769D9Z133Thumbnail => const AssetGenImage(
      'assets/mock_responses/api_v1_collections_0AEA769D9Z133_thumbnail.jpeg');

  /// File path: assets/mock_responses/api_v1_genres.json
  String get apiV1Genres => 'assets/mock_responses/api_v1_genres.json';

  /// File path: assets/mock_responses/api_v1_languages.json
  String get apiV1Languages => 'assets/mock_responses/api_v1_languages.json';

  /// File path: assets/mock_responses/api_v1_publishers.json
  String get apiV1Publishers => 'assets/mock_responses/api_v1_publishers.json';

  /// File path: assets/mock_responses/api_v1_readlists.json
  String get apiV1Readlists => 'assets/mock_responses/api_v1_readlists.json';

  /// File path: assets/mock_responses/api_v1_readlists_0AE8CY15B0WQD.json
  String get apiV1Readlists0AE8CY15B0WQD =>
      'assets/mock_responses/api_v1_readlists_0AE8CY15B0WQD.json';

  /// File path: assets/mock_responses/api_v1_readlists_0AE8CY15B0WQD_books.json
  String get apiV1Readlists0AE8CY15B0WQDBooks =>
      'assets/mock_responses/api_v1_readlists_0AE8CY15B0WQD_books.json';

  /// File path: assets/mock_responses/api_v1_readlists_0AE8CY15B0WQD_thumbnail.jpeg
  AssetGenImage get apiV1Readlists0AE8CY15B0WQDThumbnail => const AssetGenImage(
      'assets/mock_responses/api_v1_readlists_0AE8CY15B0WQD_thumbnail.jpeg');

  /// File path: assets/mock_responses/api_v1_series-page12.json
  String get apiV1SeriesPage12 =>
      'assets/mock_responses/api_v1_series-page12.json';

  /// File path: assets/mock_responses/api_v1_series_0APVQJXP650ZK.json
  String get apiV1Series0APVQJXP650ZK =>
      'assets/mock_responses/api_v1_series_0APVQJXP650ZK.json';

  /// File path: assets/mock_responses/api_v1_series_0APVQJXP650ZK_books.json
  String get apiV1Series0APVQJXP650ZKBooks =>
      'assets/mock_responses/api_v1_series_0APVQJXP650ZK_books.json';

  /// File path: assets/mock_responses/api_v1_series_0APVQJXP650ZK_thumbnail.jpeg
  AssetGenImage get apiV1Series0APVQJXP650ZKThumbnail => const AssetGenImage(
      'assets/mock_responses/api_v1_series_0APVQJXP650ZK_thumbnail.jpeg');

  /// File path: assets/mock_responses/api_v1_series_latest.json
  String get apiV1SeriesLatest =>
      'assets/mock_responses/api_v1_series_latest.json';

  /// File path: assets/mock_responses/api_v1_sharing-labels.json
  String get apiV1SharingLabels =>
      'assets/mock_responses/api_v1_sharing-labels.json';

  /// File path: assets/mock_responses/api_v1_tags.json
  String get apiV1Tags => 'assets/mock_responses/api_v1_tags.json';

  /// File path: assets/mock_responses/api_v1_tags_series.json
  String get apiV1TagsSeries => 'assets/mock_responses/api_v1_tags_series.json';

  /// File path: assets/mock_responses/api_v2_user.json
  String get apiV2User => 'assets/mock_responses/api_v2_user.json';

  /// List of all assets
  List<dynamic> get values => [
        apiV1AgeRatings,
        apiV1Books,
        apiV1Books0APVQJXSY5C7J,
        apiV1Books0APVQJXSY5C7JNext,
        apiV1Books0APVQJXSY5C7JPages,
        apiV1Books0APVQJXSY5C7JThumbnail,
        apiV1BooksOndeck,
        apiV1Collections,
        apiV1Collections0AEA769D9Z133,
        apiV1Collections0AEA769D9Z133Series,
        apiV1Collections0AEA769D9Z133Thumbnail,
        apiV1Genres,
        apiV1Languages,
        apiV1Publishers,
        apiV1Readlists,
        apiV1Readlists0AE8CY15B0WQD,
        apiV1Readlists0AE8CY15B0WQDBooks,
        apiV1Readlists0AE8CY15B0WQDThumbnail,
        apiV1SeriesPage12,
        apiV1Series0APVQJXP650ZK,
        apiV1Series0APVQJXP650ZKBooks,
        apiV1Series0APVQJXP650ZKThumbnail,
        apiV1SeriesLatest,
        apiV1SharingLabels,
        apiV1Tags,
        apiV1TagsSeries,
        apiV2User
      ];
}

class $AssetsImagesCoverPlaceholderGen {
  const $AssetsImagesCoverPlaceholderGen();

  /// File path: assets/images/cover_placeholder/v1.svg
  SvgGenImage get v1 =>
      const SvgGenImage('assets/images/cover_placeholder/v1.svg');

  /// File path: assets/images/cover_placeholder/v2.svg
  SvgGenImage get v2 =>
      const SvgGenImage('assets/images/cover_placeholder/v2.svg');

  /// File path: assets/images/cover_placeholder/v3.svg
  SvgGenImage get v3 =>
      const SvgGenImage('assets/images/cover_placeholder/v3.svg');

  /// File path: assets/images/cover_placeholder/v4.svg
  SvgGenImage get v4 =>
      const SvgGenImage('assets/images/cover_placeholder/v4.svg');

  /// File path: assets/images/cover_placeholder/v5.svg
  SvgGenImage get v5 =>
      const SvgGenImage('assets/images/cover_placeholder/v5.svg');

  /// List of all assets
  List<SvgGenImage> get values => [v1, v2, v3, v4, v5];
}

class $AssetsImagesLoaderGen {
  const $AssetsImagesLoaderGen();

  /// File path: assets/images/loader/onomatopoeia.svg
  SvgGenImage get onomatopoeia =>
      const SvgGenImage('assets/images/loader/onomatopoeia.svg');

  /// List of all assets
  List<SvgGenImage> get values => [onomatopoeia];
}

class $AssetsImagesOnboardingGen {
  const $AssetsImagesOnboardingGen();

  /// File path: assets/images/onboarding/background_dark.svg
  SvgGenImage get backgroundDark =>
      const SvgGenImage('assets/images/onboarding/background_dark.svg');

  /// File path: assets/images/onboarding/background_light.svg
  SvgGenImage get backgroundLight =>
      const SvgGenImage('assets/images/onboarding/background_light.svg');

  /// File path: assets/images/onboarding/header_login.svg
  SvgGenImage get headerLogin =>
      const SvgGenImage('assets/images/onboarding/header_login.svg');

  /// File path: assets/images/onboarding/header_server.svg
  SvgGenImage get headerServer =>
      const SvgGenImage('assets/images/onboarding/header_server.svg');

  /// List of all assets
  List<SvgGenImage> get values =>
      [backgroundDark, backgroundLight, headerLogin, headerServer];
}

class $AssetsImagesSearchGen {
  const $AssetsImagesSearchGen();

  /// File path: assets/images/search/search_empty.svg
  SvgGenImage get searchEmpty =>
      const SvgGenImage('assets/images/search/search_empty.svg');

  /// File path: assets/images/search/search_placeholder.svg
  SvgGenImage get searchPlaceholder =>
      const SvgGenImage('assets/images/search/search_placeholder.svg');

  /// List of all assets
  List<SvgGenImage> get values => [searchEmpty, searchPlaceholder];
}

class $AssetsImagesSplashGen {
  const $AssetsImagesSplashGen();

  /// File path: assets/images/splash/app_icon_blank.svg
  SvgGenImage get appIconBlank =>
      const SvgGenImage('assets/images/splash/app_icon_blank.svg');

  /// List of all assets
  List<SvgGenImage> get values => [appIconBlank];
}

class Assets {
  Assets._();

  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const $AssetsMockResponsesGen mockResponses =
      $AssetsMockResponsesGen();
}

class AssetGenImage {
  const AssetGenImage(
    this._assetName, {
    this.size,
    this.flavors = const {},
  });

  final String _assetName;

  final Size? size;
  final Set<String> flavors;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(
    this._assetName, {
    this.size,
    this.flavors = const {},
  }) : _isVecFormat = false;

  const SvgGenImage.vec(
    this._assetName, {
    this.size,
    this.flavors = const {},
  }) : _isVecFormat = true;

  final String _assetName;
  final Size? size;
  final Set<String> flavors;
  final bool _isVecFormat;

  _svg.SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    _svg.SvgTheme? theme,
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    final _svg.BytesLoader loader;
    if (_isVecFormat) {
      loader = _vg.AssetBytesLoader(
        _assetName,
        assetBundle: bundle,
        packageName: package,
      );
    } else {
      loader = _svg.SvgAssetLoader(
        _assetName,
        assetBundle: bundle,
        packageName: package,
        theme: theme,
      );
    }
    return _svg.SvgPicture(
      loader,
      key: key,
      matchTextDirection: matchTextDirection,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      colorFilter: colorFilter ??
          (color == null ? null : ColorFilter.mode(color, colorBlendMode)),
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
