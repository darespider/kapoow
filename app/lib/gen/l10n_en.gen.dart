import 'l10n.gen.dart';

// ignore_for_file: type=lint

/// The translations for English (`en`).
class TranslationsEn extends Translations {
  TranslationsEn([String locale = 'en']) : super(locale);

  @override
  String get appName => 'Kapoow!';

  @override
  String get splashScreenLoading => 'Loading...';

  @override
  String get sessionScreenInputServerTitle => 'Server Connection Info';

  @override
  String get sessionScreenContinueButton => 'Continue';

  @override
  String get sessionWidgetsInputHost => 'Host';

  @override
  String get sessionWidgetsInputHostHint => 'https://komga.example.com';

  @override
  String get sessionWidgetsInputHostError => 'Invalid Host';

  @override
  String get sessionWidgetsInputPort => 'Port';

  @override
  String get sessionWidgetsInputPortHint => '8080';

  @override
  String get sessionWidgetsInputPortError => 'Invalid Port';

  @override
  String get sessionScreenLoginTitle => 'User Credentials';

  @override
  String get sessionScreenEditServerButton => 'Edit Server';

  @override
  String get sessionScreenLoginButton => 'Login';

  @override
  String get sessionWidgetsInputEmail => 'E-mail';

  @override
  String get sessionWidgetsInputEmailHint => 'email@example.com';

  @override
  String get sessionWidgetsInputEmailError => 'Invalid E-mail';

  @override
  String get sessionWidgetsInputPassword => 'Password';

  @override
  String get sessionWidgetsInputPasswordHint => 'pass123';

  @override
  String get sessionWidgetsInputPasswordError => 'Password required';

  @override
  String get libraryScreenAdvice =>
      'Explore all your books and series, or filter by library with the selector at the top.';

  @override
  String get libraryWidgetsLibrarySelector => 'All Libraries';
}
