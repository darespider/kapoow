// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_routes_data.dart';

// **************************************************************************
// GoRouterGenerator
// **************************************************************************

List<RouteBase> get $appRoutes => [
      $dashboardShellRoute,
    ];

RouteBase get $dashboardShellRoute => ShellRouteData.$route(
      factory: $DashboardShellRouteExtension._fromState,
      routes: [
        GoRouteData.$route(
          path: '/recommended',
          factory: $RecommendedRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: '/library/:libraryId',
          factory: $LibraryRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: '/settings',
          factory: $SettingsRouteExtension._fromState,
        ),
      ],
    );

extension $DashboardShellRouteExtension on DashboardShellRoute {
  static DashboardShellRoute _fromState(GoRouterState state) =>
      const DashboardShellRoute();
}

extension $RecommendedRouteExtension on RecommendedRoute {
  static RecommendedRoute _fromState(GoRouterState state) =>
      const RecommendedRoute();

  String get location => GoRouteData.$location(
        '/recommended',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $LibraryRouteExtension on LibraryRoute {
  static LibraryRoute _fromState(GoRouterState state) => LibraryRoute(
        libraryId: state.pathParameters['libraryId']!,
      );

  String get location => GoRouteData.$location(
        '/library/${Uri.encodeComponent(libraryId)}',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $SettingsRouteExtension on SettingsRoute {
  static SettingsRoute _fromState(GoRouterState state) => const SettingsRoute();

  String get location => GoRouteData.$location(
        '/settings',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}
