import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kapoow/src/features/app_init/ui/screens/splash_screen.dart';

part 'bootstrap_routes_data.g.dart';

/// All routes of bootstrapper
final bootstrapRoutes = $appRoutes;

/// [SplashRoute] is the displayed route at init of the app
///
/// has a path of `'/splash'`
@TypedGoRoute<SplashRoute>(path: SplashRoute.path)
@immutable
class SplashRoute extends GoRouteData {
  /// Creates a [SplashRoute] instance
  const SplashRoute();

  /// value of `/splash`
  static const path = '/splash';

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const SplashScreen();
  }
}

/// [ErrorInitRoute] is the displayed route when the init of the app fails
///
/// has a path of `'/error/init'`
@TypedGoRoute<ErrorInitRoute>(path: ErrorInitRoute.path)
@immutable
class ErrorInitRoute extends GoRouteData {
  /// Creates a [ErrorInitRoute] instance
  const ErrorInitRoute();

  /// Value of `/error/init`
  static const path = '/error/init';

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const Scaffold(
      body: Center(
        child: Text('Error while Kapoow initialize'),
      ),
    );
  }
}
