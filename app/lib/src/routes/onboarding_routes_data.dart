import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kapoow/src/features/session/ui/screens/input_server_form.dart';
import 'package:kapoow/src/features/session/ui/screens/login_screen_form.dart';
import 'package:kapoow/src/features/session/ui/widgets/onboarding_scaffold.dart';

part 'onboarding_routes_data.g.dart';

/// All routes of onboarding flow
final onboardingRoutes = $appRoutes;

/// Shell Route used to reuse the background of the [OnboardingScaffold] on
/// server and login forms
///
/// wraps the routes
/// ```dart
/// '/login/server'
/// '/login/user'
/// ```
@TypedShellRoute<OnboardingShellRoute>(
  routes: [
    TypedGoRoute<ServerInfoFormRoute>(path: ServerInfoFormRoute.path),
    TypedGoRoute<LoginFormRoute>(path: LoginFormRoute.path),
  ],
)
@immutable
class OnboardingShellRoute extends ShellRouteData {
  /// Creates a [OnboardingShellRoute] instance
  const OnboardingShellRoute();

  @override
  Widget builder(BuildContext context, GoRouterState state, Widget navigator) {
    return OnboardingScaffold(child: navigator);
  }
}

/// Route that displays the server info form
///
/// has a path of `'/login/server'`
@immutable
class ServerInfoFormRoute extends GoRouteData {
  /// Creates a [ServerInfoFormRoute] instance
  const ServerInfoFormRoute();

  /// Value of `/login/server`
  static const path = '/login/server';

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const InputServerScreen();
  }
}

/// Route that displays the user login form
///
/// has a path of `'/login/user'`
@immutable
class LoginFormRoute extends GoRouteData {
  /// Creates a [LoginFormRoute] instance
  const LoginFormRoute();

  /// Value of `/login/user`
  static const path = '/login/user';

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const LoginScreen();
  }
}
