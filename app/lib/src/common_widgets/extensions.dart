import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:kapoow/src/constants/app_paddings.dart';

/// Extension to check if a widget is a sized box with 0 dimensions
extension IsShrunk on Widget {
  /// true if the widget is a Sized box with 0 dimensions
  bool get isShrunk =>
      this is SizedBox &&
      (this as SizedBox).height == 0 &&
      (this as SizedBox).width == 0;
}

/// Useful extension to handle common tasks with Lists of widgets
extension SeparatedX on List<Widget> {
  /// Inserts a Gap of the [mainAxisExtent] size between every element
  List<Widget> separated({
    double mainAxisExtent = AppPaddings.xSmall,
    // ignore: avoid_positional_boolean_parameters
    bool skipShrunk = true,
    bool sliver = false,
    bool addTrailing = false,
  }) =>
      isNotEmpty
          ? where((e) => !(skipShrunk && e.isShrunk))
              .expand(
                (e) => [
                  e,
                  if (sliver)
                    SliverGap(mainAxisExtent)
                  else
                    Gap(mainAxisExtent),
                ],
              )
              .take(length * 2 - (addTrailing ? 0 : 1))
              .toList()
          : [];

  /// Runs [wrapper] callback on every element
  List<Widget> wrappedWith(Widget Function(Widget child) wrapper) =>
      map(wrapper).toList();

  /// Wraps every element with a Padding Widget
  List<Widget> wrappedWithPadding(EdgeInsets padding) => wrappedWith(
        (child) => Padding(
          padding: padding,
          child: child,
        ),
      );
}
