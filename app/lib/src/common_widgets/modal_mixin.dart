import 'package:flutter/material.dart';

/// Mixin used on Widgets to indicate that they can be show as a modal
/// using the [show] method
mixin ModalMixin<T extends Object?> on Widget {
  /// Value returned from [show] method when the dialog returns null
  T get fallbackValue;

  /// Displays the current widget as a modal or dialog
  Future<T> show(BuildContext context) async {
    final result = await showDialog<bool>(
      context: context,
      builder: (context) => this,
    );
    if (result == null) return fallbackValue;
    return result as T;
  }
}
