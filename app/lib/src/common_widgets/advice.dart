import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Tertiary card with an icon and a simple message
class Advice extends StatelessWidget {
  /// Creates an [Advice] instance
  const Advice({
    required this.icon,
    required this.text,
    super.key,
  });

  /// Icon to display at the leading section
  final PhosphorIconData icon;

  /// Advice text
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(AppPaddings.small),
      decoration: BoxDecoration(
        color: context.colorScheme.tertiaryContainer,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        children: [
          PhosphorIcon(icon),
          Expanded(child: Text(text)),
        ].separated(),
      ),
    );
  }
}
