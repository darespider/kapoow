// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'secure_storage_pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$flutterSecureStorageHash() =>
    r'b272fa0dc9342cf5f55a9ab936d20da1410041fd';

/// Provides an instance of [FlutterSecureStorage]
///
/// Copied from [flutterSecureStorage].
@ProviderFor(flutterSecureStorage)
final flutterSecureStoragePod = Provider<FlutterSecureStorage>.internal(
  flutterSecureStorage,
  name: r'flutterSecureStoragePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$flutterSecureStorageHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FlutterSecureStorageRef = ProviderRef<FlutterSecureStorage>;
String _$secureStorageServiceHash() =>
    r'048d90f481253c5b0b977e56d3ea122db90a515d';

/// Provides an instance of [SecureStorageService]
///
/// consumes [flutterSecureStoragePod]
///
/// Copied from [secureStorageService].
@ProviderFor(secureStorageService)
final secureStorageServicePod = Provider<SecureStorageService>.internal(
  secureStorageService,
  name: r'secureStorageServicePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$secureStorageServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SecureStorageServiceRef = ProviderRef<SecureStorageService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
