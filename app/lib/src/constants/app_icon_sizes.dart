/// App Icon sizes
abstract class AppIconSizes {
  /// Value of 16.0
  static const xSmall = 16.0;

  /// Value of 20.0
  static const small = 20.0;

  /// Value of 24.0
  static const medium = 24.0;

  /// Value of 40.0
  static const big = 40.0;

  /// Value of 48.0
  static const large = 48.0;
}
