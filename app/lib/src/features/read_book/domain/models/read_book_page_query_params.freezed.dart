// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_book_page_query_params.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ReadBookPageQueryParams {
  int get page => throw _privateConstructorUsedError;
  ReadContext? get readContext => throw _privateConstructorUsedError;
  String? get readContextId => throw _privateConstructorUsedError;
  bool get incognito => throw _privateConstructorUsedError;

  /// Create a copy of ReadBookPageQueryParams
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ReadBookPageQueryParamsCopyWith<ReadBookPageQueryParams> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReadBookPageQueryParamsCopyWith<$Res> {
  factory $ReadBookPageQueryParamsCopyWith(ReadBookPageQueryParams value,
          $Res Function(ReadBookPageQueryParams) then) =
      _$ReadBookPageQueryParamsCopyWithImpl<$Res, ReadBookPageQueryParams>;
  @useResult
  $Res call(
      {int page,
      ReadContext? readContext,
      String? readContextId,
      bool incognito});
}

/// @nodoc
class _$ReadBookPageQueryParamsCopyWithImpl<$Res,
        $Val extends ReadBookPageQueryParams>
    implements $ReadBookPageQueryParamsCopyWith<$Res> {
  _$ReadBookPageQueryParamsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ReadBookPageQueryParams
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? readContext = freezed,
    Object? readContextId = freezed,
    Object? incognito = null,
  }) {
    return _then(_value.copyWith(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      readContext: freezed == readContext
          ? _value.readContext
          : readContext // ignore: cast_nullable_to_non_nullable
              as ReadContext?,
      readContextId: freezed == readContextId
          ? _value.readContextId
          : readContextId // ignore: cast_nullable_to_non_nullable
              as String?,
      incognito: null == incognito
          ? _value.incognito
          : incognito // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadBookPageQueryParamsImplCopyWith<$Res>
    implements $ReadBookPageQueryParamsCopyWith<$Res> {
  factory _$$ReadBookPageQueryParamsImplCopyWith(
          _$ReadBookPageQueryParamsImpl value,
          $Res Function(_$ReadBookPageQueryParamsImpl) then) =
      __$$ReadBookPageQueryParamsImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int page,
      ReadContext? readContext,
      String? readContextId,
      bool incognito});
}

/// @nodoc
class __$$ReadBookPageQueryParamsImplCopyWithImpl<$Res>
    extends _$ReadBookPageQueryParamsCopyWithImpl<$Res,
        _$ReadBookPageQueryParamsImpl>
    implements _$$ReadBookPageQueryParamsImplCopyWith<$Res> {
  __$$ReadBookPageQueryParamsImplCopyWithImpl(
      _$ReadBookPageQueryParamsImpl _value,
      $Res Function(_$ReadBookPageQueryParamsImpl) _then)
      : super(_value, _then);

  /// Create a copy of ReadBookPageQueryParams
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? readContext = freezed,
    Object? readContextId = freezed,
    Object? incognito = null,
  }) {
    return _then(_$ReadBookPageQueryParamsImpl(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      readContext: freezed == readContext
          ? _value.readContext
          : readContext // ignore: cast_nullable_to_non_nullable
              as ReadContext?,
      readContextId: freezed == readContextId
          ? _value.readContextId
          : readContextId // ignore: cast_nullable_to_non_nullable
              as String?,
      incognito: null == incognito
          ? _value.incognito
          : incognito // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ReadBookPageQueryParamsImpl implements _ReadBookPageQueryParams {
  const _$ReadBookPageQueryParamsImpl(
      {this.page = 1,
      this.readContext,
      this.readContextId,
      this.incognito = false});

  @override
  @JsonKey()
  final int page;
  @override
  final ReadContext? readContext;
  @override
  final String? readContextId;
  @override
  @JsonKey()
  final bool incognito;

  @override
  String toString() {
    return 'ReadBookPageQueryParams(page: $page, readContext: $readContext, readContextId: $readContextId, incognito: $incognito)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadBookPageQueryParamsImpl &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.readContext, readContext) ||
                other.readContext == readContext) &&
            (identical(other.readContextId, readContextId) ||
                other.readContextId == readContextId) &&
            (identical(other.incognito, incognito) ||
                other.incognito == incognito));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, page, readContext, readContextId, incognito);

  /// Create a copy of ReadBookPageQueryParams
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadBookPageQueryParamsImplCopyWith<_$ReadBookPageQueryParamsImpl>
      get copyWith => __$$ReadBookPageQueryParamsImplCopyWithImpl<
          _$ReadBookPageQueryParamsImpl>(this, _$identity);
}

abstract class _ReadBookPageQueryParams implements ReadBookPageQueryParams {
  const factory _ReadBookPageQueryParams(
      {final int page,
      final ReadContext? readContext,
      final String? readContextId,
      final bool incognito}) = _$ReadBookPageQueryParamsImpl;

  @override
  int get page;
  @override
  ReadContext? get readContext;
  @override
  String? get readContextId;
  @override
  bool get incognito;

  /// Create a copy of ReadBookPageQueryParams
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadBookPageQueryParamsImplCopyWith<_$ReadBookPageQueryParamsImpl>
      get copyWith => throw _privateConstructorUsedError;
}
