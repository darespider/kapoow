/// Context on which a book is being read.
enum ReadContext {
  /// The book is part of a custom readlist
  readlist,

  /// The book is part of the series it belongs
  series;
}
