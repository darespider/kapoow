import 'dart:typed_data';

/// Repository use to fetch the bytes of the pages images and thumbnails
abstract class PageBytesRepository {
  /// Fetch the bytes of the page's image
  Future<Uint8List> downloadPage(String bookId, int page);

  /// Fetch the bytes of the page's thumbnail image
  Future<Uint8List> downloadPageThumbnail(String bookId, int page);
}
