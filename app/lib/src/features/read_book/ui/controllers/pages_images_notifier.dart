import 'dart:async';
import 'dart:typed_data';

import 'package:kapoow/src/features/library_management/books/data/books_repository.dart';
import 'package:kapoow/src/features/library_management/books/domain/pods.dart';
import 'package:kapoow/src/features/library_management/books/ui/controllers/pods.dart';
import 'package:kapoow/src/features/read_book/data/page_bytes_repository.dart';
import 'package:kapoow/src/features/read_book/domain/pods.dart';
import 'package:kapoow/src/features/read_book/ui/controllers/book_pages_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pages_images_notifier.g.dart';

/// Notifier used to download and cache the bytes of the images for the pages
/// of the book with the [bookId].
///
/// The state consist of a [Map<int, Uint8List?>] which contains the bytes
/// of every page image.
///
/// If the state is empty that means the notifier hasn't downloaded any page.
///
/// To validate if a page was already tried to be downloaded but failed use
/// [Map.containsKey()] method, this will return `true` if the key exists
/// on the map even when the value is `null`.
@riverpod
class PagesImagesNotifier extends _$PagesImagesNotifier {
  late PageBytesRepository _repo;
  late BooksRepository _booksRepo;
  Timer? _debouncer;

  @override
  BookPagesState build(String bookId) {
    _repo = ref.watch(pageBytesRepoPod);
    _booksRepo = ref.watch(booksRepoPod);
    _init();
    ref.onDispose(() => _debouncer?.cancel());
    return BookPagesState.initial();
  }

  Future<void> _init() async {
    final book = await ref.read(bookPod(bookId).future);
    final currentBookPage = book.readProgress?.page ?? 1;
    final pagesCount = book.media.pagesCount;
    final pages = await _loadPageAndSides(currentBookPage, pagesCount);

    state = BookPagesState(
      pagesCount: pagesCount,
      pages: pages,
    );
  }

  /// Download bytes of the page
  Future<void> loadPage(int pageNumber) async {
    _debouncer?.cancel();
    final delay = state.pages.isNotEmpty
        ? const Duration(milliseconds: 500)
        : Duration.zero;
    _debouncer = Timer(
      delay,
      () async {
        final newPages = await _loadPageAndSides(pageNumber, state.pagesCount);
        state = state.copyWith(
          pages: {
            ...state.pages,
            ...newPages,
          },
        );
        // Update progress only if the page could be fetched
        await _booksRepo.updateBookProgress(bookId, page: pageNumber);
      },
    );
  }

  Future<Map<int, Uint8List?>> _loadPageAndSides(
    int pageNumber,
    int pagesCount,
  ) async {
    final startingPage = pageNumber - 2;

    final missingPages = List.generate(5, (i) => startingPage + i)
        .where((page) => page >= 1 && page <= pagesCount)
        .where((page) => !state.pages.containsKey(page));

    final pagesBytes = await Future.wait(
      missingPages.map((page) => _safePageDownload(bookId, page)),
    );

    return Map.fromIterables(missingPages, pagesBytes);
  }

  Future<Uint8List?> _safePageDownload(String bookId, int page) async {
    Uint8List? bytes;
    try {
      bytes = await _repo.downloadPage(bookId, page);
    } catch (_) {
      // no-op
      // there is nothing we can do if the page fails to load,
      // let user refresh the page
    }
    return bytes;
  }
}
