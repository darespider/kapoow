import 'dart:typed_data';

/// State used by the ReadBookScreen
class BookPagesState {
  /// Creates an instance of [BookPagesState]
  BookPagesState({
    required this.pagesCount,
    required this.pages,
  });

  /// Creates an empty instance of [BookPagesState]
  BookPagesState.initial()
      : pagesCount = 0,
        pages = {};

  /// Quantity of pages the book has
  final int pagesCount;

  /// Maps with the page bytes by page number
  final Map<int, Uint8List?> pages;

  /// Returns a new instance based on `this`
  BookPagesState copyWith({
    int? pagesCount,
    Map<int, Uint8List?>? pages,
  }) =>
      BookPagesState(
        pagesCount: pagesCount ?? this.pagesCount,
        pages: pages ?? this.pages,
      );
}
