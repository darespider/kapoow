import 'package:flutter/material.dart';
import 'package:kapoow/src/features/library_management/read_lists/ui/screens/readlist_screen.dart';
import 'package:kapoow/src/features/library_management/series/ui/screens/series_screen.dart';
import 'package:kapoow/src/features/read_book/domain/models/change_book_direction.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';

/// Displays a page allowing the user to read the next or previous book based on
/// the [direction]. It also allows the user to return to [SeriesScreen] or
/// [ReadlistScreen] screen based on the [readContext]
class ChangeBookScreen extends StatelessWidget {
  /// Creates a [ChangeBookScreen] instance
  const ChangeBookScreen({
    required this.bookId,
    required this.direction,
    this.readContext,
    this.readContextId,
    super.key,
  }) : assert(
          readContext != ReadContext.readlist || readContextId != null,
          'If a [ReadContext.readlist] is provided the [readContextId] '
          'should not be null',
        );

  /// Id of the current book used to fetch the next or previous book.
  final String bookId;

  /// Context on which the book will be open
  final ReadContext? readContext;

  /// Id of the read list of the [readContext]
  final String? readContextId;

  /// Indicates if we are going to the next or the previous book
  final ChangeBookDirection direction;

  @override
  Widget build(BuildContext context) {
    // TODO(rurickdev): [Beta] Finish the page
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Text('Open the $direction book'),
            Text('on the Context $readContext'),
            Text('with id $readContextId'),
          ],
        ),
      ),
    );
  }
}
