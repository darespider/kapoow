import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/ui/controllers/pods.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_book_page_query_params.dart';
import 'package:kapoow/src/features/read_book/ui/widgets/book_info_bottom_sheet.dart';
import 'package:kapoow/src/features/read_book/ui/widgets/book_pages_images_carousel.dart';
import 'package:kapoow/src/features/read_book/ui/widgets/book_pages_thumbnails_carousel.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Full screen page that displays a single page at a time and navigate
/// between pages with scroll.
///
/// Will navigate to [ChangeBookRoute] when reaching the last page + 1 or
/// first page - 1.
///
/// Allow to see a carousel of miniature pages as a preview and see the book
/// details.
class ReadBookScreen extends ConsumerWidget {
  /// Creates a [ReadBookScreen] instance
  const ReadBookScreen({
    required this.bookId,
    required this.params,
    super.key,
  });

  /// Query params from the route to display the right book page
  final ReadBookPageQueryParams params;

  /// Id of the book to fetch data from
  final String bookId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final bookAsync = ref.watch(bookPod(bookId));

    return Material(
      child: TypicalWhenBuilder(
        asyncValue: bookAsync,
        dataBuilder: (context, book) => _ReadBookScreenBody(
          book: book,
          params: params,
        ),
      ),
    );
  }
}

class _ReadBookScreenBody extends HookConsumerWidget {
  const _ReadBookScreenBody({
    required this.book,
    required this.params,
  });

  final Book book;
  final ReadBookPageQueryParams params;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pageController = usePageController(
      initialPage: params.page - 1,
    );
    final showHood = useState(false);

    void toggleHood() => showHood.value = !showHood.value;

    return Scaffold(
      extendBodyBehindAppBar: true,
      // TODO(rurickdev): [Enhancement] Read value from app settings
      backgroundColor: Colors.black,
      appBar: showHood.value
          ? AppBar(
              title: Text(
                book.metadata.title,
                overflow: TextOverflow.ellipsis,
              ),
              actions: [
                // TODO(rurickdev): [Enhancement] Add favorite toggle button
                IconButton(
                  onPressed: () => BookInfoBottomSheet.showBottomSheet(
                    context,
                    book: book,
                    page: params.page,
                  ),
                  icon: const PhosphorIcon(PhosphorIconsDuotone.info),
                ),
              ],
            )
          : null,
      body: Stack(
        children: [
          BookPagesImagesCarousel(
            bookId: book.id,
            params: params,
            controller: pageController,
            onTap: (_) => toggleHood(),
          ),
          // TODO(rurickdev): [Enhancement] animate with fade in/out
          if (showHood.value) ...[
            GestureDetector(
              onTap: toggleHood,
              child: ColoredBox(
                // TODO(rurickdev): [Enhancement] Read value from app settings
                color: Colors.black.withOpacity(0.70),
                child: const SizedBox.expand(),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: BookPagesThumbnailsCarousel(
                book: book,
                selectedPage: params.page,
                onPageTap: (page) {
                  pageController.animateToPage(
                    page - 1,
                    duration: Durations.long2,
                    curve: Curves.easeIn,
                  );
                  toggleHood();
                },
              ),
            ),
          ],
        ],
      ),
    );
  }
}
