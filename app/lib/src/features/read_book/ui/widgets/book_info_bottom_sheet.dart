import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:kapoow/src/common_widgets/app_chip.dart';
import 'package:kapoow/src/common_widgets/app_gap.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_number_badge.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/read_book_button.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Displays a simple column with a [book] info section and two button
/// to read the current book on incognito or see the series the book is part
/// of.
class BookInfoBottomSheet extends StatelessWidget {
  /// Creates a [BookInfoBottomSheet] instance
  const BookInfoBottomSheet({
    required this.book,
    required this.page,
    this.readContext,
    this.readContextId,
    super.key,
  });

  /// Context on which the book will be open
  final ReadContext? readContext;

  /// Id of the read list of the [readContext]
  final String? readContextId;

  /// [Book] to show the details
  final Book book;

  /// Current page the book is at
  final int page;

  /// Displays a Modal Bottom Sheet with as [BookInfoBottomSheet] body
  static Future<void> showBottomSheet(
    BuildContext context, {
    required Book book,
    required int page,
  }) {
    return showModalBottomSheet<void>(
      context: context,
      builder: (context) => Padding(
        padding: const EdgeInsets.all(12),
        child: BookInfoBottomSheet(book: book, page: page),
      ),
    );
  }

  // TODO(rurickdev): [Enhancement] Add a modal view for desktop
  //  Perhaps show the BookDetailsScreen on the side

  @override
  Widget build(BuildContext context) {
    final pagesCount = book.media.pagesCount;
    final dateString = '${book.metadata.releaseDate ?? '???'}';
    final title = book.metadata.title;
    final isSameTitle = title.contains(book.seriesTitle);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ConstrainedBox(
          constraints: const BoxConstraints(
            maxHeight: 200,
          ),
          child: Row(
            children: [
              CoverImage(coverUrl: book.thumbnailUri),
              const AppGap.small(),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (!isSameTitle)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            book.seriesTitle,
                            overflow: TextOverflow.ellipsis,
                            style: context.textTheme.titleSmall,
                          ),
                          BookNumberBadge(number: book.number),
                        ],
                      ),
                    Expanded(
                      child: Text(
                        title,
                        overflow: TextOverflow.ellipsis,
                        style: context.textTheme.titleLarge,
                        maxLines: 3,
                      ),
                    ),
                    const AppGap.xSmall(),
                    Column(
                      children: [
                        Row(
                          children: [
                            PhosphorIcon(
                              PhosphorIcons.calendar(
                                PhosphorIconsStyle.duotone,
                              ),
                            ),
                            const AppGap.xSmall(),
                            Text(dateString),
                          ],
                        ),
                        const AppGap.xxSmall(),
                        Row(
                          children: [
                            PhosphorIcon(
                              PhosphorIcons.bookOpenText(
                                PhosphorIconsStyle.duotone,
                              ),
                            ),
                            const AppGap.xSmall(),
                            Text('$page / $pagesCount'),
                          ],
                        ),
                        const AppGap.xxSmall(),
                        Row(
                          children: [
                            PhosphorIcon(
                              PhosphorIcons.fileArchive(
                                PhosphorIconsStyle.duotone,
                              ),
                            ),
                            const AppGap.xSmall(),
                            AppChip(
                              text: book.media.mediaType.split('/').last,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        const Gap(12),
        Row(
          children: [
            Expanded(
              child: OutlinedButton.icon(
                onPressed: () => switch (readContext) {
                  ReadContext.readlist =>
                    ReadlistRoute(id: readContextId!).go(context),
                  _ => SeriesRoute(id: book.seriesId).go(context)
                },
                icon: PhosphorIcon(PhosphorIcons.books()),
                // TODO(rurickdev): [l10n] translate
                label: const Text('Go To Series'),
              ),
            ),
            const AppGap.xSmall(),
            Expanded(child: ReadBookButton.incognito(book: book)),
          ],
        ),
      ],
    );
  }
}
