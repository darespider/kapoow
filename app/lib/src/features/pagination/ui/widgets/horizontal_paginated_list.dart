import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/paginated_list.dart';

/// Displays an "infinite" list of items T.
class HorizontalPaginatedList<T> extends PaginatedList<T> {
  /// Creates a [HorizontalPaginatedList] instance
  HorizontalPaginatedList({
    required super.paginatedState,
    required super.nextPageLoader,
    required super.itemBuilder,
    super.loadNextPage,
    super.trailingBuilder,
    Widget? loadingPageIndicator,
    Widget? errorInPageIndicator,
    Widget? separator,
    super.key,
  }) : super(
          loadingPageIndicator: loadingPageIndicator ??
              const Center(
                child: AppLoader(
                  scale: 75,
                ),
              ),
          errorInPageIndicator: errorInPageIndicator ??
              Card(
                child: Center(
                  child: Text(
                    paginatedState.errorLoadingPage?.toString() ??
                        'HorizontalPaginatedList Error',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  ),
                ),
              ),
          separator: separator ?? const Gap(AppPaddings.small),
          scrollDirection: Axis.horizontal,
        );

  /// Creates a [HorizontalPaginatedList] which Paginated state will
  /// have only one page
  HorizontalPaginatedList.singlePage({
    required Pagination<T> page,
    required super.itemBuilder,
    Widget? separator,
    super.trailingBuilder,
    super.key,
  }) : super(
          paginatedState: PaginatedState(pages: [page]),
          loadNextPage: (_) => false,
          nextPageLoader: () {},
          loadingPageIndicator: const SizedBox.shrink(),
          errorInPageIndicator: const SizedBox.shrink(),
          separator: separator ?? const Gap(AppPaddings.small),
          scrollDirection: Axis.horizontal,
        );
}
