import 'package:flutter/material.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';

/// Displays an "infinite" list of items T.
class PaginatedList<T> extends StatelessWidget {
  /// Creates a [PaginatedList] instance
  const PaginatedList({
    required this.paginatedState,
    required this.nextPageLoader,
    required this.itemBuilder,
    required this.loadingPageIndicator,
    required this.errorInPageIndicator,
    required this.separator,
    required this.scrollDirection,
    this.loadNextPage,
    this.trailingBuilder,
    this.initialLoadIndicator,
    super.key,
  });

  /// {@template kapoow.pagination.paginatedList.paginatedState}
  /// State from where to get pagination data and content
  /// {@endtemplate}
  final PaginatedState<T> paginatedState;

  /// {@template kapoow.pagination.paginatedList.nextPageLoader}
  /// Callback to trigger the loading of the next page
  /// {@endtemplate}
  final VoidCallback nextPageLoader;

  /// {@template kapoow.pagination.paginatedList.loadNextPage}
  /// Callback called on every item render to know if the [loadNextPage]
  /// callback should be called
  /// {@endtemplate}
  final bool Function(int index)? loadNextPage;

  /// {@template kapoow.pagination.paginatedList.loadingPageIndicator}
  /// Widget to display the loading next page state
  /// {@endtemplate}
  final Widget loadingPageIndicator;

  /// {@template kapoow.pagination.paginatedList.initialLoadIndicator}
  /// Widget to display the loading the first page and there is no error state.
  /// if not provided [loadingPageIndicator] will be used instead
  /// {@endtemplate}
  final Widget? initialLoadIndicator;

  /// {@template kapoow.pagination.paginatedList.errorInPageIndicator}
  /// Widget to display when there is an error loading the next page
  /// {@endtemplate}
  final Widget errorInPageIndicator;

  /// {@template kapoow.pagination.paginatedList.separator}
  /// Widget to display between the element of the list
  /// {@endtemplate}
  final Widget separator;

  /// {@template kapoow.pagination.paginatedList.itemBuilder}
  /// Builder for list items
  /// {@endtemplate}
  final Widget Function(BuildContext context, T item) itemBuilder;

  /// {@template kapoow.pagination.paginatedList.scrollDirection}
  /// Direction of the scroll
  /// {@endtemplate}
  final Axis scrollDirection;

  /// If provided the list will add a last item returned by this callback
  final WidgetBuilder? trailingBuilder;

  bool _defaultShouldLoadNextPage(int i) =>
      paginatedState.hasNextPage &&
      i == (paginatedState.content.length * 0.8).round();

  @override
  Widget build(BuildContext context) {
    if (paginatedState.isLoadingFirstPage) {
      return initialLoadIndicator ?? loadingPageIndicator;
    }

    var itemCount = paginatedState.content.length;
    if (paginatedState.isLoading ||
        paginatedState.hasError ||
        trailingBuilder != null) {
      itemCount++;
    }

    return ListView.separated(
      itemCount: itemCount,
      scrollDirection: scrollDirection,
      separatorBuilder: (context, index) => separator,
      padding: scrollDirection == Axis.horizontal
          ? const EdgeInsets.symmetric(horizontal: AppPaddings.small)
          : null,
      itemBuilder: (context, index) {
        if (paginatedState.hasError) return errorInPageIndicator;
        if (loadNextPage?.call(index) ?? _defaultShouldLoadNextPage(index)) {
          Future(nextPageLoader);
        }

        if (index == itemCount - 1) {
          if (paginatedState.isLoading) return loadingPageIndicator;
          if (trailingBuilder != null) return trailingBuilder!.call(context);
        }

        return itemBuilder(context, paginatedState.content[index]);
      },
    );
  }
}
