import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/pagination/domain/models/sort.dart';

part 'pageable.freezed.dart';

/// Page information for pagination
@freezed
class Pageable with _$Pageable {
  /// Creates a [Pageable] instance
  const factory Pageable({
    required Sort sort,
    required bool paged,
    required bool unpaged,
    required int pageSize,
    required int pageNumber,
    required int offset,
  }) = _Pageable;
}
