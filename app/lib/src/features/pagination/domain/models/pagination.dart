import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/pagination/domain/models/pageable.dart';
import 'package:kapoow/src/features/pagination/domain/models/sort.dart';

part 'pagination.freezed.dart';

@Freezed(
  toJson: false,
  fromJson: false,
  copyWith: true,
)

/// Representation of the Pagination of the app
class Pagination<T> with _$Pagination<T> {
  /// Creates a [Pagination] instance
  const factory Pagination({
    required Pageable pageable,
    required bool last,
    required int totalElements,
    required int totalPages,
    required int size,
    required int number,
    required Sort sort,
    required bool first,
    required int numberOfElements,
    required bool empty,
    @Default([]) List<T> content,
  }) = _Pagination<T>;
}
