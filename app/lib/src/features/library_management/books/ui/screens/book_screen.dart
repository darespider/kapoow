import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/ui/controllers/pods.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_details_card.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_file_info_card.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_screen_header.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/read_book_button.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Screen to display the details of a book
class BookScreen extends ConsumerWidget {
  /// Creates instance of [BookScreen] widget
  const BookScreen({
    required this.bookId,
    this.readContext,
    this.readContextId,
    super.key,
  }) : assert(
          readContext != ReadContext.readlist || readContextId != null,
          'If a [ReadContext.readlist] is provided the [readContextId] '
          'should not be null',
        );

  /// id of the item to get
  final String bookId;

  /// Context on which the book will be open
  final ReadContext? readContext;

  /// Id of the read list of the [readContext]
  final String? readContextId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final bookAsync = ref.watch(bookPod(bookId));

    // TODO(rurickdev): [Enhancement] On Horizontal scroll gesture navigate to
    //  next/previouis book based on read context context

    return TypicalWhenBuilder(
      asyncValue: bookAsync,
      dataBuilder: (context, book) => Scaffold(
        persistentFooterButtons: [
          ReadBookButton.incognito(book: book, secondary: true),
          ReadBookButton(book: book),
        ],
        body: ListView(
          children: [
            SizedBox(
              height: context.height / 3,
              width: context.width,
              child: BookScreenHeader(book: book),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: AppPaddings.small),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  BookDetailsCard(book: book),
                  BookFileInfoCard(book: book),
                ].separated(mainAxisExtent: AppPaddings.medium),
              ),
            ),
          ].separated(
            mainAxisExtent: AppPaddings.medium,
            addTrailing: true,
          ),
        ),
      ),
    );
  }
}
