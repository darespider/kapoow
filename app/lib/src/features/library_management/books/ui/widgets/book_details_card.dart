import 'package:flutter/material.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_number_badge.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_summary.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Card that displays the books details like, title, series, pages count, etc
class BookDetailsCard extends StatelessWidget {
  /// Creates a [BookDetailsCard] instance
  const BookDetailsCard({required this.book, super.key});

  /// Book to fetch info from
  final Book book;

  @override
  Widget build(BuildContext context) {
    final divider = Divider(
      color: context.colorScheme.onSurfaceVariant.withOpacity(0.4),
    );

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(AppPaddings.small),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  book.seriesTitle,
                  style: context.textTheme.titleMedium,
                ),
                BookNumberBadge(number: book.number),
              ],
            ),
            Text(
              book.name,
              style: context.textTheme.headlineMedium,
            ),
            divider,
            // TODO(rurickdev): [l10n] translate
            DefaultTextStyle.merge(
              style: context.textTheme.titleMedium,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (book.metadata.releaseDate != null)
                    // TODO(rurickdev): [Beta] Format date
                    Text('Released: ${book.metadata.releaseDate}')
                  else
                    const SizedBox.shrink(),
                  Text(
                    '${book.readProgress?.page ?? 0} of '
                    '${book.media.pagesCount} Pages',
                  ),
                ],
              ),
            ),

            divider,
            ItemSummary(summary: book.metadata.summary),
            if (book.metadata.tags.isNotEmpty) ...[
              divider,
              Text(
                book.metadata.tags.join(', '),
              ),
            ],
            if (book.metadata.authors.isNotEmpty) ...[
              divider,
              for (final author in book.metadata.authors)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('${author.role.toUpperCase()}:'),
                    Text(author.name),
                  ],
                ),
            ],
          ],
        ),
      ),
    );
  }
}
