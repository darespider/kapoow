import 'package:flutter/material.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Card with the file info of a book
class BookFileInfoCard extends StatelessWidget {
  /// Creates a [BookFileInfoCard] instance
  const BookFileInfoCard({required this.book, super.key});

  /// Book to fetch info from
  final Book book;

  @override
  Widget build(BuildContext context) {
    final divider = Divider(
      color: context.colorScheme.onSurfaceVariant.withOpacity(0.4),
    );

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(AppPaddings.small),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // TODO(rurickdev): [l10n] translate
            Text(
              'File Info',
              style: context.textTheme.headlineMedium,
            ),
            divider,
            Text('Size: ${book.size}'),
            Text('Format: ${book.media.mediaType}'),
            // TODO(rurickdev): Add links?
          ],
        ),
      ),
    );
  }
}
