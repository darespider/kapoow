import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/rest_api/ui/widgets/cached_api_image_decorated_box.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Header for the Book Details Screen.
///
/// Displays a blur version of the cover image form a book and another smaller
/// but cleaner version of the cover on top of it. Also includes a Back button
class BookScreenHeader extends StatelessWidget {
  /// Creates a [BookScreenHeader] instance
  const BookScreenHeader({
    required this.book,
    super.key,
  });

  /// Book to fetch cover image from
  final Book book;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CachedApiImageDecoratedBox(
          relativeUrl: book.thumbnailUri,
          child: SizedBox(
            height: context.height / 3.5,
            width: context.width,
          ),
        ),
        BackdropFilter(
          // TODO(rurickdev): [Enhancement] replace or mix with dots filter
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.45),
              gradient: const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.center,
                stops: [0.2, 0.5, 0.75, 1],
                colors: [
                  Colors.black,
                  Colors.black,
                  Colors.black,
                  Colors.transparent,
                ],
              ),
            ),
            child: const SizedBox.expand(),
          ),
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Gap(AppPaddings.small),
              SizedBox(
                height: context.height / 3.5,
                child: AbsorbPointer(
                  child: BookCover(
                    book: book,
                    hideDecoration: true,
                  ),
                ),
              ),
            ],
          ),
        ),
        const Positioned(
          top: AppPaddings.small,
          left: AppPaddings.small,
          child: BackButton(),
        ),
      ],
    );
  }
}
