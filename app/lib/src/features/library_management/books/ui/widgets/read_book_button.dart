import 'package:flutter/material.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Button that follows the app style to open the [ReadBookRoute] and start
/// reading the [book]. It can start reading as incognito by using the
/// [ReadBookButton.incognito] constructor.
class ReadBookButton extends StatelessWidget {
  /// Creates a [ReadBookButton] instance that will navigate to [ReadBookRoute]
  /// allowing the record of the read progress.
  const ReadBookButton({
    required this.book,
    this.readContext,
    this.readContextId,
    this.secondary = false,
    super.key,
  }) : _mode = _ReadMode.publicly;

  /// Creates a [ReadBookButton] instance that will navigate to [ReadBookRoute]
  /// preventing the record of the read progress.
  const ReadBookButton.incognito({
    required this.book,
    this.readContext,
    this.readContextId,
    this.secondary = false,
    super.key,
  }) : _mode = _ReadMode.privately;

  /// Context on which the book will be open
  final ReadContext? readContext;

  /// Id of the read list of the [readContext]
  final String? readContextId;

  /// Book to open for reading
  final Book book;

  /// Indicates if the button is used on a context with other more important
  /// call to action.
  final bool secondary;

  final _ReadMode _mode;

  @override
  Widget build(BuildContext context) {
    assert(
      readContext != ReadContext.readlist || readContextId != null,
      'If a [ReadContext.readlist] is provided the [readContextId] '
      'should not be null',
    );

    final colorScheme = context.colorScheme;
    const iconStyle = PhosphorIconsStyle.duotone;
    final styleData = switch (_mode) {
      _ReadMode.publicly => _ReadButtonStyleData(
          icon: PhosphorIcons.bookOpenText(iconStyle),
          background: colorScheme.primary,

          foreground: colorScheme.onPrimary,
          // TODO(rurickdev): [l10n] translate
          label: 'start reading',
        ),
      _ReadMode.privately => _ReadButtonStyleData(
          icon: PhosphorIcons.detective(iconStyle),
          background: colorScheme.tertiary,

          foreground: colorScheme.onTertiary,
          // TODO(rurickdev): [l10n] translate
          label: 'read privately',
          incognito: true,
        ),
    };

    void onTap() => ReadBookRoute(
          id: book.id,
          // TODO(rurickdev): [Enhancement] Add setting to open incognito
          //  from page 1 or from read progress
          page: book.readProgress?.page ?? 1,
          context: readContext,
          contextId: readContextId,
          incognito: styleData.incognito,
        ).go(context);

    final icon = PhosphorIcon(styleData.icon);

    final label = Text(styleData.label.toProperCase());

    if (secondary) {
      return OutlinedButton.icon(
        onPressed: onTap,
        style: OutlinedButton.styleFrom(
          foregroundColor: styleData.background,
        ),
        icon: icon,
        label: label,
      );
    }

    return FilledButton.icon(
      onPressed: onTap,
      style: FilledButton.styleFrom(
        backgroundColor: styleData.background,
        foregroundColor: styleData.foreground,
      ),
      icon: icon,
      label: label,
    );
  }
}

enum _ReadMode {
  publicly,
  privately;
}

class _ReadButtonStyleData {
  _ReadButtonStyleData({
    required this.icon,
    required this.background,
    required this.foreground,
    required this.label,
    this.incognito = false,
  });

  final PhosphorIconData icon;
  final Color background;
  final Color foreground;
  final bool incognito;
  final String label;
}
