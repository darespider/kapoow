import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the book data from an api call
@Riverpod(keepAlive: true)
FutureOr<Book> book(BookRef ref, String id) =>
    ref.watch(booksRepoPod).getBook(id);
