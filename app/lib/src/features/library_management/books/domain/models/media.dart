import 'package:freezed_annotation/freezed_annotation.dart';

part 'media.freezed.dart';

/// Media information of a book
@freezed
class Media with _$Media {
  /// Creates a [Media] instance
  const factory Media({
    required String status,
    required String mediaType,
    required int pagesCount,
    required String comment,
  }) = _Media;
}
