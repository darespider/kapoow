// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_metadata.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BookMetadata {
  String get title => throw _privateConstructorUsedError;
  bool get titleLock => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  bool get summaryLock => throw _privateConstructorUsedError;
  double get number => throw _privateConstructorUsedError;
  bool get numberLock => throw _privateConstructorUsedError;
  double get numberSort => throw _privateConstructorUsedError;
  bool get numberSortLock => throw _privateConstructorUsedError;
  bool get releaseDateLock => throw _privateConstructorUsedError;
  List<Author> get authors => throw _privateConstructorUsedError;
  bool get authorsLock => throw _privateConstructorUsedError;
  Set<String> get tags => throw _privateConstructorUsedError;
  bool get tagsLock => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  String get isbn => throw _privateConstructorUsedError;
  bool get isbnLock => throw _privateConstructorUsedError;
  bool get linksLock => throw _privateConstructorUsedError;
  List<WebLink> get links => throw _privateConstructorUsedError;
  DateTime? get releaseDate => throw _privateConstructorUsedError;

  /// Create a copy of BookMetadata
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $BookMetadataCopyWith<BookMetadata> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookMetadataCopyWith<$Res> {
  factory $BookMetadataCopyWith(
          BookMetadata value, $Res Function(BookMetadata) then) =
      _$BookMetadataCopyWithImpl<$Res, BookMetadata>;
  @useResult
  $Res call(
      {String title,
      bool titleLock,
      String summary,
      bool summaryLock,
      double number,
      bool numberLock,
      double numberSort,
      bool numberSortLock,
      bool releaseDateLock,
      List<Author> authors,
      bool authorsLock,
      Set<String> tags,
      bool tagsLock,
      DateTime created,
      DateTime lastModified,
      String isbn,
      bool isbnLock,
      bool linksLock,
      List<WebLink> links,
      DateTime? releaseDate});
}

/// @nodoc
class _$BookMetadataCopyWithImpl<$Res, $Val extends BookMetadata>
    implements $BookMetadataCopyWith<$Res> {
  _$BookMetadataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of BookMetadata
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? titleLock = null,
    Object? summary = null,
    Object? summaryLock = null,
    Object? number = null,
    Object? numberLock = null,
    Object? numberSort = null,
    Object? numberSortLock = null,
    Object? releaseDateLock = null,
    Object? authors = null,
    Object? authorsLock = null,
    Object? tags = null,
    Object? tagsLock = null,
    Object? created = null,
    Object? lastModified = null,
    Object? isbn = null,
    Object? isbnLock = null,
    Object? linksLock = null,
    Object? links = null,
    Object? releaseDate = freezed,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      titleLock: null == titleLock
          ? _value.titleLock
          : titleLock // ignore: cast_nullable_to_non_nullable
              as bool,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      summaryLock: null == summaryLock
          ? _value.summaryLock
          : summaryLock // ignore: cast_nullable_to_non_nullable
              as bool,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as double,
      numberLock: null == numberLock
          ? _value.numberLock
          : numberLock // ignore: cast_nullable_to_non_nullable
              as bool,
      numberSort: null == numberSort
          ? _value.numberSort
          : numberSort // ignore: cast_nullable_to_non_nullable
              as double,
      numberSortLock: null == numberSortLock
          ? _value.numberSortLock
          : numberSortLock // ignore: cast_nullable_to_non_nullable
              as bool,
      releaseDateLock: null == releaseDateLock
          ? _value.releaseDateLock
          : releaseDateLock // ignore: cast_nullable_to_non_nullable
              as bool,
      authors: null == authors
          ? _value.authors
          : authors // ignore: cast_nullable_to_non_nullable
              as List<Author>,
      authorsLock: null == authorsLock
          ? _value.authorsLock
          : authorsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      tags: null == tags
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      tagsLock: null == tagsLock
          ? _value.tagsLock
          : tagsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      isbn: null == isbn
          ? _value.isbn
          : isbn // ignore: cast_nullable_to_non_nullable
              as String,
      isbnLock: null == isbnLock
          ? _value.isbnLock
          : isbnLock // ignore: cast_nullable_to_non_nullable
              as bool,
      linksLock: null == linksLock
          ? _value.linksLock
          : linksLock // ignore: cast_nullable_to_non_nullable
              as bool,
      links: null == links
          ? _value.links
          : links // ignore: cast_nullable_to_non_nullable
              as List<WebLink>,
      releaseDate: freezed == releaseDate
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BookMetadataImplCopyWith<$Res>
    implements $BookMetadataCopyWith<$Res> {
  factory _$$BookMetadataImplCopyWith(
          _$BookMetadataImpl value, $Res Function(_$BookMetadataImpl) then) =
      __$$BookMetadataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String title,
      bool titleLock,
      String summary,
      bool summaryLock,
      double number,
      bool numberLock,
      double numberSort,
      bool numberSortLock,
      bool releaseDateLock,
      List<Author> authors,
      bool authorsLock,
      Set<String> tags,
      bool tagsLock,
      DateTime created,
      DateTime lastModified,
      String isbn,
      bool isbnLock,
      bool linksLock,
      List<WebLink> links,
      DateTime? releaseDate});
}

/// @nodoc
class __$$BookMetadataImplCopyWithImpl<$Res>
    extends _$BookMetadataCopyWithImpl<$Res, _$BookMetadataImpl>
    implements _$$BookMetadataImplCopyWith<$Res> {
  __$$BookMetadataImplCopyWithImpl(
      _$BookMetadataImpl _value, $Res Function(_$BookMetadataImpl) _then)
      : super(_value, _then);

  /// Create a copy of BookMetadata
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? titleLock = null,
    Object? summary = null,
    Object? summaryLock = null,
    Object? number = null,
    Object? numberLock = null,
    Object? numberSort = null,
    Object? numberSortLock = null,
    Object? releaseDateLock = null,
    Object? authors = null,
    Object? authorsLock = null,
    Object? tags = null,
    Object? tagsLock = null,
    Object? created = null,
    Object? lastModified = null,
    Object? isbn = null,
    Object? isbnLock = null,
    Object? linksLock = null,
    Object? links = null,
    Object? releaseDate = freezed,
  }) {
    return _then(_$BookMetadataImpl(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      titleLock: null == titleLock
          ? _value.titleLock
          : titleLock // ignore: cast_nullable_to_non_nullable
              as bool,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      summaryLock: null == summaryLock
          ? _value.summaryLock
          : summaryLock // ignore: cast_nullable_to_non_nullable
              as bool,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as double,
      numberLock: null == numberLock
          ? _value.numberLock
          : numberLock // ignore: cast_nullable_to_non_nullable
              as bool,
      numberSort: null == numberSort
          ? _value.numberSort
          : numberSort // ignore: cast_nullable_to_non_nullable
              as double,
      numberSortLock: null == numberSortLock
          ? _value.numberSortLock
          : numberSortLock // ignore: cast_nullable_to_non_nullable
              as bool,
      releaseDateLock: null == releaseDateLock
          ? _value.releaseDateLock
          : releaseDateLock // ignore: cast_nullable_to_non_nullable
              as bool,
      authors: null == authors
          ? _value._authors
          : authors // ignore: cast_nullable_to_non_nullable
              as List<Author>,
      authorsLock: null == authorsLock
          ? _value.authorsLock
          : authorsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      tags: null == tags
          ? _value._tags
          : tags // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      tagsLock: null == tagsLock
          ? _value.tagsLock
          : tagsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      isbn: null == isbn
          ? _value.isbn
          : isbn // ignore: cast_nullable_to_non_nullable
              as String,
      isbnLock: null == isbnLock
          ? _value.isbnLock
          : isbnLock // ignore: cast_nullable_to_non_nullable
              as bool,
      linksLock: null == linksLock
          ? _value.linksLock
          : linksLock // ignore: cast_nullable_to_non_nullable
              as bool,
      links: null == links
          ? _value._links
          : links // ignore: cast_nullable_to_non_nullable
              as List<WebLink>,
      releaseDate: freezed == releaseDate
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc

class _$BookMetadataImpl implements _BookMetadata {
  const _$BookMetadataImpl(
      {required this.title,
      required this.titleLock,
      required this.summary,
      required this.summaryLock,
      required this.number,
      required this.numberLock,
      required this.numberSort,
      required this.numberSortLock,
      required this.releaseDateLock,
      required final List<Author> authors,
      required this.authorsLock,
      required final Set<String> tags,
      required this.tagsLock,
      required this.created,
      required this.lastModified,
      required this.isbn,
      required this.isbnLock,
      required this.linksLock,
      required final List<WebLink> links,
      this.releaseDate})
      : _authors = authors,
        _tags = tags,
        _links = links;

  @override
  final String title;
  @override
  final bool titleLock;
  @override
  final String summary;
  @override
  final bool summaryLock;
  @override
  final double number;
  @override
  final bool numberLock;
  @override
  final double numberSort;
  @override
  final bool numberSortLock;
  @override
  final bool releaseDateLock;
  final List<Author> _authors;
  @override
  List<Author> get authors {
    if (_authors is EqualUnmodifiableListView) return _authors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_authors);
  }

  @override
  final bool authorsLock;
  final Set<String> _tags;
  @override
  Set<String> get tags {
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_tags);
  }

  @override
  final bool tagsLock;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final String isbn;
  @override
  final bool isbnLock;
  @override
  final bool linksLock;
  final List<WebLink> _links;
  @override
  List<WebLink> get links {
    if (_links is EqualUnmodifiableListView) return _links;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_links);
  }

  @override
  final DateTime? releaseDate;

  @override
  String toString() {
    return 'BookMetadata(title: $title, titleLock: $titleLock, summary: $summary, summaryLock: $summaryLock, number: $number, numberLock: $numberLock, numberSort: $numberSort, numberSortLock: $numberSortLock, releaseDateLock: $releaseDateLock, authors: $authors, authorsLock: $authorsLock, tags: $tags, tagsLock: $tagsLock, created: $created, lastModified: $lastModified, isbn: $isbn, isbnLock: $isbnLock, linksLock: $linksLock, links: $links, releaseDate: $releaseDate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BookMetadataImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.titleLock, titleLock) ||
                other.titleLock == titleLock) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryLock, summaryLock) ||
                other.summaryLock == summaryLock) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.numberLock, numberLock) ||
                other.numberLock == numberLock) &&
            (identical(other.numberSort, numberSort) ||
                other.numberSort == numberSort) &&
            (identical(other.numberSortLock, numberSortLock) ||
                other.numberSortLock == numberSortLock) &&
            (identical(other.releaseDateLock, releaseDateLock) ||
                other.releaseDateLock == releaseDateLock) &&
            const DeepCollectionEquality().equals(other._authors, _authors) &&
            (identical(other.authorsLock, authorsLock) ||
                other.authorsLock == authorsLock) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.tagsLock, tagsLock) ||
                other.tagsLock == tagsLock) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.isbn, isbn) || other.isbn == isbn) &&
            (identical(other.isbnLock, isbnLock) ||
                other.isbnLock == isbnLock) &&
            (identical(other.linksLock, linksLock) ||
                other.linksLock == linksLock) &&
            const DeepCollectionEquality().equals(other._links, _links) &&
            (identical(other.releaseDate, releaseDate) ||
                other.releaseDate == releaseDate));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        title,
        titleLock,
        summary,
        summaryLock,
        number,
        numberLock,
        numberSort,
        numberSortLock,
        releaseDateLock,
        const DeepCollectionEquality().hash(_authors),
        authorsLock,
        const DeepCollectionEquality().hash(_tags),
        tagsLock,
        created,
        lastModified,
        isbn,
        isbnLock,
        linksLock,
        const DeepCollectionEquality().hash(_links),
        releaseDate
      ]);

  /// Create a copy of BookMetadata
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$BookMetadataImplCopyWith<_$BookMetadataImpl> get copyWith =>
      __$$BookMetadataImplCopyWithImpl<_$BookMetadataImpl>(this, _$identity);
}

abstract class _BookMetadata implements BookMetadata {
  const factory _BookMetadata(
      {required final String title,
      required final bool titleLock,
      required final String summary,
      required final bool summaryLock,
      required final double number,
      required final bool numberLock,
      required final double numberSort,
      required final bool numberSortLock,
      required final bool releaseDateLock,
      required final List<Author> authors,
      required final bool authorsLock,
      required final Set<String> tags,
      required final bool tagsLock,
      required final DateTime created,
      required final DateTime lastModified,
      required final String isbn,
      required final bool isbnLock,
      required final bool linksLock,
      required final List<WebLink> links,
      final DateTime? releaseDate}) = _$BookMetadataImpl;

  @override
  String get title;
  @override
  bool get titleLock;
  @override
  String get summary;
  @override
  bool get summaryLock;
  @override
  double get number;
  @override
  bool get numberLock;
  @override
  double get numberSort;
  @override
  bool get numberSortLock;
  @override
  bool get releaseDateLock;
  @override
  List<Author> get authors;
  @override
  bool get authorsLock;
  @override
  Set<String> get tags;
  @override
  bool get tagsLock;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  String get isbn;
  @override
  bool get isbnLock;
  @override
  bool get linksLock;
  @override
  List<WebLink> get links;
  @override
  DateTime? get releaseDate;

  /// Create a copy of BookMetadata
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$BookMetadataImplCopyWith<_$BookMetadataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
