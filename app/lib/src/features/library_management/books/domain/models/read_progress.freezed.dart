// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_progress.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ReadProgress {
  int get page => throw _privateConstructorUsedError;
  bool get completed => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;

  /// Create a copy of ReadProgress
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ReadProgressCopyWith<ReadProgress> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReadProgressCopyWith<$Res> {
  factory $ReadProgressCopyWith(
          ReadProgress value, $Res Function(ReadProgress) then) =
      _$ReadProgressCopyWithImpl<$Res, ReadProgress>;
  @useResult
  $Res call(
      {int page, bool completed, DateTime created, DateTime lastModified});
}

/// @nodoc
class _$ReadProgressCopyWithImpl<$Res, $Val extends ReadProgress>
    implements $ReadProgressCopyWith<$Res> {
  _$ReadProgressCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ReadProgress
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? completed = null,
    Object? created = null,
    Object? lastModified = null,
  }) {
    return _then(_value.copyWith(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      completed: null == completed
          ? _value.completed
          : completed // ignore: cast_nullable_to_non_nullable
              as bool,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadProgressImplCopyWith<$Res>
    implements $ReadProgressCopyWith<$Res> {
  factory _$$ReadProgressImplCopyWith(
          _$ReadProgressImpl value, $Res Function(_$ReadProgressImpl) then) =
      __$$ReadProgressImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int page, bool completed, DateTime created, DateTime lastModified});
}

/// @nodoc
class __$$ReadProgressImplCopyWithImpl<$Res>
    extends _$ReadProgressCopyWithImpl<$Res, _$ReadProgressImpl>
    implements _$$ReadProgressImplCopyWith<$Res> {
  __$$ReadProgressImplCopyWithImpl(
      _$ReadProgressImpl _value, $Res Function(_$ReadProgressImpl) _then)
      : super(_value, _then);

  /// Create a copy of ReadProgress
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? completed = null,
    Object? created = null,
    Object? lastModified = null,
  }) {
    return _then(_$ReadProgressImpl(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      completed: null == completed
          ? _value.completed
          : completed // ignore: cast_nullable_to_non_nullable
              as bool,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$ReadProgressImpl implements _ReadProgress {
  const _$ReadProgressImpl(
      {required this.page,
      required this.completed,
      required this.created,
      required this.lastModified});

  @override
  final int page;
  @override
  final bool completed;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;

  @override
  String toString() {
    return 'ReadProgress(page: $page, completed: $completed, created: $created, lastModified: $lastModified)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadProgressImpl &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.completed, completed) ||
                other.completed == completed) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, page, completed, created, lastModified);

  /// Create a copy of ReadProgress
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadProgressImplCopyWith<_$ReadProgressImpl> get copyWith =>
      __$$ReadProgressImplCopyWithImpl<_$ReadProgressImpl>(this, _$identity);
}

abstract class _ReadProgress implements ReadProgress {
  const factory _ReadProgress(
      {required final int page,
      required final bool completed,
      required final DateTime created,
      required final DateTime lastModified}) = _$ReadProgressImpl;

  @override
  int get page;
  @override
  bool get completed;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;

  /// Create a copy of ReadProgress
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadProgressImplCopyWith<_$ReadProgressImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
