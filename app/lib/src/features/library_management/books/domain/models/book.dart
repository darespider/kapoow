import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book_metadata.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/media.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/read_progress.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/thumbnailed.dart';

part 'book.freezed.dart';

/// Book data
@freezed
class Book with _$Book implements Thumbnailed {
  /// Creates a [Book] instance
  const factory Book({
    required String id,
    required String seriesId,
    required String seriesTitle,
    required String libraryId,
    required String name,
    required String url,
    required bool deleted,
    required int number,
    required DateTime created,
    required DateTime lastModified,
    required DateTime fileLastModified,
    required int sizeBytes,
    required String size,
    required Media media,
    required BookMetadata metadata,
    required String fileHash,
    required bool oneshot,
    ReadProgress? readProgress,
  }) = _Book;

  const Book._();

  @override
  String get thumbnailUri => 'books/$id/thumbnail';

  /// Getter of the read status base on the read progress of the book
  ReadStatus get readStatus => readProgress == null || readProgress!.page <= 1
      ? ReadStatus.unread
      : readProgress!.completed
          ? ReadStatus.read
          : ReadStatus.inProgress;
}
