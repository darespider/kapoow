// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'web_link.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$WebLink {
  String get label => throw _privateConstructorUsedError;
  Uri get url => throw _privateConstructorUsedError;

  /// Create a copy of WebLink
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $WebLinkCopyWith<WebLink> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WebLinkCopyWith<$Res> {
  factory $WebLinkCopyWith(WebLink value, $Res Function(WebLink) then) =
      _$WebLinkCopyWithImpl<$Res, WebLink>;
  @useResult
  $Res call({String label, Uri url});
}

/// @nodoc
class _$WebLinkCopyWithImpl<$Res, $Val extends WebLink>
    implements $WebLinkCopyWith<$Res> {
  _$WebLinkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of WebLink
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? label = null,
    Object? url = null,
  }) {
    return _then(_value.copyWith(
      label: null == label
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as Uri,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$WebLinkImplCopyWith<$Res> implements $WebLinkCopyWith<$Res> {
  factory _$$WebLinkImplCopyWith(
          _$WebLinkImpl value, $Res Function(_$WebLinkImpl) then) =
      __$$WebLinkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String label, Uri url});
}

/// @nodoc
class __$$WebLinkImplCopyWithImpl<$Res>
    extends _$WebLinkCopyWithImpl<$Res, _$WebLinkImpl>
    implements _$$WebLinkImplCopyWith<$Res> {
  __$$WebLinkImplCopyWithImpl(
      _$WebLinkImpl _value, $Res Function(_$WebLinkImpl) _then)
      : super(_value, _then);

  /// Create a copy of WebLink
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? label = null,
    Object? url = null,
  }) {
    return _then(_$WebLinkImpl(
      label: null == label
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as Uri,
    ));
  }
}

/// @nodoc

class _$WebLinkImpl implements _WebLink {
  const _$WebLinkImpl({required this.label, required this.url});

  @override
  final String label;
  @override
  final Uri url;

  @override
  String toString() {
    return 'WebLink(label: $label, url: $url)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WebLinkImpl &&
            (identical(other.label, label) || other.label == label) &&
            (identical(other.url, url) || other.url == url));
  }

  @override
  int get hashCode => Object.hash(runtimeType, label, url);

  /// Create a copy of WebLink
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$WebLinkImplCopyWith<_$WebLinkImpl> get copyWith =>
      __$$WebLinkImplCopyWithImpl<_$WebLinkImpl>(this, _$identity);
}

abstract class _WebLink implements WebLink {
  const factory _WebLink(
      {required final String label, required final Uri url}) = _$WebLinkImpl;

  @override
  String get label;
  @override
  Uri get url;

  /// Create a copy of WebLink
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$WebLinkImplCopyWith<_$WebLinkImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
