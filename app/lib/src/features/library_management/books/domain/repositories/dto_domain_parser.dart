import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book_metadata.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/media.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/page.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/read_progress.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/web_link.dart';
import 'package:kapoow/src/features/library_management/common/models/author.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/author_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_metadata_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/media_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/page_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_progress_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/web_link_dto.dart';

/// Extension to parse Dto list into list of domain objects
extension ReadProgressListModelParser on List<ReadProgressDto> {
  /// Converts every this list into a domain T object list
  List<ReadProgress> mapToDomain() => map((e) => e.toDomain()).toList();
}

/// Extension to parse Dto into domain object
extension ReadProgressModelParser on ReadProgressDto {
  /// Creates a ReadProgress domain object from this
  ReadProgress toDomain() => ReadProgress(
        page: page,
        completed: completed,
        created: created,
        lastModified: lastModified,
      );
}

/// Extension to parse Dto list into list of domain objects
extension BookMetadataListModelParser on List<BookMetadataDto> {
  /// Converts every this list into a domain T object list
  List<BookMetadata> toDomain() => map((e) => e.toDomain()).toList();
}

/// Extension to parse Dto into domain object
extension BookMetadataModelParser on BookMetadataDto {
  /// Creates a BookMetadata domain object from this
  BookMetadata toDomain() => BookMetadata(
        title: title,
        titleLock: titleLock,
        summary: summary,
        summaryLock: summaryLock,
        number: double.parse(number),
        numberLock: numberLock,
        numberSort: numberSort,
        numberSortLock: numberSortLock,
        releaseDateLock: releaseDateLock,
        authors: authors.map((a) => a.toDomain()).toList(),
        authorsLock: authorsLock,
        tags: tags,
        tagsLock: tagsLock,
        created: created,
        lastModified: lastModified,
        releaseDate: releaseDate,
        isbn: isbn,
        isbnLock: isbnLock,
        linksLock: linksLock,
        links: links.map((l) => l.toDomain()).toList(),
      );
}

/// Extension to parse Dto list into list of domain objects
extension BookListModelParser on List<BookDto> {
  /// Converts every this list into a domain T object list
  List<Book> mapToDomain() => map((e) => e.toDomain()).toList();
}

/// Extension to parse Dto into domain object
extension BookModelParser on BookDto {
  /// Creates a Book domain object from this
  Book toDomain() => Book(
        id: id,
        seriesId: seriesId,
        libraryId: libraryId,
        name: name,
        url: url,
        number: number,
        created: created,
        lastModified: lastModified,
        fileLastModified: fileLastModified,
        sizeBytes: sizeBytes,
        media: media.toDomain(),
        metadata: metadata.toDomain(),
        readProgress: readProgress?.toDomain(),
        deleted: deleted,
        fileHash: fileHash,
        oneshot: oneshot,
        seriesTitle: seriesTitle,
        size: size,
      );
}

/// Extension to parse Dto list into list of domain objects
extension MediaListModelParser on List<MediaDto> {
  /// Converts every this list into a domain T object list
  List<Media> mapToDomain() => map((e) => e.toDomain()).toList();
}

/// Extension to parse Dto into domain object
extension MediaModelParser on MediaDto {
  /// Creates a Media domain object from this
  Media toDomain() => Media(
        status: status,
        mediaType: mediaType,
        pagesCount: pagesCount,
        comment: comment,
      );
}

/// Extension to parse Dto list into list of domain objects
extension PageListModelParser on List<PageDto> {
  /// Converts every this list into a domain T object list
  List<Page> mapToDomain() => map((e) => e.toDomain()).toList();
}

/// Extension to parse Dto into domain object
extension PageModelParser on PageDto {
  /// Creates a Page domain object from this
  Page toDomain() => Page(
        number: number,
        fileName: fileName,
        mediaType: mediaType,
        width: width,
        height: height,
      );
}

/// Extension to parse Dto list into list of domain objects
extension AuthorListModelParser on List<AuthorDto> {
  /// Converts every this list into a domain T object list
  List<Author> mapToDomain() => map((e) => e.toDomain()).toList();
}

/// Extension to parse Dto into domain object
extension AuthorModelParser on AuthorDto {
  /// Creates a Author domain object from this
  Author toDomain() => Author(
        name: name,
        role: role,
      );
}

/// Extension to parse Dto list into list of domain objects
extension LinkListModelParser on List<WebLinkDto> {
  /// Converts every this list into a domain T object list
  List<WebLink> mapToDomain() => map((e) => e.toDomain()).toList();
}

/// Extension to parse Dto into domain object
extension WebLinkModelParser on WebLinkDto {
  /// Creates a Author domain object from this
  WebLink toDomain() => WebLink(
        label: label,
        url: url,
      );
}
