import 'package:kapoow/src/features/library_management/libraries/domain/models/library.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/library_dto.dart';

/// Extension to parse Dto into domain object
extension LibraryModelParser on LibraryDto {
  /// Creates a [Library] domain object from this
  Library toDomain() => Library(
        id: id,
        name: name,
        root: root,
        importComicInfoBook: importComicInfoBook,
        importComicInfoSeries: importComicInfoSeries,
        importComicInfoCollection: importComicInfoCollection,
        importComicInfoReadList: importComicInfoReadList,
        importComicInfoSeriesAppendVolume: importComicInfoSeriesAppendVolume,
        importEpubBook: importEpubBook,
        importEpubSeries: importEpubSeries,
        importMylarSeries: importMylarSeries,
        importLocalArtwork: importLocalArtwork,
        importBarcodeIsbn: importBarcodeIsbn,
        scanForceModifiedTime: scanForceModifiedTime,
        scanInterval: scanInterval,
        scanOnStartup: scanOnStartup,
        scanCbx: scanCbx,
        scanPdf: scanPdf,
        scanEpub: scanEpub,
        scanDirectoryExclusions: scanDirectoryExclusions,
        repairExtensions: repairExtensions,
        convertToCbz: convertToCbz,
        emptyTrashAfterScan: emptyTrashAfterScan,
        seriesCover: seriesCover,
        hashFiles: hashFiles,
        hashPages: hashPages,
        analyzeDimensions: analyzeDimensions,
        unavailable: unavailable,
        oneshotsDirectory: oneshotsDirectory,
      );
}
