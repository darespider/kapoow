// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'library.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Library {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get root => throw _privateConstructorUsedError;
  bool get importComicInfoBook => throw _privateConstructorUsedError;
  bool get importComicInfoSeries => throw _privateConstructorUsedError;
  bool get importComicInfoCollection => throw _privateConstructorUsedError;
  bool get importComicInfoReadList => throw _privateConstructorUsedError;
  bool get importComicInfoSeriesAppendVolume =>
      throw _privateConstructorUsedError;
  bool get importEpubBook => throw _privateConstructorUsedError;
  bool get importEpubSeries => throw _privateConstructorUsedError;
  bool get importMylarSeries => throw _privateConstructorUsedError;
  bool get importLocalArtwork => throw _privateConstructorUsedError;
  bool get importBarcodeIsbn => throw _privateConstructorUsedError;
  bool get scanForceModifiedTime => throw _privateConstructorUsedError;
  String get scanInterval => throw _privateConstructorUsedError;
  bool get scanOnStartup => throw _privateConstructorUsedError;
  bool get scanCbx => throw _privateConstructorUsedError;
  bool get scanPdf => throw _privateConstructorUsedError;
  bool get scanEpub => throw _privateConstructorUsedError;
  Set<String> get scanDirectoryExclusions => throw _privateConstructorUsedError;
  bool get repairExtensions => throw _privateConstructorUsedError;
  bool get convertToCbz => throw _privateConstructorUsedError;
  bool get emptyTrashAfterScan => throw _privateConstructorUsedError;
  String get seriesCover => throw _privateConstructorUsedError;
  bool get hashFiles => throw _privateConstructorUsedError;
  bool get hashPages => throw _privateConstructorUsedError;
  bool get analyzeDimensions => throw _privateConstructorUsedError;
  bool get unavailable => throw _privateConstructorUsedError;
  String? get oneshotsDirectory => throw _privateConstructorUsedError;

  /// Create a copy of Library
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $LibraryCopyWith<Library> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LibraryCopyWith<$Res> {
  factory $LibraryCopyWith(Library value, $Res Function(Library) then) =
      _$LibraryCopyWithImpl<$Res, Library>;
  @useResult
  $Res call(
      {String id,
      String name,
      String root,
      bool importComicInfoBook,
      bool importComicInfoSeries,
      bool importComicInfoCollection,
      bool importComicInfoReadList,
      bool importComicInfoSeriesAppendVolume,
      bool importEpubBook,
      bool importEpubSeries,
      bool importMylarSeries,
      bool importLocalArtwork,
      bool importBarcodeIsbn,
      bool scanForceModifiedTime,
      String scanInterval,
      bool scanOnStartup,
      bool scanCbx,
      bool scanPdf,
      bool scanEpub,
      Set<String> scanDirectoryExclusions,
      bool repairExtensions,
      bool convertToCbz,
      bool emptyTrashAfterScan,
      String seriesCover,
      bool hashFiles,
      bool hashPages,
      bool analyzeDimensions,
      bool unavailable,
      String? oneshotsDirectory});
}

/// @nodoc
class _$LibraryCopyWithImpl<$Res, $Val extends Library>
    implements $LibraryCopyWith<$Res> {
  _$LibraryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Library
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? root = null,
    Object? importComicInfoBook = null,
    Object? importComicInfoSeries = null,
    Object? importComicInfoCollection = null,
    Object? importComicInfoReadList = null,
    Object? importComicInfoSeriesAppendVolume = null,
    Object? importEpubBook = null,
    Object? importEpubSeries = null,
    Object? importMylarSeries = null,
    Object? importLocalArtwork = null,
    Object? importBarcodeIsbn = null,
    Object? scanForceModifiedTime = null,
    Object? scanInterval = null,
    Object? scanOnStartup = null,
    Object? scanCbx = null,
    Object? scanPdf = null,
    Object? scanEpub = null,
    Object? scanDirectoryExclusions = null,
    Object? repairExtensions = null,
    Object? convertToCbz = null,
    Object? emptyTrashAfterScan = null,
    Object? seriesCover = null,
    Object? hashFiles = null,
    Object? hashPages = null,
    Object? analyzeDimensions = null,
    Object? unavailable = null,
    Object? oneshotsDirectory = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      root: null == root
          ? _value.root
          : root // ignore: cast_nullable_to_non_nullable
              as String,
      importComicInfoBook: null == importComicInfoBook
          ? _value.importComicInfoBook
          : importComicInfoBook // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoSeries: null == importComicInfoSeries
          ? _value.importComicInfoSeries
          : importComicInfoSeries // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoCollection: null == importComicInfoCollection
          ? _value.importComicInfoCollection
          : importComicInfoCollection // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoReadList: null == importComicInfoReadList
          ? _value.importComicInfoReadList
          : importComicInfoReadList // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoSeriesAppendVolume: null ==
              importComicInfoSeriesAppendVolume
          ? _value.importComicInfoSeriesAppendVolume
          : importComicInfoSeriesAppendVolume // ignore: cast_nullable_to_non_nullable
              as bool,
      importEpubBook: null == importEpubBook
          ? _value.importEpubBook
          : importEpubBook // ignore: cast_nullable_to_non_nullable
              as bool,
      importEpubSeries: null == importEpubSeries
          ? _value.importEpubSeries
          : importEpubSeries // ignore: cast_nullable_to_non_nullable
              as bool,
      importMylarSeries: null == importMylarSeries
          ? _value.importMylarSeries
          : importMylarSeries // ignore: cast_nullable_to_non_nullable
              as bool,
      importLocalArtwork: null == importLocalArtwork
          ? _value.importLocalArtwork
          : importLocalArtwork // ignore: cast_nullable_to_non_nullable
              as bool,
      importBarcodeIsbn: null == importBarcodeIsbn
          ? _value.importBarcodeIsbn
          : importBarcodeIsbn // ignore: cast_nullable_to_non_nullable
              as bool,
      scanForceModifiedTime: null == scanForceModifiedTime
          ? _value.scanForceModifiedTime
          : scanForceModifiedTime // ignore: cast_nullable_to_non_nullable
              as bool,
      scanInterval: null == scanInterval
          ? _value.scanInterval
          : scanInterval // ignore: cast_nullable_to_non_nullable
              as String,
      scanOnStartup: null == scanOnStartup
          ? _value.scanOnStartup
          : scanOnStartup // ignore: cast_nullable_to_non_nullable
              as bool,
      scanCbx: null == scanCbx
          ? _value.scanCbx
          : scanCbx // ignore: cast_nullable_to_non_nullable
              as bool,
      scanPdf: null == scanPdf
          ? _value.scanPdf
          : scanPdf // ignore: cast_nullable_to_non_nullable
              as bool,
      scanEpub: null == scanEpub
          ? _value.scanEpub
          : scanEpub // ignore: cast_nullable_to_non_nullable
              as bool,
      scanDirectoryExclusions: null == scanDirectoryExclusions
          ? _value.scanDirectoryExclusions
          : scanDirectoryExclusions // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      repairExtensions: null == repairExtensions
          ? _value.repairExtensions
          : repairExtensions // ignore: cast_nullable_to_non_nullable
              as bool,
      convertToCbz: null == convertToCbz
          ? _value.convertToCbz
          : convertToCbz // ignore: cast_nullable_to_non_nullable
              as bool,
      emptyTrashAfterScan: null == emptyTrashAfterScan
          ? _value.emptyTrashAfterScan
          : emptyTrashAfterScan // ignore: cast_nullable_to_non_nullable
              as bool,
      seriesCover: null == seriesCover
          ? _value.seriesCover
          : seriesCover // ignore: cast_nullable_to_non_nullable
              as String,
      hashFiles: null == hashFiles
          ? _value.hashFiles
          : hashFiles // ignore: cast_nullable_to_non_nullable
              as bool,
      hashPages: null == hashPages
          ? _value.hashPages
          : hashPages // ignore: cast_nullable_to_non_nullable
              as bool,
      analyzeDimensions: null == analyzeDimensions
          ? _value.analyzeDimensions
          : analyzeDimensions // ignore: cast_nullable_to_non_nullable
              as bool,
      unavailable: null == unavailable
          ? _value.unavailable
          : unavailable // ignore: cast_nullable_to_non_nullable
              as bool,
      oneshotsDirectory: freezed == oneshotsDirectory
          ? _value.oneshotsDirectory
          : oneshotsDirectory // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LibraryImplCopyWith<$Res> implements $LibraryCopyWith<$Res> {
  factory _$$LibraryImplCopyWith(
          _$LibraryImpl value, $Res Function(_$LibraryImpl) then) =
      __$$LibraryImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String name,
      String root,
      bool importComicInfoBook,
      bool importComicInfoSeries,
      bool importComicInfoCollection,
      bool importComicInfoReadList,
      bool importComicInfoSeriesAppendVolume,
      bool importEpubBook,
      bool importEpubSeries,
      bool importMylarSeries,
      bool importLocalArtwork,
      bool importBarcodeIsbn,
      bool scanForceModifiedTime,
      String scanInterval,
      bool scanOnStartup,
      bool scanCbx,
      bool scanPdf,
      bool scanEpub,
      Set<String> scanDirectoryExclusions,
      bool repairExtensions,
      bool convertToCbz,
      bool emptyTrashAfterScan,
      String seriesCover,
      bool hashFiles,
      bool hashPages,
      bool analyzeDimensions,
      bool unavailable,
      String? oneshotsDirectory});
}

/// @nodoc
class __$$LibraryImplCopyWithImpl<$Res>
    extends _$LibraryCopyWithImpl<$Res, _$LibraryImpl>
    implements _$$LibraryImplCopyWith<$Res> {
  __$$LibraryImplCopyWithImpl(
      _$LibraryImpl _value, $Res Function(_$LibraryImpl) _then)
      : super(_value, _then);

  /// Create a copy of Library
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? root = null,
    Object? importComicInfoBook = null,
    Object? importComicInfoSeries = null,
    Object? importComicInfoCollection = null,
    Object? importComicInfoReadList = null,
    Object? importComicInfoSeriesAppendVolume = null,
    Object? importEpubBook = null,
    Object? importEpubSeries = null,
    Object? importMylarSeries = null,
    Object? importLocalArtwork = null,
    Object? importBarcodeIsbn = null,
    Object? scanForceModifiedTime = null,
    Object? scanInterval = null,
    Object? scanOnStartup = null,
    Object? scanCbx = null,
    Object? scanPdf = null,
    Object? scanEpub = null,
    Object? scanDirectoryExclusions = null,
    Object? repairExtensions = null,
    Object? convertToCbz = null,
    Object? emptyTrashAfterScan = null,
    Object? seriesCover = null,
    Object? hashFiles = null,
    Object? hashPages = null,
    Object? analyzeDimensions = null,
    Object? unavailable = null,
    Object? oneshotsDirectory = freezed,
  }) {
    return _then(_$LibraryImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      root: null == root
          ? _value.root
          : root // ignore: cast_nullable_to_non_nullable
              as String,
      importComicInfoBook: null == importComicInfoBook
          ? _value.importComicInfoBook
          : importComicInfoBook // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoSeries: null == importComicInfoSeries
          ? _value.importComicInfoSeries
          : importComicInfoSeries // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoCollection: null == importComicInfoCollection
          ? _value.importComicInfoCollection
          : importComicInfoCollection // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoReadList: null == importComicInfoReadList
          ? _value.importComicInfoReadList
          : importComicInfoReadList // ignore: cast_nullable_to_non_nullable
              as bool,
      importComicInfoSeriesAppendVolume: null ==
              importComicInfoSeriesAppendVolume
          ? _value.importComicInfoSeriesAppendVolume
          : importComicInfoSeriesAppendVolume // ignore: cast_nullable_to_non_nullable
              as bool,
      importEpubBook: null == importEpubBook
          ? _value.importEpubBook
          : importEpubBook // ignore: cast_nullable_to_non_nullable
              as bool,
      importEpubSeries: null == importEpubSeries
          ? _value.importEpubSeries
          : importEpubSeries // ignore: cast_nullable_to_non_nullable
              as bool,
      importMylarSeries: null == importMylarSeries
          ? _value.importMylarSeries
          : importMylarSeries // ignore: cast_nullable_to_non_nullable
              as bool,
      importLocalArtwork: null == importLocalArtwork
          ? _value.importLocalArtwork
          : importLocalArtwork // ignore: cast_nullable_to_non_nullable
              as bool,
      importBarcodeIsbn: null == importBarcodeIsbn
          ? _value.importBarcodeIsbn
          : importBarcodeIsbn // ignore: cast_nullable_to_non_nullable
              as bool,
      scanForceModifiedTime: null == scanForceModifiedTime
          ? _value.scanForceModifiedTime
          : scanForceModifiedTime // ignore: cast_nullable_to_non_nullable
              as bool,
      scanInterval: null == scanInterval
          ? _value.scanInterval
          : scanInterval // ignore: cast_nullable_to_non_nullable
              as String,
      scanOnStartup: null == scanOnStartup
          ? _value.scanOnStartup
          : scanOnStartup // ignore: cast_nullable_to_non_nullable
              as bool,
      scanCbx: null == scanCbx
          ? _value.scanCbx
          : scanCbx // ignore: cast_nullable_to_non_nullable
              as bool,
      scanPdf: null == scanPdf
          ? _value.scanPdf
          : scanPdf // ignore: cast_nullable_to_non_nullable
              as bool,
      scanEpub: null == scanEpub
          ? _value.scanEpub
          : scanEpub // ignore: cast_nullable_to_non_nullable
              as bool,
      scanDirectoryExclusions: null == scanDirectoryExclusions
          ? _value._scanDirectoryExclusions
          : scanDirectoryExclusions // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      repairExtensions: null == repairExtensions
          ? _value.repairExtensions
          : repairExtensions // ignore: cast_nullable_to_non_nullable
              as bool,
      convertToCbz: null == convertToCbz
          ? _value.convertToCbz
          : convertToCbz // ignore: cast_nullable_to_non_nullable
              as bool,
      emptyTrashAfterScan: null == emptyTrashAfterScan
          ? _value.emptyTrashAfterScan
          : emptyTrashAfterScan // ignore: cast_nullable_to_non_nullable
              as bool,
      seriesCover: null == seriesCover
          ? _value.seriesCover
          : seriesCover // ignore: cast_nullable_to_non_nullable
              as String,
      hashFiles: null == hashFiles
          ? _value.hashFiles
          : hashFiles // ignore: cast_nullable_to_non_nullable
              as bool,
      hashPages: null == hashPages
          ? _value.hashPages
          : hashPages // ignore: cast_nullable_to_non_nullable
              as bool,
      analyzeDimensions: null == analyzeDimensions
          ? _value.analyzeDimensions
          : analyzeDimensions // ignore: cast_nullable_to_non_nullable
              as bool,
      unavailable: null == unavailable
          ? _value.unavailable
          : unavailable // ignore: cast_nullable_to_non_nullable
              as bool,
      oneshotsDirectory: freezed == oneshotsDirectory
          ? _value.oneshotsDirectory
          : oneshotsDirectory // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$LibraryImpl implements _Library {
  const _$LibraryImpl(
      {required this.id,
      required this.name,
      required this.root,
      required this.importComicInfoBook,
      required this.importComicInfoSeries,
      required this.importComicInfoCollection,
      required this.importComicInfoReadList,
      required this.importComicInfoSeriesAppendVolume,
      required this.importEpubBook,
      required this.importEpubSeries,
      required this.importMylarSeries,
      required this.importLocalArtwork,
      required this.importBarcodeIsbn,
      required this.scanForceModifiedTime,
      required this.scanInterval,
      required this.scanOnStartup,
      required this.scanCbx,
      required this.scanPdf,
      required this.scanEpub,
      required final Set<String> scanDirectoryExclusions,
      required this.repairExtensions,
      required this.convertToCbz,
      required this.emptyTrashAfterScan,
      required this.seriesCover,
      required this.hashFiles,
      required this.hashPages,
      required this.analyzeDimensions,
      required this.unavailable,
      this.oneshotsDirectory})
      : _scanDirectoryExclusions = scanDirectoryExclusions;

  @override
  final String id;
  @override
  final String name;
  @override
  final String root;
  @override
  final bool importComicInfoBook;
  @override
  final bool importComicInfoSeries;
  @override
  final bool importComicInfoCollection;
  @override
  final bool importComicInfoReadList;
  @override
  final bool importComicInfoSeriesAppendVolume;
  @override
  final bool importEpubBook;
  @override
  final bool importEpubSeries;
  @override
  final bool importMylarSeries;
  @override
  final bool importLocalArtwork;
  @override
  final bool importBarcodeIsbn;
  @override
  final bool scanForceModifiedTime;
  @override
  final String scanInterval;
  @override
  final bool scanOnStartup;
  @override
  final bool scanCbx;
  @override
  final bool scanPdf;
  @override
  final bool scanEpub;
  final Set<String> _scanDirectoryExclusions;
  @override
  Set<String> get scanDirectoryExclusions {
    if (_scanDirectoryExclusions is EqualUnmodifiableSetView)
      return _scanDirectoryExclusions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_scanDirectoryExclusions);
  }

  @override
  final bool repairExtensions;
  @override
  final bool convertToCbz;
  @override
  final bool emptyTrashAfterScan;
  @override
  final String seriesCover;
  @override
  final bool hashFiles;
  @override
  final bool hashPages;
  @override
  final bool analyzeDimensions;
  @override
  final bool unavailable;
  @override
  final String? oneshotsDirectory;

  @override
  String toString() {
    return 'Library(id: $id, name: $name, root: $root, importComicInfoBook: $importComicInfoBook, importComicInfoSeries: $importComicInfoSeries, importComicInfoCollection: $importComicInfoCollection, importComicInfoReadList: $importComicInfoReadList, importComicInfoSeriesAppendVolume: $importComicInfoSeriesAppendVolume, importEpubBook: $importEpubBook, importEpubSeries: $importEpubSeries, importMylarSeries: $importMylarSeries, importLocalArtwork: $importLocalArtwork, importBarcodeIsbn: $importBarcodeIsbn, scanForceModifiedTime: $scanForceModifiedTime, scanInterval: $scanInterval, scanOnStartup: $scanOnStartup, scanCbx: $scanCbx, scanPdf: $scanPdf, scanEpub: $scanEpub, scanDirectoryExclusions: $scanDirectoryExclusions, repairExtensions: $repairExtensions, convertToCbz: $convertToCbz, emptyTrashAfterScan: $emptyTrashAfterScan, seriesCover: $seriesCover, hashFiles: $hashFiles, hashPages: $hashPages, analyzeDimensions: $analyzeDimensions, unavailable: $unavailable, oneshotsDirectory: $oneshotsDirectory)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LibraryImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.root, root) || other.root == root) &&
            (identical(other.importComicInfoBook, importComicInfoBook) ||
                other.importComicInfoBook == importComicInfoBook) &&
            (identical(other.importComicInfoSeries, importComicInfoSeries) ||
                other.importComicInfoSeries == importComicInfoSeries) &&
            (identical(other.importComicInfoCollection, importComicInfoCollection) ||
                other.importComicInfoCollection == importComicInfoCollection) &&
            (identical(other.importComicInfoReadList, importComicInfoReadList) ||
                other.importComicInfoReadList == importComicInfoReadList) &&
            (identical(other.importComicInfoSeriesAppendVolume,
                    importComicInfoSeriesAppendVolume) ||
                other.importComicInfoSeriesAppendVolume ==
                    importComicInfoSeriesAppendVolume) &&
            (identical(other.importEpubBook, importEpubBook) ||
                other.importEpubBook == importEpubBook) &&
            (identical(other.importEpubSeries, importEpubSeries) ||
                other.importEpubSeries == importEpubSeries) &&
            (identical(other.importMylarSeries, importMylarSeries) ||
                other.importMylarSeries == importMylarSeries) &&
            (identical(other.importLocalArtwork, importLocalArtwork) ||
                other.importLocalArtwork == importLocalArtwork) &&
            (identical(other.importBarcodeIsbn, importBarcodeIsbn) ||
                other.importBarcodeIsbn == importBarcodeIsbn) &&
            (identical(other.scanForceModifiedTime, scanForceModifiedTime) ||
                other.scanForceModifiedTime == scanForceModifiedTime) &&
            (identical(other.scanInterval, scanInterval) ||
                other.scanInterval == scanInterval) &&
            (identical(other.scanOnStartup, scanOnStartup) ||
                other.scanOnStartup == scanOnStartup) &&
            (identical(other.scanCbx, scanCbx) || other.scanCbx == scanCbx) &&
            (identical(other.scanPdf, scanPdf) || other.scanPdf == scanPdf) &&
            (identical(other.scanEpub, scanEpub) ||
                other.scanEpub == scanEpub) &&
            const DeepCollectionEquality().equals(
                other._scanDirectoryExclusions, _scanDirectoryExclusions) &&
            (identical(other.repairExtensions, repairExtensions) ||
                other.repairExtensions == repairExtensions) &&
            (identical(other.convertToCbz, convertToCbz) ||
                other.convertToCbz == convertToCbz) &&
            (identical(other.emptyTrashAfterScan, emptyTrashAfterScan) ||
                other.emptyTrashAfterScan == emptyTrashAfterScan) &&
            (identical(other.seriesCover, seriesCover) ||
                other.seriesCover == seriesCover) &&
            (identical(other.hashFiles, hashFiles) || other.hashFiles == hashFiles) &&
            (identical(other.hashPages, hashPages) || other.hashPages == hashPages) &&
            (identical(other.analyzeDimensions, analyzeDimensions) || other.analyzeDimensions == analyzeDimensions) &&
            (identical(other.unavailable, unavailable) || other.unavailable == unavailable) &&
            (identical(other.oneshotsDirectory, oneshotsDirectory) || other.oneshotsDirectory == oneshotsDirectory));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        name,
        root,
        importComicInfoBook,
        importComicInfoSeries,
        importComicInfoCollection,
        importComicInfoReadList,
        importComicInfoSeriesAppendVolume,
        importEpubBook,
        importEpubSeries,
        importMylarSeries,
        importLocalArtwork,
        importBarcodeIsbn,
        scanForceModifiedTime,
        scanInterval,
        scanOnStartup,
        scanCbx,
        scanPdf,
        scanEpub,
        const DeepCollectionEquality().hash(_scanDirectoryExclusions),
        repairExtensions,
        convertToCbz,
        emptyTrashAfterScan,
        seriesCover,
        hashFiles,
        hashPages,
        analyzeDimensions,
        unavailable,
        oneshotsDirectory
      ]);

  /// Create a copy of Library
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$LibraryImplCopyWith<_$LibraryImpl> get copyWith =>
      __$$LibraryImplCopyWithImpl<_$LibraryImpl>(this, _$identity);
}

abstract class _Library implements Library {
  const factory _Library(
      {required final String id,
      required final String name,
      required final String root,
      required final bool importComicInfoBook,
      required final bool importComicInfoSeries,
      required final bool importComicInfoCollection,
      required final bool importComicInfoReadList,
      required final bool importComicInfoSeriesAppendVolume,
      required final bool importEpubBook,
      required final bool importEpubSeries,
      required final bool importMylarSeries,
      required final bool importLocalArtwork,
      required final bool importBarcodeIsbn,
      required final bool scanForceModifiedTime,
      required final String scanInterval,
      required final bool scanOnStartup,
      required final bool scanCbx,
      required final bool scanPdf,
      required final bool scanEpub,
      required final Set<String> scanDirectoryExclusions,
      required final bool repairExtensions,
      required final bool convertToCbz,
      required final bool emptyTrashAfterScan,
      required final String seriesCover,
      required final bool hashFiles,
      required final bool hashPages,
      required final bool analyzeDimensions,
      required final bool unavailable,
      final String? oneshotsDirectory}) = _$LibraryImpl;

  @override
  String get id;
  @override
  String get name;
  @override
  String get root;
  @override
  bool get importComicInfoBook;
  @override
  bool get importComicInfoSeries;
  @override
  bool get importComicInfoCollection;
  @override
  bool get importComicInfoReadList;
  @override
  bool get importComicInfoSeriesAppendVolume;
  @override
  bool get importEpubBook;
  @override
  bool get importEpubSeries;
  @override
  bool get importMylarSeries;
  @override
  bool get importLocalArtwork;
  @override
  bool get importBarcodeIsbn;
  @override
  bool get scanForceModifiedTime;
  @override
  String get scanInterval;
  @override
  bool get scanOnStartup;
  @override
  bool get scanCbx;
  @override
  bool get scanPdf;
  @override
  bool get scanEpub;
  @override
  Set<String> get scanDirectoryExclusions;
  @override
  bool get repairExtensions;
  @override
  bool get convertToCbz;
  @override
  bool get emptyTrashAfterScan;
  @override
  String get seriesCover;
  @override
  bool get hashFiles;
  @override
  bool get hashPages;
  @override
  bool get analyzeDimensions;
  @override
  bool get unavailable;
  @override
  String? get oneshotsDirectory;

  /// Create a copy of Library
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$LibraryImplCopyWith<_$LibraryImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
