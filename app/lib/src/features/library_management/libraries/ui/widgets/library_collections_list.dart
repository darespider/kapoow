import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/library_management/collections/ui/widgets/collection_series_row.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/library_collections_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/widgets/library_items_list.dart';

/// Displays an "infinite" list of collections from the library.
class LibraryCollectionsList extends ConsumerWidget {
  /// Creates a [LibraryCollectionsList] instance
  const LibraryCollectionsList({
    required this.libraryId,
    super.key,
  });

  /// Library Id from where the collections are
  final String? libraryId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pod = libraryCollectionPaginatedPod(libraryId);
    final collectionsPages = ref.watch(pod);

    return LibraryItemsList(
      paginatedState: collectionsPages,
      nextPageLoader: ref.watch(pod.notifier).loadNextPage,
      itemBuilder: (_, item) => CollectionFirstSeriesRow(collection: item),
    );
  }
}
