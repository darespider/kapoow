// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_readlists_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$libraryReadListsFirstPageAsyncHash() =>
    r'fc2cd68cd3d3cb258e5eb63d232fe0211bd33665';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the first page of a list of ReadLists from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [libraryReadListsFirstPageAsync].
@ProviderFor(libraryReadListsFirstPageAsync)
const libraryReadListsFirstPageAsyncPod =
    LibraryReadListsFirstPageAsyncFamily();

/// Provides the first page of a list of ReadLists from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [libraryReadListsFirstPageAsync].
class LibraryReadListsFirstPageAsyncFamily
    extends Family<AsyncValue<Pagination<ReadList>>> {
  /// Provides the first page of a list of ReadLists from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [libraryReadListsFirstPageAsync].
  const LibraryReadListsFirstPageAsyncFamily();

  /// Provides the first page of a list of ReadLists from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [libraryReadListsFirstPageAsync].
  LibraryReadListsFirstPageAsyncProvider call({
    required String? libraryId,
  }) {
    return LibraryReadListsFirstPageAsyncProvider(
      libraryId: libraryId,
    );
  }

  @override
  LibraryReadListsFirstPageAsyncProvider getProviderOverride(
    covariant LibraryReadListsFirstPageAsyncProvider provider,
  ) {
    return call(
      libraryId: provider.libraryId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'libraryReadListsFirstPageAsyncPod';
}

/// Provides the first page of a list of ReadLists from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [libraryReadListsFirstPageAsync].
class LibraryReadListsFirstPageAsyncProvider
    extends FutureProvider<Pagination<ReadList>> {
  /// Provides the first page of a list of ReadLists from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [libraryReadListsFirstPageAsync].
  LibraryReadListsFirstPageAsyncProvider({
    required String? libraryId,
  }) : this._internal(
          (ref) => libraryReadListsFirstPageAsync(
            ref as LibraryReadListsFirstPageAsyncRef,
            libraryId: libraryId,
          ),
          from: libraryReadListsFirstPageAsyncPod,
          name: r'libraryReadListsFirstPageAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$libraryReadListsFirstPageAsyncHash,
          dependencies: LibraryReadListsFirstPageAsyncFamily._dependencies,
          allTransitiveDependencies:
              LibraryReadListsFirstPageAsyncFamily._allTransitiveDependencies,
          libraryId: libraryId,
        );

  LibraryReadListsFirstPageAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.libraryId,
  }) : super.internal();

  final String? libraryId;

  @override
  Override overrideWith(
    FutureOr<Pagination<ReadList>> Function(
            LibraryReadListsFirstPageAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LibraryReadListsFirstPageAsyncProvider._internal(
        (ref) => create(ref as LibraryReadListsFirstPageAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        libraryId: libraryId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<ReadList>> createElement() {
    return _LibraryReadListsFirstPageAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LibraryReadListsFirstPageAsyncProvider &&
        other.libraryId == libraryId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, libraryId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin LibraryReadListsFirstPageAsyncRef
    on FutureProviderRef<Pagination<ReadList>> {
  /// The parameter `libraryId` of this provider.
  String? get libraryId;
}

class _LibraryReadListsFirstPageAsyncProviderElement
    extends FutureProviderElement<Pagination<ReadList>>
    with LibraryReadListsFirstPageAsyncRef {
  _LibraryReadListsFirstPageAsyncProviderElement(super.provider);

  @override
  String? get libraryId =>
      (origin as LibraryReadListsFirstPageAsyncProvider).libraryId;
}

String _$libraryReadListPaginatedHash() =>
    r'd6ab44107316b026910d3b6ab4a92ee6eb808edb';

abstract class _$LibraryReadListPaginated
    extends BuildlessNotifier<PaginatedState<ReadList>> {
  late final String? params;

  PaginatedState<ReadList> build([
    String? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for ReadLists from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [LibraryReadListPaginated].
@ProviderFor(LibraryReadListPaginated)
const libraryReadListPaginatedPod = LibraryReadListPaginatedFamily();

/// Notifier dedicated to handle the pagination state for ReadLists from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [LibraryReadListPaginated].
class LibraryReadListPaginatedFamily extends Family<PaginatedState<ReadList>> {
  /// Notifier dedicated to handle the pagination state for ReadLists from a
  /// specific library, the [params] param should contain the library id.
  /// if the [params] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [LibraryReadListPaginated].
  const LibraryReadListPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for ReadLists from a
  /// specific library, the [params] param should contain the library id.
  /// if the [params] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [LibraryReadListPaginated].
  LibraryReadListPaginatedProvider call([
    String? params,
  ]) {
    return LibraryReadListPaginatedProvider(
      params,
    );
  }

  @override
  LibraryReadListPaginatedProvider getProviderOverride(
    covariant LibraryReadListPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'libraryReadListPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for ReadLists from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [LibraryReadListPaginated].
class LibraryReadListPaginatedProvider extends NotifierProviderImpl<
    LibraryReadListPaginated, PaginatedState<ReadList>> {
  /// Notifier dedicated to handle the pagination state for ReadLists from a
  /// specific library, the [params] param should contain the library id.
  /// if the [params] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [LibraryReadListPaginated].
  LibraryReadListPaginatedProvider([
    String? params,
  ]) : this._internal(
          () => LibraryReadListPaginated()..params = params,
          from: libraryReadListPaginatedPod,
          name: r'libraryReadListPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$libraryReadListPaginatedHash,
          dependencies: LibraryReadListPaginatedFamily._dependencies,
          allTransitiveDependencies:
              LibraryReadListPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  LibraryReadListPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String? params;

  @override
  PaginatedState<ReadList> runNotifierBuild(
    covariant LibraryReadListPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(LibraryReadListPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: LibraryReadListPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<LibraryReadListPaginated, PaginatedState<ReadList>>
      createElement() {
    return _LibraryReadListPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LibraryReadListPaginatedProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin LibraryReadListPaginatedRef
    on NotifierProviderRef<PaginatedState<ReadList>> {
  /// The parameter `params` of this provider.
  String? get params;
}

class _LibraryReadListPaginatedProviderElement extends NotifierProviderElement<
    LibraryReadListPaginated,
    PaginatedState<ReadList>> with LibraryReadListPaginatedRef {
  _LibraryReadListPaginatedProviderElement(super.provider);

  @override
  String? get params => (origin as LibraryReadListPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
