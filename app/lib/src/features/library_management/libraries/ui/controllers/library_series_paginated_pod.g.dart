// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_series_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$librarySeriesFirstPageAsyncHash() =>
    r'820b3c570988665fb6628425bf545ec8ebc1920d';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the first page of a list of Series from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [librarySeriesFirstPageAsync].
@ProviderFor(librarySeriesFirstPageAsync)
const librarySeriesFirstPageAsyncPod = LibrarySeriesFirstPageAsyncFamily();

/// Provides the first page of a list of Series from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [librarySeriesFirstPageAsync].
class LibrarySeriesFirstPageAsyncFamily
    extends Family<AsyncValue<Pagination<Series>>> {
  /// Provides the first page of a list of Series from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [librarySeriesFirstPageAsync].
  const LibrarySeriesFirstPageAsyncFamily();

  /// Provides the first page of a list of Series from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [librarySeriesFirstPageAsync].
  LibrarySeriesFirstPageAsyncProvider call({
    required String? libraryId,
  }) {
    return LibrarySeriesFirstPageAsyncProvider(
      libraryId: libraryId,
    );
  }

  @override
  LibrarySeriesFirstPageAsyncProvider getProviderOverride(
    covariant LibrarySeriesFirstPageAsyncProvider provider,
  ) {
    return call(
      libraryId: provider.libraryId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'librarySeriesFirstPageAsyncPod';
}

/// Provides the first page of a list of Series from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [librarySeriesFirstPageAsync].
class LibrarySeriesFirstPageAsyncProvider
    extends FutureProvider<Pagination<Series>> {
  /// Provides the first page of a list of Series from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [librarySeriesFirstPageAsync].
  LibrarySeriesFirstPageAsyncProvider({
    required String? libraryId,
  }) : this._internal(
          (ref) => librarySeriesFirstPageAsync(
            ref as LibrarySeriesFirstPageAsyncRef,
            libraryId: libraryId,
          ),
          from: librarySeriesFirstPageAsyncPod,
          name: r'librarySeriesFirstPageAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$librarySeriesFirstPageAsyncHash,
          dependencies: LibrarySeriesFirstPageAsyncFamily._dependencies,
          allTransitiveDependencies:
              LibrarySeriesFirstPageAsyncFamily._allTransitiveDependencies,
          libraryId: libraryId,
        );

  LibrarySeriesFirstPageAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.libraryId,
  }) : super.internal();

  final String? libraryId;

  @override
  Override overrideWith(
    FutureOr<Pagination<Series>> Function(
            LibrarySeriesFirstPageAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LibrarySeriesFirstPageAsyncProvider._internal(
        (ref) => create(ref as LibrarySeriesFirstPageAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        libraryId: libraryId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Series>> createElement() {
    return _LibrarySeriesFirstPageAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LibrarySeriesFirstPageAsyncProvider &&
        other.libraryId == libraryId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, libraryId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin LibrarySeriesFirstPageAsyncRef on FutureProviderRef<Pagination<Series>> {
  /// The parameter `libraryId` of this provider.
  String? get libraryId;
}

class _LibrarySeriesFirstPageAsyncProviderElement
    extends FutureProviderElement<Pagination<Series>>
    with LibrarySeriesFirstPageAsyncRef {
  _LibrarySeriesFirstPageAsyncProviderElement(super.provider);

  @override
  String? get libraryId =>
      (origin as LibrarySeriesFirstPageAsyncProvider).libraryId;
}

String _$librarySeriesPaginatedHash() =>
    r'17460fa98add041cb13bc87b26cc635627d49599';

abstract class _$LibrarySeriesPaginated
    extends BuildlessNotifier<PaginatedState<Series>> {
  late final String? params;

  PaginatedState<Series> build([
    String? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
///
/// Copied from [LibrarySeriesPaginated].
@ProviderFor(LibrarySeriesPaginated)
const librarySeriesPaginatedPod = LibrarySeriesPaginatedFamily();

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
///
/// Copied from [LibrarySeriesPaginated].
class LibrarySeriesPaginatedFamily extends Family<PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for Series from a
  /// specific library, if the [params] is `null` then it will fetch the
  /// pagination from all libraries
  ///
  /// Copied from [LibrarySeriesPaginated].
  const LibrarySeriesPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for Series from a
  /// specific library, if the [params] is `null` then it will fetch the
  /// pagination from all libraries
  ///
  /// Copied from [LibrarySeriesPaginated].
  LibrarySeriesPaginatedProvider call([
    String? params,
  ]) {
    return LibrarySeriesPaginatedProvider(
      params,
    );
  }

  @override
  LibrarySeriesPaginatedProvider getProviderOverride(
    covariant LibrarySeriesPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'librarySeriesPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
///
/// Copied from [LibrarySeriesPaginated].
class LibrarySeriesPaginatedProvider extends NotifierProviderImpl<
    LibrarySeriesPaginated, PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for Series from a
  /// specific library, if the [params] is `null` then it will fetch the
  /// pagination from all libraries
  ///
  /// Copied from [LibrarySeriesPaginated].
  LibrarySeriesPaginatedProvider([
    String? params,
  ]) : this._internal(
          () => LibrarySeriesPaginated()..params = params,
          from: librarySeriesPaginatedPod,
          name: r'librarySeriesPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$librarySeriesPaginatedHash,
          dependencies: LibrarySeriesPaginatedFamily._dependencies,
          allTransitiveDependencies:
              LibrarySeriesPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  LibrarySeriesPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String? params;

  @override
  PaginatedState<Series> runNotifierBuild(
    covariant LibrarySeriesPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(LibrarySeriesPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: LibrarySeriesPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<LibrarySeriesPaginated, PaginatedState<Series>>
      createElement() {
    return _LibrarySeriesPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LibrarySeriesPaginatedProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin LibrarySeriesPaginatedRef on NotifierProviderRef<PaginatedState<Series>> {
  /// The parameter `params` of this provider.
  String? get params;
}

class _LibrarySeriesPaginatedProviderElement extends NotifierProviderElement<
    LibrarySeriesPaginated,
    PaginatedState<Series>> with LibrarySeriesPaginatedRef {
  _LibrarySeriesPaginatedProviderElement(super.provider);

  @override
  String? get params => (origin as LibrarySeriesPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
