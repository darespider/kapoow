import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/collections/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'library_collections_paginated_pod.g.dart';

/// Provides the first page of a list of Collections from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
@Riverpod(keepAlive: true)
FutureOr<Pagination<Collection>> libraryCollectionsFirstPageAsync(
  LibraryCollectionsFirstPageAsyncRef ref, {
  required String? libraryId,
}) {
  return ref.watch(collectionsRepoPod).getCollections(
        libraryIds: libraryId != null ? [libraryId] : null,
        page: 0,
      );
}

/// Notifier dedicated to handle the pagination state for Collections from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the  pagination from all
/// libraries
@Riverpod(keepAlive: true)
class LibraryCollectionPaginated extends _$LibraryCollectionPaginated
    with PaginatedMixin<Collection, String?> {
  @override
  PaginatedState<Collection> build([String? params]) {
    final firstPageAsync =
        ref.watch(libraryCollectionsFirstPageAsyncPod(libraryId: params));

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Collection>> fetchPage(int page) async {
    return ref.read(collectionsRepoPod).getCollections(
          libraryIds: params?.toNullableList(),
          page: page,
        );
  }
}
