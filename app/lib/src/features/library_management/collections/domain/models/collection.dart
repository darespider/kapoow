import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/common/models/thumbnailed.dart';

part 'collection.freezed.dart';

/// Collection of Books in a library
@freezed
class Collection with _$Collection implements Thumbnailed {
  /// Creates a [Collection] instance
  const factory Collection({
    required String id,
    required String name,
    required bool ordered,
    required List<String> seriesIds,
    required DateTime createdDate,
    required DateTime lastModifiedDate,
    required bool filtered,
  }) = _Collection;

  const Collection._();

  @override
  String get thumbnailUri => 'collections/$id/thumbnail';
}
