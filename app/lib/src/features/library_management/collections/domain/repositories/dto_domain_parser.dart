import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';

/// Extension to parse Dto into domain object
extension CollectionModelParser on CollectionDto {
  /// Creates a Pagination<T> domain object from this
  Collection toDomain() => Collection(
        id: id,
        name: name,
        ordered: ordered,
        seriesIds: seriesIds,
        createdDate: createdDate,
        lastModifiedDate: lastModifiedDate,
        filtered: filtered,
      );
}
