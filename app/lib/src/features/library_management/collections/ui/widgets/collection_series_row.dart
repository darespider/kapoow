import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/collections/ui/controllers/pods.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_images_row.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/view_all_cover_card.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_cover.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Horizontal list with the first 5 books of a series.
///
/// Displays the covers, the title of the series and a button to access to
/// the series details page.
class CollectionFirstSeriesRow extends ConsumerWidget {
  /// Creates a [CollectionFirstSeriesRow] instance
  const CollectionFirstSeriesRow({
    required this.collection,
    super.key,
  });

  /// The collection to display the books
  final Collection collection;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final seriesPageAsync =
        ref.watch(firstSeriesByCollectionPod(collection.id));

    void goToCollection() => CollectionRoute(id: collection.id).go(context);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: AppPaddings.small,
                ),
                child: Text(
                  collection.name,
                  style: context.textTheme.titleLarge,
                ),
              ),
            ),
            IconButton(
              icon: PhosphorIcon(
                PhosphorIcons.archive(PhosphorIconsStyle.duotone),
              ),
              onPressed: goToCollection,
            ),
          ],
        ),
        Flexible(
          child: TypicalWhenBuilder(
            asyncValue: seriesPageAsync,
            loadingBuilder: (_) => CoverImagesRow.placeholders(
              collection.seriesIds.length > 10
                  ? 10
                  : collection.seriesIds.length,
            ),
            dataBuilder: (_, page) {
              return HorizontalPaginatedList.singlePage(
                page: page,
                trailingBuilder: (context) => ViewAllCoverCard(
                  onTap: goToCollection,
                ),
                itemBuilder: (context, series) => SeriesCover(series: series),
              );
            },
          ),
        ),
      ],
    );
  }
}
