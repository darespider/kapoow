// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$collectionHash() => r'f81a1ce33e294fe3f2c6eeba9cd96675c5e25588';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the collection data from an api call
///
/// Copied from [collection].
@ProviderFor(collection)
const collectionPod = CollectionFamily();

/// Provides the collection data from an api call
///
/// Copied from [collection].
class CollectionFamily extends Family<AsyncValue<Collection>> {
  /// Provides the collection data from an api call
  ///
  /// Copied from [collection].
  const CollectionFamily();

  /// Provides the collection data from an api call
  ///
  /// Copied from [collection].
  CollectionProvider call(
    String id,
  ) {
    return CollectionProvider(
      id,
    );
  }

  @override
  CollectionProvider getProviderOverride(
    covariant CollectionProvider provider,
  ) {
    return call(
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'collectionPod';
}

/// Provides the collection data from an api call
///
/// Copied from [collection].
class CollectionProvider extends FutureProvider<Collection> {
  /// Provides the collection data from an api call
  ///
  /// Copied from [collection].
  CollectionProvider(
    String id,
  ) : this._internal(
          (ref) => collection(
            ref as CollectionRef,
            id,
          ),
          from: collectionPod,
          name: r'collectionPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$collectionHash,
          dependencies: CollectionFamily._dependencies,
          allTransitiveDependencies:
              CollectionFamily._allTransitiveDependencies,
          id: id,
        );

  CollectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.id,
  }) : super.internal();

  final String id;

  @override
  Override overrideWith(
    FutureOr<Collection> Function(CollectionRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CollectionProvider._internal(
        (ref) => create(ref as CollectionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        id: id,
      ),
    );
  }

  @override
  FutureProviderElement<Collection> createElement() {
    return _CollectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CollectionProvider && other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin CollectionRef on FutureProviderRef<Collection> {
  /// The parameter `id` of this provider.
  String get id;
}

class _CollectionProviderElement extends FutureProviderElement<Collection>
    with CollectionRef {
  _CollectionProviderElement(super.provider);

  @override
  String get id => (origin as CollectionProvider).id;
}

String _$firstSeriesByCollectionHash() =>
    r'72d6f43f9c0ef9c4c521742e22a49934dcd249bf';

/// The first 10 series of a collection
///
/// Copied from [firstSeriesByCollection].
@ProviderFor(firstSeriesByCollection)
const firstSeriesByCollectionPod = FirstSeriesByCollectionFamily();

/// The first 10 series of a collection
///
/// Copied from [firstSeriesByCollection].
class FirstSeriesByCollectionFamily
    extends Family<AsyncValue<Pagination<Series>>> {
  /// The first 10 series of a collection
  ///
  /// Copied from [firstSeriesByCollection].
  const FirstSeriesByCollectionFamily();

  /// The first 10 series of a collection
  ///
  /// Copied from [firstSeriesByCollection].
  FirstSeriesByCollectionProvider call(
    String collectionId,
  ) {
    return FirstSeriesByCollectionProvider(
      collectionId,
    );
  }

  @override
  FirstSeriesByCollectionProvider getProviderOverride(
    covariant FirstSeriesByCollectionProvider provider,
  ) {
    return call(
      provider.collectionId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'firstSeriesByCollectionPod';
}

/// The first 10 series of a collection
///
/// Copied from [firstSeriesByCollection].
class FirstSeriesByCollectionProvider
    extends FutureProvider<Pagination<Series>> {
  /// The first 10 series of a collection
  ///
  /// Copied from [firstSeriesByCollection].
  FirstSeriesByCollectionProvider(
    String collectionId,
  ) : this._internal(
          (ref) => firstSeriesByCollection(
            ref as FirstSeriesByCollectionRef,
            collectionId,
          ),
          from: firstSeriesByCollectionPod,
          name: r'firstSeriesByCollectionPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$firstSeriesByCollectionHash,
          dependencies: FirstSeriesByCollectionFamily._dependencies,
          allTransitiveDependencies:
              FirstSeriesByCollectionFamily._allTransitiveDependencies,
          collectionId: collectionId,
        );

  FirstSeriesByCollectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.collectionId,
  }) : super.internal();

  final String collectionId;

  @override
  Override overrideWith(
    FutureOr<Pagination<Series>> Function(FirstSeriesByCollectionRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FirstSeriesByCollectionProvider._internal(
        (ref) => create(ref as FirstSeriesByCollectionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        collectionId: collectionId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Series>> createElement() {
    return _FirstSeriesByCollectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FirstSeriesByCollectionProvider &&
        other.collectionId == collectionId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, collectionId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin FirstSeriesByCollectionRef on FutureProviderRef<Pagination<Series>> {
  /// The parameter `collectionId` of this provider.
  String get collectionId;
}

class _FirstSeriesByCollectionProviderElement
    extends FutureProviderElement<Pagination<Series>>
    with FirstSeriesByCollectionRef {
  _FirstSeriesByCollectionProviderElement(super.provider);

  @override
  String get collectionId =>
      (origin as FirstSeriesByCollectionProvider).collectionId;
}

String _$collectionSeriesFirstPageAsyncHash() =>
    r'c03c83573ed184034267e6d1a6f0ed4ddc676eee';

/// Provides the first page of a list of Series from the specified library,
/// if the [collectionId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [collectionSeriesFirstPageAsync].
@ProviderFor(collectionSeriesFirstPageAsync)
const collectionSeriesFirstPageAsyncPod =
    CollectionSeriesFirstPageAsyncFamily();

/// Provides the first page of a list of Series from the specified library,
/// if the [collectionId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [collectionSeriesFirstPageAsync].
class CollectionSeriesFirstPageAsyncFamily
    extends Family<AsyncValue<Pagination<Series>>> {
  /// Provides the first page of a list of Series from the specified library,
  /// if the [collectionId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [collectionSeriesFirstPageAsync].
  const CollectionSeriesFirstPageAsyncFamily();

  /// Provides the first page of a list of Series from the specified library,
  /// if the [collectionId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [collectionSeriesFirstPageAsync].
  CollectionSeriesFirstPageAsyncProvider call(
    String collectionId,
  ) {
    return CollectionSeriesFirstPageAsyncProvider(
      collectionId,
    );
  }

  @override
  CollectionSeriesFirstPageAsyncProvider getProviderOverride(
    covariant CollectionSeriesFirstPageAsyncProvider provider,
  ) {
    return call(
      provider.collectionId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'collectionSeriesFirstPageAsyncPod';
}

/// Provides the first page of a list of Series from the specified library,
/// if the [collectionId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [collectionSeriesFirstPageAsync].
class CollectionSeriesFirstPageAsyncProvider
    extends FutureProvider<Pagination<Series>> {
  /// Provides the first page of a list of Series from the specified library,
  /// if the [collectionId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [collectionSeriesFirstPageAsync].
  CollectionSeriesFirstPageAsyncProvider(
    String collectionId,
  ) : this._internal(
          (ref) => collectionSeriesFirstPageAsync(
            ref as CollectionSeriesFirstPageAsyncRef,
            collectionId,
          ),
          from: collectionSeriesFirstPageAsyncPod,
          name: r'collectionSeriesFirstPageAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$collectionSeriesFirstPageAsyncHash,
          dependencies: CollectionSeriesFirstPageAsyncFamily._dependencies,
          allTransitiveDependencies:
              CollectionSeriesFirstPageAsyncFamily._allTransitiveDependencies,
          collectionId: collectionId,
        );

  CollectionSeriesFirstPageAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.collectionId,
  }) : super.internal();

  final String collectionId;

  @override
  Override overrideWith(
    FutureOr<Pagination<Series>> Function(
            CollectionSeriesFirstPageAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CollectionSeriesFirstPageAsyncProvider._internal(
        (ref) => create(ref as CollectionSeriesFirstPageAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        collectionId: collectionId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Series>> createElement() {
    return _CollectionSeriesFirstPageAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CollectionSeriesFirstPageAsyncProvider &&
        other.collectionId == collectionId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, collectionId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin CollectionSeriesFirstPageAsyncRef
    on FutureProviderRef<Pagination<Series>> {
  /// The parameter `collectionId` of this provider.
  String get collectionId;
}

class _CollectionSeriesFirstPageAsyncProviderElement
    extends FutureProviderElement<Pagination<Series>>
    with CollectionSeriesFirstPageAsyncRef {
  _CollectionSeriesFirstPageAsyncProviderElement(super.provider);

  @override
  String get collectionId =>
      (origin as CollectionSeriesFirstPageAsyncProvider).collectionId;
}

String _$collectionSeriesPaginatedHash() =>
    r'2730a8d5ffcb34b203fb7f448e4618f29a5b399b';

abstract class _$CollectionSeriesPaginated
    extends BuildlessNotifier<PaginatedState<Series>> {
  late final String params;

  PaginatedState<Series> build([
    String params = '',
  ]);
}

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
///
/// Copied from [CollectionSeriesPaginated].
@ProviderFor(CollectionSeriesPaginated)
const collectionSeriesPaginatedPod = CollectionSeriesPaginatedFamily();

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
///
/// Copied from [CollectionSeriesPaginated].
class CollectionSeriesPaginatedFamily extends Family<PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for Series from a
  /// specific library, if the [params] is `null` then it will fetch the
  /// pagination from all libraries
  ///
  /// Copied from [CollectionSeriesPaginated].
  const CollectionSeriesPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for Series from a
  /// specific library, if the [params] is `null` then it will fetch the
  /// pagination from all libraries
  ///
  /// Copied from [CollectionSeriesPaginated].
  CollectionSeriesPaginatedProvider call([
    String params = '',
  ]) {
    return CollectionSeriesPaginatedProvider(
      params,
    );
  }

  @override
  CollectionSeriesPaginatedProvider getProviderOverride(
    covariant CollectionSeriesPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'collectionSeriesPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
///
/// Copied from [CollectionSeriesPaginated].
class CollectionSeriesPaginatedProvider extends NotifierProviderImpl<
    CollectionSeriesPaginated, PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for Series from a
  /// specific library, if the [params] is `null` then it will fetch the
  /// pagination from all libraries
  ///
  /// Copied from [CollectionSeriesPaginated].
  CollectionSeriesPaginatedProvider([
    String params = '',
  ]) : this._internal(
          () => CollectionSeriesPaginated()..params = params,
          from: collectionSeriesPaginatedPod,
          name: r'collectionSeriesPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$collectionSeriesPaginatedHash,
          dependencies: CollectionSeriesPaginatedFamily._dependencies,
          allTransitiveDependencies:
              CollectionSeriesPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  CollectionSeriesPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String params;

  @override
  PaginatedState<Series> runNotifierBuild(
    covariant CollectionSeriesPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(CollectionSeriesPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: CollectionSeriesPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<CollectionSeriesPaginated, PaginatedState<Series>>
      createElement() {
    return _CollectionSeriesPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CollectionSeriesPaginatedProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin CollectionSeriesPaginatedRef
    on NotifierProviderRef<PaginatedState<Series>> {
  /// The parameter `params` of this provider.
  String get params;
}

class _CollectionSeriesPaginatedProviderElement extends NotifierProviderElement<
    CollectionSeriesPaginated,
    PaginatedState<Series>> with CollectionSeriesPaginatedRef {
  _CollectionSeriesPaginatedProviderElement(super.provider);

  @override
  String get params => (origin as CollectionSeriesPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
