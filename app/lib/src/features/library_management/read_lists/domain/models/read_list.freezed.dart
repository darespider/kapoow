// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_list.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ReadList {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  bool get ordered => throw _privateConstructorUsedError;
  List<String> get bookIds => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;
  DateTime get lastModifiedDate => throw _privateConstructorUsedError;
  bool get filtered => throw _privateConstructorUsedError;

  /// Create a copy of ReadList
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ReadListCopyWith<ReadList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReadListCopyWith<$Res> {
  factory $ReadListCopyWith(ReadList value, $Res Function(ReadList) then) =
      _$ReadListCopyWithImpl<$Res, ReadList>;
  @useResult
  $Res call(
      {String id,
      String name,
      String summary,
      bool ordered,
      List<String> bookIds,
      DateTime createdDate,
      DateTime lastModifiedDate,
      bool filtered});
}

/// @nodoc
class _$ReadListCopyWithImpl<$Res, $Val extends ReadList>
    implements $ReadListCopyWith<$Res> {
  _$ReadListCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ReadList
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? summary = null,
    Object? ordered = null,
    Object? bookIds = null,
    Object? createdDate = null,
    Object? lastModifiedDate = null,
    Object? filtered = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      ordered: null == ordered
          ? _value.ordered
          : ordered // ignore: cast_nullable_to_non_nullable
              as bool,
      bookIds: null == bookIds
          ? _value.bookIds
          : bookIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
      createdDate: null == createdDate
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModifiedDate: null == lastModifiedDate
          ? _value.lastModifiedDate
          : lastModifiedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      filtered: null == filtered
          ? _value.filtered
          : filtered // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadListImplCopyWith<$Res>
    implements $ReadListCopyWith<$Res> {
  factory _$$ReadListImplCopyWith(
          _$ReadListImpl value, $Res Function(_$ReadListImpl) then) =
      __$$ReadListImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String name,
      String summary,
      bool ordered,
      List<String> bookIds,
      DateTime createdDate,
      DateTime lastModifiedDate,
      bool filtered});
}

/// @nodoc
class __$$ReadListImplCopyWithImpl<$Res>
    extends _$ReadListCopyWithImpl<$Res, _$ReadListImpl>
    implements _$$ReadListImplCopyWith<$Res> {
  __$$ReadListImplCopyWithImpl(
      _$ReadListImpl _value, $Res Function(_$ReadListImpl) _then)
      : super(_value, _then);

  /// Create a copy of ReadList
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? summary = null,
    Object? ordered = null,
    Object? bookIds = null,
    Object? createdDate = null,
    Object? lastModifiedDate = null,
    Object? filtered = null,
  }) {
    return _then(_$ReadListImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      ordered: null == ordered
          ? _value.ordered
          : ordered // ignore: cast_nullable_to_non_nullable
              as bool,
      bookIds: null == bookIds
          ? _value._bookIds
          : bookIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
      createdDate: null == createdDate
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModifiedDate: null == lastModifiedDate
          ? _value.lastModifiedDate
          : lastModifiedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      filtered: null == filtered
          ? _value.filtered
          : filtered // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ReadListImpl extends _ReadList {
  const _$ReadListImpl(
      {required this.id,
      required this.name,
      required this.summary,
      required this.ordered,
      required final List<String> bookIds,
      required this.createdDate,
      required this.lastModifiedDate,
      required this.filtered})
      : _bookIds = bookIds,
        super._();

  @override
  final String id;
  @override
  final String name;
  @override
  final String summary;
  @override
  final bool ordered;
  final List<String> _bookIds;
  @override
  List<String> get bookIds {
    if (_bookIds is EqualUnmodifiableListView) return _bookIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_bookIds);
  }

  @override
  final DateTime createdDate;
  @override
  final DateTime lastModifiedDate;
  @override
  final bool filtered;

  @override
  String toString() {
    return 'ReadList(id: $id, name: $name, summary: $summary, ordered: $ordered, bookIds: $bookIds, createdDate: $createdDate, lastModifiedDate: $lastModifiedDate, filtered: $filtered)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadListImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.ordered, ordered) || other.ordered == ordered) &&
            const DeepCollectionEquality().equals(other._bookIds, _bookIds) &&
            (identical(other.createdDate, createdDate) ||
                other.createdDate == createdDate) &&
            (identical(other.lastModifiedDate, lastModifiedDate) ||
                other.lastModifiedDate == lastModifiedDate) &&
            (identical(other.filtered, filtered) ||
                other.filtered == filtered));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      name,
      summary,
      ordered,
      const DeepCollectionEquality().hash(_bookIds),
      createdDate,
      lastModifiedDate,
      filtered);

  /// Create a copy of ReadList
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadListImplCopyWith<_$ReadListImpl> get copyWith =>
      __$$ReadListImplCopyWithImpl<_$ReadListImpl>(this, _$identity);
}

abstract class _ReadList extends ReadList {
  const factory _ReadList(
      {required final String id,
      required final String name,
      required final String summary,
      required final bool ordered,
      required final List<String> bookIds,
      required final DateTime createdDate,
      required final DateTime lastModifiedDate,
      required final bool filtered}) = _$ReadListImpl;
  const _ReadList._() : super._();

  @override
  String get id;
  @override
  String get name;
  @override
  String get summary;
  @override
  bool get ordered;
  @override
  List<String> get bookIds;
  @override
  DateTime get createdDate;
  @override
  DateTime get lastModifiedDate;
  @override
  bool get filtered;

  /// Create a copy of ReadList
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadListImplCopyWith<_$ReadListImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
