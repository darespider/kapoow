import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/common/models/thumbnailed.dart';

part 'read_list.freezed.dart';

/// Collection of books in a reading order
@freezed
class ReadList with _$ReadList implements Thumbnailed {
  /// Creates a [ReadList] instance
  const factory ReadList({
    required String id,
    required String name,
    required String summary,
    required bool ordered,
    required List<String> bookIds,
    required DateTime createdDate,
    required DateTime lastModifiedDate,
    required bool filtered,
  }) = _ReadList;

  const ReadList._();

  @override
  String get thumbnailUri => 'readlists/$id/thumbnail';
}
