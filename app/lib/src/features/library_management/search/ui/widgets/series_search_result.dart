import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/series_paginated_search_pod.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_cover.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

/// Horizontal Infinite list with Recently Added Series
class SeriesSearchResult extends ConsumerWidget {
  /// Creates a [SeriesSearchResult] instance
  const SeriesSearchResult({
    required this.query,
    super.key,
  });

  /// {@macro kapoow.library_management.search.SearchResultBody}
  final String query;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pod = seriesPaginatedSearchPod(query);
    final foundSeries = ref.watch(pod);

    return TitledSection(
      title: const Text('Series'),
      hide: foundSeries.hasNoResults,
      body: HorizontalPaginatedList(
        paginatedState: foundSeries,
        nextPageLoader: ref.watch(pod.notifier).loadNextPage,
        itemBuilder: (context, series) => SeriesCover(series: series),
      ),
    );
  }
}
