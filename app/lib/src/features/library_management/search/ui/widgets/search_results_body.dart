import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/pods.dart';
import 'package:kapoow/src/features/library_management/search/ui/widgets/books_search_result.dart';
import 'package:kapoow/src/features/library_management/search/ui/widgets/collections_search_result.dart';
import 'package:kapoow/src/features/library_management/search/ui/widgets/readlists_search_result.dart';
import 'package:kapoow/src/features/library_management/search/ui/widgets/series_search_result.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Wraps all the results of the search and displays an image if there are no
/// results.
class SearchResultBody extends ConsumerWidget {
  /// Create a [SearchResultBody] instance.
  ///
  /// The [query] should not be empty
  SearchResultBody({required this.query, super.key})
      : assert(query.isNotEmpty, 'The [query] should not be empty');

  /// {@template kapoow.library_management.search.SearchResultBody}
  /// Query to search for
  /// {@endtemplate}
  final String query;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final hasResultsAsync = ref.watch(somethingFoundPod(query));

    return TypicalWhenBuilder(
      asyncValue: hasResultsAsync,
      dataBuilder: (context, hasResults) {
        if (!hasResults) {
          return Center(
            child: Padding(
              padding: const EdgeInsets.all(AppPaddings.medium),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    // TODO(rurickdev): [l10n] translate
                    'Nothing found.\nTry searching for something else',
                    style: context.textTheme.headlineMedium,
                    textAlign: TextAlign.center,
                  ),
                  Assets.images.search.searchEmpty.svg(),
                ],
              ),
            ),
          );
        }

        return Column(
          children: [
            BooksSearchResult(query: query),
            SeriesSearchResult(query: query),
            ReadlistsSearchResult(query: query),
            CollectionsSearchResult(query: query),
          ].separated(mainAxisExtent: AppPaddings.medium),
        );
      },
    );
  }
}
