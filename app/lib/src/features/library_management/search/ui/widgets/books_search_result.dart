import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/books_paginated_search_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

/// Horizontal Infinite list with Recently Added Books
class BooksSearchResult extends ConsumerWidget {
  /// Creates a [BooksSearchResult] instance
  const BooksSearchResult({
    required this.query,
    super.key,
  });

  /// {@macro kapoow.library_management.search.SearchResultBody}
  final String query;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pod = booksPaginatedSearchPod(query);
    final foundBooks = ref.watch(pod);

    return TitledSection(
      title: const Text('Books'),
      hide: foundBooks.hasNoResults,
      body: HorizontalPaginatedList(
        paginatedState: foundBooks,
        nextPageLoader: ref.watch(pod.notifier).loadNextPage,
        itemBuilder: (context, book) => BookCover(book: book),
      ),
    );
  }
}
