// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'collections_paginated_search_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$collectionsFirstPageSearchAsyncHash() =>
    r'85645e1fc33a9bd6bdd35cc4a68d8ac4db7ace1e';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the first page of a list of searched Collections
///
/// Copied from [collectionsFirstPageSearchAsync].
@ProviderFor(collectionsFirstPageSearchAsync)
const collectionsFirstPageSearchAsyncPod =
    CollectionsFirstPageSearchAsyncFamily();

/// Provides the first page of a list of searched Collections
///
/// Copied from [collectionsFirstPageSearchAsync].
class CollectionsFirstPageSearchAsyncFamily
    extends Family<AsyncValue<Pagination<Collection>>> {
  /// Provides the first page of a list of searched Collections
  ///
  /// Copied from [collectionsFirstPageSearchAsync].
  const CollectionsFirstPageSearchAsyncFamily();

  /// Provides the first page of a list of searched Collections
  ///
  /// Copied from [collectionsFirstPageSearchAsync].
  CollectionsFirstPageSearchAsyncProvider call(
    String query,
  ) {
    return CollectionsFirstPageSearchAsyncProvider(
      query,
    );
  }

  @override
  CollectionsFirstPageSearchAsyncProvider getProviderOverride(
    covariant CollectionsFirstPageSearchAsyncProvider provider,
  ) {
    return call(
      provider.query,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'collectionsFirstPageSearchAsyncPod';
}

/// Provides the first page of a list of searched Collections
///
/// Copied from [collectionsFirstPageSearchAsync].
class CollectionsFirstPageSearchAsyncProvider
    extends AutoDisposeFutureProvider<Pagination<Collection>> {
  /// Provides the first page of a list of searched Collections
  ///
  /// Copied from [collectionsFirstPageSearchAsync].
  CollectionsFirstPageSearchAsyncProvider(
    String query,
  ) : this._internal(
          (ref) => collectionsFirstPageSearchAsync(
            ref as CollectionsFirstPageSearchAsyncRef,
            query,
          ),
          from: collectionsFirstPageSearchAsyncPod,
          name: r'collectionsFirstPageSearchAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$collectionsFirstPageSearchAsyncHash,
          dependencies: CollectionsFirstPageSearchAsyncFamily._dependencies,
          allTransitiveDependencies:
              CollectionsFirstPageSearchAsyncFamily._allTransitiveDependencies,
          query: query,
        );

  CollectionsFirstPageSearchAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.query,
  }) : super.internal();

  final String query;

  @override
  Override overrideWith(
    FutureOr<Pagination<Collection>> Function(
            CollectionsFirstPageSearchAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CollectionsFirstPageSearchAsyncProvider._internal(
        (ref) => create(ref as CollectionsFirstPageSearchAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        query: query,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Pagination<Collection>> createElement() {
    return _CollectionsFirstPageSearchAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CollectionsFirstPageSearchAsyncProvider &&
        other.query == query;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, query.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin CollectionsFirstPageSearchAsyncRef
    on AutoDisposeFutureProviderRef<Pagination<Collection>> {
  /// The parameter `query` of this provider.
  String get query;
}

class _CollectionsFirstPageSearchAsyncProviderElement
    extends AutoDisposeFutureProviderElement<Pagination<Collection>>
    with CollectionsFirstPageSearchAsyncRef {
  _CollectionsFirstPageSearchAsyncProviderElement(super.provider);

  @override
  String get query => (origin as CollectionsFirstPageSearchAsyncProvider).query;
}

String _$collectionsPaginatedSearchHash() =>
    r'654cb8f2005054283d7c3d66feb97bf57ccdea1d';

abstract class _$CollectionsPaginatedSearch
    extends BuildlessAutoDisposeNotifier<PaginatedState<Collection>> {
  late final String params;

  PaginatedState<Collection> build([
    String params = '',
  ]);
}

/// Notifier dedicated to handle the pagination state to search Collections
///
/// Copied from [CollectionsPaginatedSearch].
@ProviderFor(CollectionsPaginatedSearch)
const collectionsPaginatedSearchPod = CollectionsPaginatedSearchFamily();

/// Notifier dedicated to handle the pagination state to search Collections
///
/// Copied from [CollectionsPaginatedSearch].
class CollectionsPaginatedSearchFamily
    extends Family<PaginatedState<Collection>> {
  /// Notifier dedicated to handle the pagination state to search Collections
  ///
  /// Copied from [CollectionsPaginatedSearch].
  const CollectionsPaginatedSearchFamily();

  /// Notifier dedicated to handle the pagination state to search Collections
  ///
  /// Copied from [CollectionsPaginatedSearch].
  CollectionsPaginatedSearchProvider call([
    String params = '',
  ]) {
    return CollectionsPaginatedSearchProvider(
      params,
    );
  }

  @override
  CollectionsPaginatedSearchProvider getProviderOverride(
    covariant CollectionsPaginatedSearchProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'collectionsPaginatedSearchPod';
}

/// Notifier dedicated to handle the pagination state to search Collections
///
/// Copied from [CollectionsPaginatedSearch].
class CollectionsPaginatedSearchProvider
    extends AutoDisposeNotifierProviderImpl<CollectionsPaginatedSearch,
        PaginatedState<Collection>> {
  /// Notifier dedicated to handle the pagination state to search Collections
  ///
  /// Copied from [CollectionsPaginatedSearch].
  CollectionsPaginatedSearchProvider([
    String params = '',
  ]) : this._internal(
          () => CollectionsPaginatedSearch()..params = params,
          from: collectionsPaginatedSearchPod,
          name: r'collectionsPaginatedSearchPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$collectionsPaginatedSearchHash,
          dependencies: CollectionsPaginatedSearchFamily._dependencies,
          allTransitiveDependencies:
              CollectionsPaginatedSearchFamily._allTransitiveDependencies,
          params: params,
        );

  CollectionsPaginatedSearchProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String params;

  @override
  PaginatedState<Collection> runNotifierBuild(
    covariant CollectionsPaginatedSearch notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(CollectionsPaginatedSearch Function() create) {
    return ProviderOverride(
      origin: this,
      override: CollectionsPaginatedSearchProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<CollectionsPaginatedSearch,
      PaginatedState<Collection>> createElement() {
    return _CollectionsPaginatedSearchProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CollectionsPaginatedSearchProvider &&
        other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin CollectionsPaginatedSearchRef
    on AutoDisposeNotifierProviderRef<PaginatedState<Collection>> {
  /// The parameter `params` of this provider.
  String get params;
}

class _CollectionsPaginatedSearchProviderElement
    extends AutoDisposeNotifierProviderElement<CollectionsPaginatedSearch,
        PaginatedState<Collection>> with CollectionsPaginatedSearchRef {
  _CollectionsPaginatedSearchProviderElement(super.provider);

  @override
  String get params => (origin as CollectionsPaginatedSearchProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
