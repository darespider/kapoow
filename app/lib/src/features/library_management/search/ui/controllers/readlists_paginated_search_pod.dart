import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'readlists_paginated_search_pod.g.dart';

/// Provides the first page of a list of searched ReadLists
@riverpod
FutureOr<Pagination<ReadList>> readlistsFirstPageSearchAsync(
  ReadlistsFirstPageSearchAsyncRef ref,
  String query,
) {
  return ref.watch(readlistsRepoPod).fetchReadlists(
        page: 0,
        search: query,
      );
}

/// Notifier dedicated to handle the pagination state to search ReadLists
@riverpod
class ReadListsPaginatedSearch extends _$ReadListsPaginatedSearch
    with PaginatedMixin<ReadList, String> {
  @override
  PaginatedState<ReadList> build([String params = '']) {
    final firstPageAsync = ref.watch(readlistsFirstPageSearchAsyncPod(params));

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<ReadList>> fetchPage(int page) async {
    return ref.read(readlistsRepoPod).fetchReadlists(
          page: page,
          search: params,
        );
  }
}
