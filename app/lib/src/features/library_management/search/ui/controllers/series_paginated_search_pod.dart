import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/library_management/series/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'series_paginated_search_pod.g.dart';

/// Provides the first page of a list of searched Series
@riverpod
FutureOr<Pagination<Series>> seriesFirstPageSearchAsync(
  SeriesFirstPageSearchAsyncRef ref,
  String query,
) {
  return ref.watch(seriesRepoPod).getSeries(
        page: 0,
        search: query,
      );
}

/// Notifier dedicated to handle the pagination state to search Series
@riverpod
class SeriesPaginatedSearch extends _$SeriesPaginatedSearch
    with PaginatedMixin<Series, String> {
  @override
  PaginatedState<Series> build([String params = '']) {
    final firstPageAsync = ref.watch(seriesFirstPageSearchAsyncPod(params));

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Series>> fetchPage(int page) async {
    return ref.read(seriesRepoPod).getSeries(
          page: page,
          search: params,
        );
  }
}
