import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';

/// Pagination<Book>
typedef PageBook = Pagination<Book>;

/// Pagination<Series>
typedef PageSeries = Pagination<Series>;

/// Pagination<Collection>
typedef PageCollection = Pagination<Collection>;

/// Pagination<ReadList>
typedef PageReadList = Pagination<ReadList>;
