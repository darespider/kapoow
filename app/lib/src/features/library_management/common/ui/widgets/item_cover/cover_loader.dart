import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_aspect_ratio.dart';

/// Card that follows the cover aspect ratio and displays an [AppLoader]
/// at its center.
class CoverCardLoader extends StatelessWidget {
  /// Creates a [CoverCardLoader] instance
  const CoverCardLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return const CoverAspectRatio(
      child: Card(
        child: Center(
          child: AppLoader(),
        ),
      ),
    );
  }
}
