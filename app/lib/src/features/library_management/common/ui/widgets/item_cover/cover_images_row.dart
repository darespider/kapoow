import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';

/// Function that dynamically creates a [CoverDecorationBuilder]
typedef CoverDecorationBuilder = CoverDecorationData Function(
  BuildContext context,
  int index,
);

/// Scrollable horizontal list of covers
class CoverImagesRow extends StatelessWidget {
  /// Creates a [CoverImagesRow] instance
  const CoverImagesRow({
    required this.urls,
    this.statuses = const [],
    super.key,
    this.onItemTap,
    this.onItemLongPress,
    this.trailingBuilder,
    this.decorationBuilder,
  });

  /// Creates a [CoverImagesRow] instance with placeholders
  CoverImagesRow.placeholders(
    int length, {
    this.trailingBuilder,
    super.key,
  })  : urls = List.generate(length, (_) => ''),
        statuses = [],
        decorationBuilder = null,
        onItemLongPress = null,
        onItemTap = null;

  /// Urls if the images for the covers
  final List<String> urls;

  /// Read Status on the cover
  final List<ReadStatus> statuses;

  /// What to do when the image is tapped
  final ValueChanged<int>? onItemTap;

  /// What to do when the image is long pressed
  final ValueChanged<int>? onItemLongPress;

  /// If provided the list will add a last item returned by this callback
  final WidgetBuilder? trailingBuilder;

  /// Pass to add a decoration to the cover
  final CoverDecorationBuilder? decorationBuilder;

  @override
  Widget build(BuildContext context) {
    final itemCount = trailingBuilder != null ? urls.length + 1 : urls.length;

    return ListView.separated(
      scrollDirection: Axis.horizontal,
      padding: const EdgeInsets.symmetric(horizontal: AppPaddings.small),
      itemCount: itemCount,
      separatorBuilder: (context, index) => const Gap(AppPaddings.xSmall),
      itemBuilder: (context, index) {
        if (index == urls.length && trailingBuilder != null) {
          return trailingBuilder!.call(context);
        }

        return CoverImage(
          onLongPress: onItemLongPress != null
              ? () => onItemLongPress!.call(index)
              : null,
          onTap: onItemTap != null ? () => onItemTap!.call(index) : null,
          coverUrl: urls[index],
          decoration: decorationBuilder?.call(context, index),
        );
      },
    );
  }
}
