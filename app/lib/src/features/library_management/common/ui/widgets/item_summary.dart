import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:kapoow/src/features/library_management/common/utils/summary_formatter.dart';

/// Expandable text widget that renders its content following the common
/// summary layout.
class ItemSummary extends HookWidget {
  /// Creates an [ItemSummary] instance
  const ItemSummary({
    required this.summary,
    this.formatter = const SummaryFormatter(),
    super.key,
  });

  /// Summary to format and display
  final String summary;

  /// Custom formatter, by default is a SummaryFormatter()
  final SummaryFormatter formatter;

  @override
  Widget build(BuildContext context) {
    final seeMoreDescription = useState(false);
    final (formattedSummary, table) = formatter.format(summary);

    return Column(
      children: [
        Text(
          formattedSummary.isNotEmpty
              ? formattedSummary
              // TODO(rurickdev): translate
              : 'No Summary',
          textAlign: TextAlign.justify,
          // style: context.textTheme.bodyLarge,
          maxLines: seeMoreDescription.value ? null : 3,
          overflow: seeMoreDescription.value ? null : TextOverflow.ellipsis,
        ),
        if (seeMoreDescription.value && table.isNotEmpty)
          Markdown(
            data: table,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.zero,
          ),
        if (summary.isNotEmpty)
          Align(
            alignment: AlignmentDirectional.centerEnd,
            child: TextButton(
              onPressed: () {
                seeMoreDescription.value = !seeMoreDescription.value;
              },
              child: Text(
                seeMoreDescription.value ? 'Hide' : 'Expand',
              ),
            ),
          ),
      ],
    );
  }
}
