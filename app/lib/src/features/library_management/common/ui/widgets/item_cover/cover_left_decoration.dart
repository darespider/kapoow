import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Layout for the left side of the Cover Decoration
class CoverLeftDecoration extends StatelessWidget {
  /// Creates a [CoverLeftDecoration] instance
  const CoverLeftDecoration({
    required this.decoration,
    super.key,
  });

  /// Decoration data
  final CoverDecorationData decoration;

  @override
  Widget build(BuildContext context) {
    final formatter = NumberFormat('###0.#');

    return Container(
      decoration: BoxDecoration(
        color: context.colorScheme.secondaryContainer,
        borderRadius: const BorderRadius.only(
          topLeft: coverRadius,
          bottomRight: coverRadius,
        ),
      ),
      padding: const EdgeInsets.all(AppPaddings.xxSmall),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (decoration.issueNumber != null)
            Text('#${formatter.format(decoration.issueNumber)}'),
          if (decoration.annual != null) ...[
            Text(decoration.annual!),
            PhosphorIcon(
              PhosphorIcons.calendar(
                PhosphorIconsStyle.duotone,
              ),
              size: 16,
            ),
          ],
          if (decoration.finished ?? false)
            PhosphorIcon(
              PhosphorIcons.checkCircle(
                PhosphorIconsStyle.duotone,
              ),
              size: 16,
            ),
        ].separated(mainAxisExtent: AppPaddings.xxSmall),
      ),
    );
  }
}
