import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_added_series_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_cover.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

final _pod = recentlyAddedSeriesPaginatedPod();

/// Horizontal Infinite list with Recently Added Series
class RecentlyAddedSeriesSection extends ConsumerWidget {
  /// Creates a [RecentlyAddedSeriesSection] instance
  const RecentlyAddedSeriesSection({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final recentlyAddedSeries = ref.watch(_pod);

    return TitledSection(
      title: const Text('Recently Added Series'),
      hide: recentlyAddedSeries.hasNoResults,
      body: HorizontalPaginatedList(
        paginatedState: recentlyAddedSeries,
        nextPageLoader: ref.watch(_pod.notifier).loadNextPage,
        itemBuilder: (context, series) => SeriesCover(series: series),
      ),
    );
  }
}
