import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_updated_series_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_cover.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

final _pod = recentlyUpdatedSeriesPaginatedPod();

/// Horizontal Infinite list with Recently Updated Series
class RecentlyUpdatedSeriesSection extends ConsumerWidget {
  /// Creates a [RecentlyUpdatedSeriesSection] instance
  const RecentlyUpdatedSeriesSection({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final recentlyUpdatedSeries = ref.watch(_pod);

    return TitledSection(
      hide: recentlyUpdatedSeries.hasNoResults,
      title: const Text('Recently Updated Series'),
      body: HorizontalPaginatedList(
        paginatedState: recentlyUpdatedSeries,
        nextPageLoader: ref.watch(_pod.notifier).loadNextPage,
        itemBuilder: (context, series) => SeriesCover(series: series),
      ),
    );
  }
}
