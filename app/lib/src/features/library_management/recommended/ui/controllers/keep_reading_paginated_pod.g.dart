// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'keep_reading_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$keepReadingFirstPageAsyncHash() =>
    r'7045393c7854fc05999f1eddd1f97bb5e7618b20';

/// Provides the first page of a list of keep reading books
///
/// Copied from [keepReadingFirstPageAsync].
@ProviderFor(keepReadingFirstPageAsync)
final keepReadingFirstPageAsyncPod = FutureProvider<Pagination<Book>>.internal(
  keepReadingFirstPageAsync,
  name: r'keepReadingFirstPageAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$keepReadingFirstPageAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef KeepReadingFirstPageAsyncRef = FutureProviderRef<Pagination<Book>>;
String _$keepReadingPaginatedHash() =>
    r'1e4bab75608960fda2add514dd4c37c52d58fc28';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$KeepReadingPaginated
    extends BuildlessNotifier<PaginatedState<Book>> {
  late final Object? params;

  PaginatedState<Book> build([
    Object? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for books on keep reading
/// state, the resulted pagination will be sorted by read date in descending
/// order.
///
/// Copied from [KeepReadingPaginated].
@ProviderFor(KeepReadingPaginated)
const keepReadingPaginatedPod = KeepReadingPaginatedFamily();

/// Notifier dedicated to handle the pagination state for books on keep reading
/// state, the resulted pagination will be sorted by read date in descending
/// order.
///
/// Copied from [KeepReadingPaginated].
class KeepReadingPaginatedFamily extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books on keep reading
  /// state, the resulted pagination will be sorted by read date in descending
  /// order.
  ///
  /// Copied from [KeepReadingPaginated].
  const KeepReadingPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for books on keep reading
  /// state, the resulted pagination will be sorted by read date in descending
  /// order.
  ///
  /// Copied from [KeepReadingPaginated].
  KeepReadingPaginatedProvider call([
    Object? params,
  ]) {
    return KeepReadingPaginatedProvider(
      params,
    );
  }

  @override
  KeepReadingPaginatedProvider getProviderOverride(
    covariant KeepReadingPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'keepReadingPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for books on keep reading
/// state, the resulted pagination will be sorted by read date in descending
/// order.
///
/// Copied from [KeepReadingPaginated].
class KeepReadingPaginatedProvider
    extends NotifierProviderImpl<KeepReadingPaginated, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books on keep reading
  /// state, the resulted pagination will be sorted by read date in descending
  /// order.
  ///
  /// Copied from [KeepReadingPaginated].
  KeepReadingPaginatedProvider([
    Object? params,
  ]) : this._internal(
          () => KeepReadingPaginated()..params = params,
          from: keepReadingPaginatedPod,
          name: r'keepReadingPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$keepReadingPaginatedHash,
          dependencies: KeepReadingPaginatedFamily._dependencies,
          allTransitiveDependencies:
              KeepReadingPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  KeepReadingPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final Object? params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant KeepReadingPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(KeepReadingPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: KeepReadingPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<KeepReadingPaginated, PaginatedState<Book>>
      createElement() {
    return _KeepReadingPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is KeepReadingPaginatedProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin KeepReadingPaginatedRef on NotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  Object? get params;
}

class _KeepReadingPaginatedProviderElement
    extends NotifierProviderElement<KeepReadingPaginated, PaginatedState<Book>>
    with KeepReadingPaginatedRef {
  _KeepReadingPaginatedProviderElement(super.provider);

  @override
  Object? get params => (origin as KeepReadingPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
