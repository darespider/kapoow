// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recently_added_series_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$recentlyAddedSeriesFirstPageAsyncHash() =>
    r'6fb19a36a7409b880bee79f39150328c9f449f09';

/// Provides the first page of a list of keep reading books
///
/// Copied from [recentlyAddedSeriesFirstPageAsync].
@ProviderFor(recentlyAddedSeriesFirstPageAsync)
final recentlyAddedSeriesFirstPageAsyncPod =
    FutureProvider<Pagination<Series>>.internal(
  recentlyAddedSeriesFirstPageAsync,
  name: r'recentlyAddedSeriesFirstPageAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$recentlyAddedSeriesFirstPageAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RecentlyAddedSeriesFirstPageAsyncRef
    = FutureProviderRef<Pagination<Series>>;
String _$recentlyAddedSeriesPaginatedHash() =>
    r'9008611cf368ba12bd14759f82b43275c302c6dc';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$RecentlyAddedSeriesPaginated
    extends BuildlessNotifier<PaginatedState<Series>> {
  late final Object? params;

  PaginatedState<Series> build([
    Object? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyAddedSeriesPaginated].
@ProviderFor(RecentlyAddedSeriesPaginated)
const recentlyAddedSeriesPaginatedPod = RecentlyAddedSeriesPaginatedFamily();

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyAddedSeriesPaginated].
class RecentlyAddedSeriesPaginatedFamily
    extends Family<PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyAddedSeriesPaginated].
  const RecentlyAddedSeriesPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyAddedSeriesPaginated].
  RecentlyAddedSeriesPaginatedProvider call([
    Object? params,
  ]) {
    return RecentlyAddedSeriesPaginatedProvider(
      params,
    );
  }

  @override
  RecentlyAddedSeriesPaginatedProvider getProviderOverride(
    covariant RecentlyAddedSeriesPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'recentlyAddedSeriesPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyAddedSeriesPaginated].
class RecentlyAddedSeriesPaginatedProvider extends NotifierProviderImpl<
    RecentlyAddedSeriesPaginated, PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyAddedSeriesPaginated].
  RecentlyAddedSeriesPaginatedProvider([
    Object? params,
  ]) : this._internal(
          () => RecentlyAddedSeriesPaginated()..params = params,
          from: recentlyAddedSeriesPaginatedPod,
          name: r'recentlyAddedSeriesPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$recentlyAddedSeriesPaginatedHash,
          dependencies: RecentlyAddedSeriesPaginatedFamily._dependencies,
          allTransitiveDependencies:
              RecentlyAddedSeriesPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  RecentlyAddedSeriesPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final Object? params;

  @override
  PaginatedState<Series> runNotifierBuild(
    covariant RecentlyAddedSeriesPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(RecentlyAddedSeriesPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: RecentlyAddedSeriesPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<RecentlyAddedSeriesPaginated, PaginatedState<Series>>
      createElement() {
    return _RecentlyAddedSeriesPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RecentlyAddedSeriesPaginatedProvider &&
        other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin RecentlyAddedSeriesPaginatedRef
    on NotifierProviderRef<PaginatedState<Series>> {
  /// The parameter `params` of this provider.
  Object? get params;
}

class _RecentlyAddedSeriesPaginatedProviderElement
    extends NotifierProviderElement<RecentlyAddedSeriesPaginated,
        PaginatedState<Series>> with RecentlyAddedSeriesPaginatedRef {
  _RecentlyAddedSeriesPaginatedProviderElement(super.provider);

  @override
  Object? get params => (origin as RecentlyAddedSeriesPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
