// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recently_released_books_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$recentlyReleasedBooksFirstPageAsyncHash() =>
    r'e507266bd7acbf6bffc6d5e1f343ffd26dc76bce';

/// Provides the first page of a list of keep reading books
///
/// Copied from [recentlyReleasedBooksFirstPageAsync].
@ProviderFor(recentlyReleasedBooksFirstPageAsync)
final recentlyReleasedBooksFirstPageAsyncPod =
    FutureProvider<Pagination<Book>>.internal(
  recentlyReleasedBooksFirstPageAsync,
  name: r'recentlyReleasedBooksFirstPageAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$recentlyReleasedBooksFirstPageAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RecentlyReleasedBooksFirstPageAsyncRef
    = FutureProviderRef<Pagination<Book>>;
String _$recentlyReleasedBooksPaginatedHash() =>
    r'734e37746da76d493b2454eb05442ae917ca460a';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$RecentlyReleasedBooksPaginated
    extends BuildlessNotifier<PaginatedState<Book>> {
  late final Object? params;

  PaginatedState<Book> build([
    Object? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyReleasedBooksPaginated].
@ProviderFor(RecentlyReleasedBooksPaginated)
const recentlyReleasedBooksPaginatedPod =
    RecentlyReleasedBooksPaginatedFamily();

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyReleasedBooksPaginated].
class RecentlyReleasedBooksPaginatedFamily
    extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyReleasedBooksPaginated].
  const RecentlyReleasedBooksPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyReleasedBooksPaginated].
  RecentlyReleasedBooksPaginatedProvider call([
    Object? params,
  ]) {
    return RecentlyReleasedBooksPaginatedProvider(
      params,
    );
  }

  @override
  RecentlyReleasedBooksPaginatedProvider getProviderOverride(
    covariant RecentlyReleasedBooksPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'recentlyReleasedBooksPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyReleasedBooksPaginated].
class RecentlyReleasedBooksPaginatedProvider extends NotifierProviderImpl<
    RecentlyReleasedBooksPaginated, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyReleasedBooksPaginated].
  RecentlyReleasedBooksPaginatedProvider([
    Object? params,
  ]) : this._internal(
          () => RecentlyReleasedBooksPaginated()..params = params,
          from: recentlyReleasedBooksPaginatedPod,
          name: r'recentlyReleasedBooksPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$recentlyReleasedBooksPaginatedHash,
          dependencies: RecentlyReleasedBooksPaginatedFamily._dependencies,
          allTransitiveDependencies:
              RecentlyReleasedBooksPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  RecentlyReleasedBooksPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final Object? params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant RecentlyReleasedBooksPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(RecentlyReleasedBooksPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: RecentlyReleasedBooksPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<RecentlyReleasedBooksPaginated, PaginatedState<Book>>
      createElement() {
    return _RecentlyReleasedBooksPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RecentlyReleasedBooksPaginatedProvider &&
        other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin RecentlyReleasedBooksPaginatedRef
    on NotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  Object? get params;
}

class _RecentlyReleasedBooksPaginatedProviderElement
    extends NotifierProviderElement<RecentlyReleasedBooksPaginated,
        PaginatedState<Book>> with RecentlyReleasedBooksPaginatedRef {
  _RecentlyReleasedBooksPaginatedProviderElement(super.provider);

  @override
  Object? get params =>
      (origin as RecentlyReleasedBooksPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
