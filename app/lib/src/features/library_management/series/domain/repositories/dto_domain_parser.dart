import 'package:kapoow/src/features/library_management/books/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/book_metadata_aggregation.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series_metadata.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_metadata_aggregation_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_metadata_dto.dart';

/// Extension to parse Dto into domain object
extension SeriesModelParser on SeriesDto {
  /// Creates a [Series] domain object from this
  Series toDomain() => Series(
        id: id,
        libraryId: libraryId,
        name: name,
        url: url,
        created: created,
        lastModified: lastModified,
        fileLastModified: fileLastModified,
        booksCount: booksCount,
        booksReadCount: booksReadCount,
        booksUnreadCount: booksUnreadCount,
        booksInProgressCount: booksInProgressCount,
        metadata: metadata.toDomain(),
        booksMetadata: booksMetadata.toDomain(),
        deleted: deleted,
        oneshot: oneshot,
      );
}

/// Extension to parse Dto into domain object
extension SeriesMetadataModelParser on SeriesMetadataDto {
  /// Creates a [SeriesMetadata] domain object from this
  SeriesMetadata toDomain() => SeriesMetadata(
        status: SeriesStatus.fromValue(status),
        statusLock: statusLock,
        title: title,
        titleLock: titleLock,
        titleSort: titleSort,
        titleSortLock: titleSortLock,
        summary: summary,
        summaryLock: summaryLock,
        readingDirection: readingDirection,
        readingDirectionLock: readingDirectionLock,
        publisher: publisher,
        publisherLock: publisherLock,
        ageRating: ageRating,
        ageRatingLock: ageRatingLock,
        language: language,
        languageLock: languageLock,
        genres: genres,
        genresLock: genresLock,
        tags: tags,
        tagsLock: tagsLock,
        created: created,
        lastModified: lastModified,
        sharingLabels: sharingLabels,
        sharingLabelsLock: sharingLabelsLock,
        totalBookCount: totalBookCount,
        totalBookCountLock: totalBookCountLock,
        alternateTitles: alternateTitles,
        alternateTitlesLock: alternateTitlesLock,
        links: links,
        linksLock: linksLock,
      );
}

/// Extension to parse Dto into domain object
extension BookMetadataAggregationModelParser on BookMetadataAggregationDto {
  /// Creates a [BookMetadataAggregation] domain object from this
  BookMetadataAggregation toDomain() => BookMetadataAggregation(
        authors: authors.mapToDomain(),
        summary: summary,
        summaryNumber: summaryNumber,
        created: created,
        lastModified: lastModified,
        tags: tags,
        releaseDate: releaseDate,
      );
}
