import 'package:kapoow/src/features/library_management/series/data/series_repository.dart';
import 'package:kapoow/src/features/library_management/series/domain/repositories/series_repository_imp.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the [SeriesRepository]
@Riverpod(keepAlive: true)
SeriesRepository seriesRepo(SeriesRepoRef ref) {
  return SeriesRepositoryImp(ref.watch(seriesDatasourcePod));
}
