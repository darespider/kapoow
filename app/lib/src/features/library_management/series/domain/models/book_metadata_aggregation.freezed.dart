// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_metadata_aggregation.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BookMetadataAggregation {
  List<Author> get authors => throw _privateConstructorUsedError;
  Set<String> get tags => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  String get summaryNumber => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  DateTime? get releaseDate => throw _privateConstructorUsedError;

  /// Create a copy of BookMetadataAggregation
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $BookMetadataAggregationCopyWith<BookMetadataAggregation> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookMetadataAggregationCopyWith<$Res> {
  factory $BookMetadataAggregationCopyWith(BookMetadataAggregation value,
          $Res Function(BookMetadataAggregation) then) =
      _$BookMetadataAggregationCopyWithImpl<$Res, BookMetadataAggregation>;
  @useResult
  $Res call(
      {List<Author> authors,
      Set<String> tags,
      String summary,
      String summaryNumber,
      DateTime created,
      DateTime lastModified,
      DateTime? releaseDate});
}

/// @nodoc
class _$BookMetadataAggregationCopyWithImpl<$Res,
        $Val extends BookMetadataAggregation>
    implements $BookMetadataAggregationCopyWith<$Res> {
  _$BookMetadataAggregationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of BookMetadataAggregation
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authors = null,
    Object? tags = null,
    Object? summary = null,
    Object? summaryNumber = null,
    Object? created = null,
    Object? lastModified = null,
    Object? releaseDate = freezed,
  }) {
    return _then(_value.copyWith(
      authors: null == authors
          ? _value.authors
          : authors // ignore: cast_nullable_to_non_nullable
              as List<Author>,
      tags: null == tags
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      summaryNumber: null == summaryNumber
          ? _value.summaryNumber
          : summaryNumber // ignore: cast_nullable_to_non_nullable
              as String,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      releaseDate: freezed == releaseDate
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BookMetadataAggregationImplCopyWith<$Res>
    implements $BookMetadataAggregationCopyWith<$Res> {
  factory _$$BookMetadataAggregationImplCopyWith(
          _$BookMetadataAggregationImpl value,
          $Res Function(_$BookMetadataAggregationImpl) then) =
      __$$BookMetadataAggregationImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<Author> authors,
      Set<String> tags,
      String summary,
      String summaryNumber,
      DateTime created,
      DateTime lastModified,
      DateTime? releaseDate});
}

/// @nodoc
class __$$BookMetadataAggregationImplCopyWithImpl<$Res>
    extends _$BookMetadataAggregationCopyWithImpl<$Res,
        _$BookMetadataAggregationImpl>
    implements _$$BookMetadataAggregationImplCopyWith<$Res> {
  __$$BookMetadataAggregationImplCopyWithImpl(
      _$BookMetadataAggregationImpl _value,
      $Res Function(_$BookMetadataAggregationImpl) _then)
      : super(_value, _then);

  /// Create a copy of BookMetadataAggregation
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authors = null,
    Object? tags = null,
    Object? summary = null,
    Object? summaryNumber = null,
    Object? created = null,
    Object? lastModified = null,
    Object? releaseDate = freezed,
  }) {
    return _then(_$BookMetadataAggregationImpl(
      authors: null == authors
          ? _value._authors
          : authors // ignore: cast_nullable_to_non_nullable
              as List<Author>,
      tags: null == tags
          ? _value._tags
          : tags // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      summaryNumber: null == summaryNumber
          ? _value.summaryNumber
          : summaryNumber // ignore: cast_nullable_to_non_nullable
              as String,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      releaseDate: freezed == releaseDate
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc

class _$BookMetadataAggregationImpl implements _BookMetadataAggregation {
  const _$BookMetadataAggregationImpl(
      {required final List<Author> authors,
      required final Set<String> tags,
      required this.summary,
      required this.summaryNumber,
      required this.created,
      required this.lastModified,
      this.releaseDate})
      : _authors = authors,
        _tags = tags;

  final List<Author> _authors;
  @override
  List<Author> get authors {
    if (_authors is EqualUnmodifiableListView) return _authors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_authors);
  }

  final Set<String> _tags;
  @override
  Set<String> get tags {
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_tags);
  }

  @override
  final String summary;
  @override
  final String summaryNumber;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final DateTime? releaseDate;

  @override
  String toString() {
    return 'BookMetadataAggregation(authors: $authors, tags: $tags, summary: $summary, summaryNumber: $summaryNumber, created: $created, lastModified: $lastModified, releaseDate: $releaseDate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BookMetadataAggregationImpl &&
            const DeepCollectionEquality().equals(other._authors, _authors) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryNumber, summaryNumber) ||
                other.summaryNumber == summaryNumber) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.releaseDate, releaseDate) ||
                other.releaseDate == releaseDate));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_authors),
      const DeepCollectionEquality().hash(_tags),
      summary,
      summaryNumber,
      created,
      lastModified,
      releaseDate);

  /// Create a copy of BookMetadataAggregation
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$BookMetadataAggregationImplCopyWith<_$BookMetadataAggregationImpl>
      get copyWith => __$$BookMetadataAggregationImplCopyWithImpl<
          _$BookMetadataAggregationImpl>(this, _$identity);
}

abstract class _BookMetadataAggregation implements BookMetadataAggregation {
  const factory _BookMetadataAggregation(
      {required final List<Author> authors,
      required final Set<String> tags,
      required final String summary,
      required final String summaryNumber,
      required final DateTime created,
      required final DateTime lastModified,
      final DateTime? releaseDate}) = _$BookMetadataAggregationImpl;

  @override
  List<Author> get authors;
  @override
  Set<String> get tags;
  @override
  String get summary;
  @override
  String get summaryNumber;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  DateTime? get releaseDate;

  /// Create a copy of BookMetadataAggregation
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$BookMetadataAggregationImplCopyWith<_$BookMetadataAggregationImpl>
      get copyWith => throw _privateConstructorUsedError;
}
