// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'series_metadata.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SeriesMetadata {
  SeriesStatus get status => throw _privateConstructorUsedError;
  bool get statusLock => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  bool get titleLock => throw _privateConstructorUsedError;
  String get titleSort => throw _privateConstructorUsedError;
  bool get titleSortLock => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  bool get summaryLock => throw _privateConstructorUsedError;
  String get readingDirection => throw _privateConstructorUsedError;
  bool get readingDirectionLock => throw _privateConstructorUsedError;
  String get publisher => throw _privateConstructorUsedError;
  bool get publisherLock => throw _privateConstructorUsedError;
  bool get ageRatingLock => throw _privateConstructorUsedError;
  String get language => throw _privateConstructorUsedError;
  bool get languageLock => throw _privateConstructorUsedError;
  Set<String> get genres => throw _privateConstructorUsedError;
  bool get genresLock => throw _privateConstructorUsedError;
  Set<String> get tags => throw _privateConstructorUsedError;
  bool get tagsLock => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  bool get totalBookCountLock => throw _privateConstructorUsedError;
  Set<String> get sharingLabels => throw _privateConstructorUsedError;
  bool get sharingLabelsLock => throw _privateConstructorUsedError;
  Set<String> get links => throw _privateConstructorUsedError;
  bool get linksLock => throw _privateConstructorUsedError;
  Set<String> get alternateTitles => throw _privateConstructorUsedError;
  bool get alternateTitlesLock => throw _privateConstructorUsedError;
  int? get totalBookCount => throw _privateConstructorUsedError;
  int? get ageRating => throw _privateConstructorUsedError;

  /// Create a copy of SeriesMetadata
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SeriesMetadataCopyWith<SeriesMetadata> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SeriesMetadataCopyWith<$Res> {
  factory $SeriesMetadataCopyWith(
          SeriesMetadata value, $Res Function(SeriesMetadata) then) =
      _$SeriesMetadataCopyWithImpl<$Res, SeriesMetadata>;
  @useResult
  $Res call(
      {SeriesStatus status,
      bool statusLock,
      String title,
      bool titleLock,
      String titleSort,
      bool titleSortLock,
      String summary,
      bool summaryLock,
      String readingDirection,
      bool readingDirectionLock,
      String publisher,
      bool publisherLock,
      bool ageRatingLock,
      String language,
      bool languageLock,
      Set<String> genres,
      bool genresLock,
      Set<String> tags,
      bool tagsLock,
      DateTime created,
      DateTime lastModified,
      bool totalBookCountLock,
      Set<String> sharingLabels,
      bool sharingLabelsLock,
      Set<String> links,
      bool linksLock,
      Set<String> alternateTitles,
      bool alternateTitlesLock,
      int? totalBookCount,
      int? ageRating});
}

/// @nodoc
class _$SeriesMetadataCopyWithImpl<$Res, $Val extends SeriesMetadata>
    implements $SeriesMetadataCopyWith<$Res> {
  _$SeriesMetadataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of SeriesMetadata
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? statusLock = null,
    Object? title = null,
    Object? titleLock = null,
    Object? titleSort = null,
    Object? titleSortLock = null,
    Object? summary = null,
    Object? summaryLock = null,
    Object? readingDirection = null,
    Object? readingDirectionLock = null,
    Object? publisher = null,
    Object? publisherLock = null,
    Object? ageRatingLock = null,
    Object? language = null,
    Object? languageLock = null,
    Object? genres = null,
    Object? genresLock = null,
    Object? tags = null,
    Object? tagsLock = null,
    Object? created = null,
    Object? lastModified = null,
    Object? totalBookCountLock = null,
    Object? sharingLabels = null,
    Object? sharingLabelsLock = null,
    Object? links = null,
    Object? linksLock = null,
    Object? alternateTitles = null,
    Object? alternateTitlesLock = null,
    Object? totalBookCount = freezed,
    Object? ageRating = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SeriesStatus,
      statusLock: null == statusLock
          ? _value.statusLock
          : statusLock // ignore: cast_nullable_to_non_nullable
              as bool,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      titleLock: null == titleLock
          ? _value.titleLock
          : titleLock // ignore: cast_nullable_to_non_nullable
              as bool,
      titleSort: null == titleSort
          ? _value.titleSort
          : titleSort // ignore: cast_nullable_to_non_nullable
              as String,
      titleSortLock: null == titleSortLock
          ? _value.titleSortLock
          : titleSortLock // ignore: cast_nullable_to_non_nullable
              as bool,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      summaryLock: null == summaryLock
          ? _value.summaryLock
          : summaryLock // ignore: cast_nullable_to_non_nullable
              as bool,
      readingDirection: null == readingDirection
          ? _value.readingDirection
          : readingDirection // ignore: cast_nullable_to_non_nullable
              as String,
      readingDirectionLock: null == readingDirectionLock
          ? _value.readingDirectionLock
          : readingDirectionLock // ignore: cast_nullable_to_non_nullable
              as bool,
      publisher: null == publisher
          ? _value.publisher
          : publisher // ignore: cast_nullable_to_non_nullable
              as String,
      publisherLock: null == publisherLock
          ? _value.publisherLock
          : publisherLock // ignore: cast_nullable_to_non_nullable
              as bool,
      ageRatingLock: null == ageRatingLock
          ? _value.ageRatingLock
          : ageRatingLock // ignore: cast_nullable_to_non_nullable
              as bool,
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      languageLock: null == languageLock
          ? _value.languageLock
          : languageLock // ignore: cast_nullable_to_non_nullable
              as bool,
      genres: null == genres
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      genresLock: null == genresLock
          ? _value.genresLock
          : genresLock // ignore: cast_nullable_to_non_nullable
              as bool,
      tags: null == tags
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      tagsLock: null == tagsLock
          ? _value.tagsLock
          : tagsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      totalBookCountLock: null == totalBookCountLock
          ? _value.totalBookCountLock
          : totalBookCountLock // ignore: cast_nullable_to_non_nullable
              as bool,
      sharingLabels: null == sharingLabels
          ? _value.sharingLabels
          : sharingLabels // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      sharingLabelsLock: null == sharingLabelsLock
          ? _value.sharingLabelsLock
          : sharingLabelsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      links: null == links
          ? _value.links
          : links // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      linksLock: null == linksLock
          ? _value.linksLock
          : linksLock // ignore: cast_nullable_to_non_nullable
              as bool,
      alternateTitles: null == alternateTitles
          ? _value.alternateTitles
          : alternateTitles // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      alternateTitlesLock: null == alternateTitlesLock
          ? _value.alternateTitlesLock
          : alternateTitlesLock // ignore: cast_nullable_to_non_nullable
              as bool,
      totalBookCount: freezed == totalBookCount
          ? _value.totalBookCount
          : totalBookCount // ignore: cast_nullable_to_non_nullable
              as int?,
      ageRating: freezed == ageRating
          ? _value.ageRating
          : ageRating // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SeriesMetadataImplCopyWith<$Res>
    implements $SeriesMetadataCopyWith<$Res> {
  factory _$$SeriesMetadataImplCopyWith(_$SeriesMetadataImpl value,
          $Res Function(_$SeriesMetadataImpl) then) =
      __$$SeriesMetadataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {SeriesStatus status,
      bool statusLock,
      String title,
      bool titleLock,
      String titleSort,
      bool titleSortLock,
      String summary,
      bool summaryLock,
      String readingDirection,
      bool readingDirectionLock,
      String publisher,
      bool publisherLock,
      bool ageRatingLock,
      String language,
      bool languageLock,
      Set<String> genres,
      bool genresLock,
      Set<String> tags,
      bool tagsLock,
      DateTime created,
      DateTime lastModified,
      bool totalBookCountLock,
      Set<String> sharingLabels,
      bool sharingLabelsLock,
      Set<String> links,
      bool linksLock,
      Set<String> alternateTitles,
      bool alternateTitlesLock,
      int? totalBookCount,
      int? ageRating});
}

/// @nodoc
class __$$SeriesMetadataImplCopyWithImpl<$Res>
    extends _$SeriesMetadataCopyWithImpl<$Res, _$SeriesMetadataImpl>
    implements _$$SeriesMetadataImplCopyWith<$Res> {
  __$$SeriesMetadataImplCopyWithImpl(
      _$SeriesMetadataImpl _value, $Res Function(_$SeriesMetadataImpl) _then)
      : super(_value, _then);

  /// Create a copy of SeriesMetadata
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? statusLock = null,
    Object? title = null,
    Object? titleLock = null,
    Object? titleSort = null,
    Object? titleSortLock = null,
    Object? summary = null,
    Object? summaryLock = null,
    Object? readingDirection = null,
    Object? readingDirectionLock = null,
    Object? publisher = null,
    Object? publisherLock = null,
    Object? ageRatingLock = null,
    Object? language = null,
    Object? languageLock = null,
    Object? genres = null,
    Object? genresLock = null,
    Object? tags = null,
    Object? tagsLock = null,
    Object? created = null,
    Object? lastModified = null,
    Object? totalBookCountLock = null,
    Object? sharingLabels = null,
    Object? sharingLabelsLock = null,
    Object? links = null,
    Object? linksLock = null,
    Object? alternateTitles = null,
    Object? alternateTitlesLock = null,
    Object? totalBookCount = freezed,
    Object? ageRating = freezed,
  }) {
    return _then(_$SeriesMetadataImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SeriesStatus,
      statusLock: null == statusLock
          ? _value.statusLock
          : statusLock // ignore: cast_nullable_to_non_nullable
              as bool,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      titleLock: null == titleLock
          ? _value.titleLock
          : titleLock // ignore: cast_nullable_to_non_nullable
              as bool,
      titleSort: null == titleSort
          ? _value.titleSort
          : titleSort // ignore: cast_nullable_to_non_nullable
              as String,
      titleSortLock: null == titleSortLock
          ? _value.titleSortLock
          : titleSortLock // ignore: cast_nullable_to_non_nullable
              as bool,
      summary: null == summary
          ? _value.summary
          : summary // ignore: cast_nullable_to_non_nullable
              as String,
      summaryLock: null == summaryLock
          ? _value.summaryLock
          : summaryLock // ignore: cast_nullable_to_non_nullable
              as bool,
      readingDirection: null == readingDirection
          ? _value.readingDirection
          : readingDirection // ignore: cast_nullable_to_non_nullable
              as String,
      readingDirectionLock: null == readingDirectionLock
          ? _value.readingDirectionLock
          : readingDirectionLock // ignore: cast_nullable_to_non_nullable
              as bool,
      publisher: null == publisher
          ? _value.publisher
          : publisher // ignore: cast_nullable_to_non_nullable
              as String,
      publisherLock: null == publisherLock
          ? _value.publisherLock
          : publisherLock // ignore: cast_nullable_to_non_nullable
              as bool,
      ageRatingLock: null == ageRatingLock
          ? _value.ageRatingLock
          : ageRatingLock // ignore: cast_nullable_to_non_nullable
              as bool,
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      languageLock: null == languageLock
          ? _value.languageLock
          : languageLock // ignore: cast_nullable_to_non_nullable
              as bool,
      genres: null == genres
          ? _value._genres
          : genres // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      genresLock: null == genresLock
          ? _value.genresLock
          : genresLock // ignore: cast_nullable_to_non_nullable
              as bool,
      tags: null == tags
          ? _value._tags
          : tags // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      tagsLock: null == tagsLock
          ? _value.tagsLock
          : tagsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      totalBookCountLock: null == totalBookCountLock
          ? _value.totalBookCountLock
          : totalBookCountLock // ignore: cast_nullable_to_non_nullable
              as bool,
      sharingLabels: null == sharingLabels
          ? _value._sharingLabels
          : sharingLabels // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      sharingLabelsLock: null == sharingLabelsLock
          ? _value.sharingLabelsLock
          : sharingLabelsLock // ignore: cast_nullable_to_non_nullable
              as bool,
      links: null == links
          ? _value._links
          : links // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      linksLock: null == linksLock
          ? _value.linksLock
          : linksLock // ignore: cast_nullable_to_non_nullable
              as bool,
      alternateTitles: null == alternateTitles
          ? _value._alternateTitles
          : alternateTitles // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      alternateTitlesLock: null == alternateTitlesLock
          ? _value.alternateTitlesLock
          : alternateTitlesLock // ignore: cast_nullable_to_non_nullable
              as bool,
      totalBookCount: freezed == totalBookCount
          ? _value.totalBookCount
          : totalBookCount // ignore: cast_nullable_to_non_nullable
              as int?,
      ageRating: freezed == ageRating
          ? _value.ageRating
          : ageRating // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$SeriesMetadataImpl implements _SeriesMetadata {
  const _$SeriesMetadataImpl(
      {required this.status,
      required this.statusLock,
      required this.title,
      required this.titleLock,
      required this.titleSort,
      required this.titleSortLock,
      required this.summary,
      required this.summaryLock,
      required this.readingDirection,
      required this.readingDirectionLock,
      required this.publisher,
      required this.publisherLock,
      required this.ageRatingLock,
      required this.language,
      required this.languageLock,
      required final Set<String> genres,
      required this.genresLock,
      required final Set<String> tags,
      required this.tagsLock,
      required this.created,
      required this.lastModified,
      required this.totalBookCountLock,
      required final Set<String> sharingLabels,
      required this.sharingLabelsLock,
      required final Set<String> links,
      required this.linksLock,
      required final Set<String> alternateTitles,
      required this.alternateTitlesLock,
      this.totalBookCount,
      this.ageRating})
      : _genres = genres,
        _tags = tags,
        _sharingLabels = sharingLabels,
        _links = links,
        _alternateTitles = alternateTitles;

  @override
  final SeriesStatus status;
  @override
  final bool statusLock;
  @override
  final String title;
  @override
  final bool titleLock;
  @override
  final String titleSort;
  @override
  final bool titleSortLock;
  @override
  final String summary;
  @override
  final bool summaryLock;
  @override
  final String readingDirection;
  @override
  final bool readingDirectionLock;
  @override
  final String publisher;
  @override
  final bool publisherLock;
  @override
  final bool ageRatingLock;
  @override
  final String language;
  @override
  final bool languageLock;
  final Set<String> _genres;
  @override
  Set<String> get genres {
    if (_genres is EqualUnmodifiableSetView) return _genres;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_genres);
  }

  @override
  final bool genresLock;
  final Set<String> _tags;
  @override
  Set<String> get tags {
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_tags);
  }

  @override
  final bool tagsLock;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final bool totalBookCountLock;
  final Set<String> _sharingLabels;
  @override
  Set<String> get sharingLabels {
    if (_sharingLabels is EqualUnmodifiableSetView) return _sharingLabels;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_sharingLabels);
  }

  @override
  final bool sharingLabelsLock;
  final Set<String> _links;
  @override
  Set<String> get links {
    if (_links is EqualUnmodifiableSetView) return _links;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_links);
  }

  @override
  final bool linksLock;
  final Set<String> _alternateTitles;
  @override
  Set<String> get alternateTitles {
    if (_alternateTitles is EqualUnmodifiableSetView) return _alternateTitles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_alternateTitles);
  }

  @override
  final bool alternateTitlesLock;
  @override
  final int? totalBookCount;
  @override
  final int? ageRating;

  @override
  String toString() {
    return 'SeriesMetadata(status: $status, statusLock: $statusLock, title: $title, titleLock: $titleLock, titleSort: $titleSort, titleSortLock: $titleSortLock, summary: $summary, summaryLock: $summaryLock, readingDirection: $readingDirection, readingDirectionLock: $readingDirectionLock, publisher: $publisher, publisherLock: $publisherLock, ageRatingLock: $ageRatingLock, language: $language, languageLock: $languageLock, genres: $genres, genresLock: $genresLock, tags: $tags, tagsLock: $tagsLock, created: $created, lastModified: $lastModified, totalBookCountLock: $totalBookCountLock, sharingLabels: $sharingLabels, sharingLabelsLock: $sharingLabelsLock, links: $links, linksLock: $linksLock, alternateTitles: $alternateTitles, alternateTitlesLock: $alternateTitlesLock, totalBookCount: $totalBookCount, ageRating: $ageRating)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SeriesMetadataImpl &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.statusLock, statusLock) ||
                other.statusLock == statusLock) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.titleLock, titleLock) ||
                other.titleLock == titleLock) &&
            (identical(other.titleSort, titleSort) ||
                other.titleSort == titleSort) &&
            (identical(other.titleSortLock, titleSortLock) ||
                other.titleSortLock == titleSortLock) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryLock, summaryLock) ||
                other.summaryLock == summaryLock) &&
            (identical(other.readingDirection, readingDirection) ||
                other.readingDirection == readingDirection) &&
            (identical(other.readingDirectionLock, readingDirectionLock) ||
                other.readingDirectionLock == readingDirectionLock) &&
            (identical(other.publisher, publisher) ||
                other.publisher == publisher) &&
            (identical(other.publisherLock, publisherLock) ||
                other.publisherLock == publisherLock) &&
            (identical(other.ageRatingLock, ageRatingLock) ||
                other.ageRatingLock == ageRatingLock) &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.languageLock, languageLock) ||
                other.languageLock == languageLock) &&
            const DeepCollectionEquality().equals(other._genres, _genres) &&
            (identical(other.genresLock, genresLock) ||
                other.genresLock == genresLock) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.tagsLock, tagsLock) ||
                other.tagsLock == tagsLock) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.totalBookCountLock, totalBookCountLock) ||
                other.totalBookCountLock == totalBookCountLock) &&
            const DeepCollectionEquality()
                .equals(other._sharingLabels, _sharingLabels) &&
            (identical(other.sharingLabelsLock, sharingLabelsLock) ||
                other.sharingLabelsLock == sharingLabelsLock) &&
            const DeepCollectionEquality().equals(other._links, _links) &&
            (identical(other.linksLock, linksLock) ||
                other.linksLock == linksLock) &&
            const DeepCollectionEquality()
                .equals(other._alternateTitles, _alternateTitles) &&
            (identical(other.alternateTitlesLock, alternateTitlesLock) ||
                other.alternateTitlesLock == alternateTitlesLock) &&
            (identical(other.totalBookCount, totalBookCount) ||
                other.totalBookCount == totalBookCount) &&
            (identical(other.ageRating, ageRating) ||
                other.ageRating == ageRating));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        status,
        statusLock,
        title,
        titleLock,
        titleSort,
        titleSortLock,
        summary,
        summaryLock,
        readingDirection,
        readingDirectionLock,
        publisher,
        publisherLock,
        ageRatingLock,
        language,
        languageLock,
        const DeepCollectionEquality().hash(_genres),
        genresLock,
        const DeepCollectionEquality().hash(_tags),
        tagsLock,
        created,
        lastModified,
        totalBookCountLock,
        const DeepCollectionEquality().hash(_sharingLabels),
        sharingLabelsLock,
        const DeepCollectionEquality().hash(_links),
        linksLock,
        const DeepCollectionEquality().hash(_alternateTitles),
        alternateTitlesLock,
        totalBookCount,
        ageRating
      ]);

  /// Create a copy of SeriesMetadata
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SeriesMetadataImplCopyWith<_$SeriesMetadataImpl> get copyWith =>
      __$$SeriesMetadataImplCopyWithImpl<_$SeriesMetadataImpl>(
          this, _$identity);
}

abstract class _SeriesMetadata implements SeriesMetadata {
  const factory _SeriesMetadata(
      {required final SeriesStatus status,
      required final bool statusLock,
      required final String title,
      required final bool titleLock,
      required final String titleSort,
      required final bool titleSortLock,
      required final String summary,
      required final bool summaryLock,
      required final String readingDirection,
      required final bool readingDirectionLock,
      required final String publisher,
      required final bool publisherLock,
      required final bool ageRatingLock,
      required final String language,
      required final bool languageLock,
      required final Set<String> genres,
      required final bool genresLock,
      required final Set<String> tags,
      required final bool tagsLock,
      required final DateTime created,
      required final DateTime lastModified,
      required final bool totalBookCountLock,
      required final Set<String> sharingLabels,
      required final bool sharingLabelsLock,
      required final Set<String> links,
      required final bool linksLock,
      required final Set<String> alternateTitles,
      required final bool alternateTitlesLock,
      final int? totalBookCount,
      final int? ageRating}) = _$SeriesMetadataImpl;

  @override
  SeriesStatus get status;
  @override
  bool get statusLock;
  @override
  String get title;
  @override
  bool get titleLock;
  @override
  String get titleSort;
  @override
  bool get titleSortLock;
  @override
  String get summary;
  @override
  bool get summaryLock;
  @override
  String get readingDirection;
  @override
  bool get readingDirectionLock;
  @override
  String get publisher;
  @override
  bool get publisherLock;
  @override
  bool get ageRatingLock;
  @override
  String get language;
  @override
  bool get languageLock;
  @override
  Set<String> get genres;
  @override
  bool get genresLock;
  @override
  Set<String> get tags;
  @override
  bool get tagsLock;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  bool get totalBookCountLock;
  @override
  Set<String> get sharingLabels;
  @override
  bool get sharingLabelsLock;
  @override
  Set<String> get links;
  @override
  bool get linksLock;
  @override
  Set<String> get alternateTitles;
  @override
  bool get alternateTitlesLock;
  @override
  int? get totalBookCount;
  @override
  int? get ageRating;

  /// Create a copy of SeriesMetadata
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SeriesMetadataImplCopyWith<_$SeriesMetadataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
