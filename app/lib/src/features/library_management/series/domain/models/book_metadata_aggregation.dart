import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/common/models/author.dart';

part 'book_metadata_aggregation.freezed.dart';

/// Domain object to handle the aggregation data operation for a book
@freezed
class BookMetadataAggregation with _$BookMetadataAggregation {
  /// Creates a [BookMetadataAggregation] instance
  const factory BookMetadataAggregation({
    required List<Author> authors,
    required Set<String> tags,
    required String summary,
    required String summaryNumber,
    required DateTime created,
    required DateTime lastModified,
    DateTime? releaseDate,
  }) = _BookMetadataAggregation;
}
