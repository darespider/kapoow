import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/src/common_widgets/advice.dart';
import 'package:kapoow/src/common_widgets/app_chip.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_summary.dart';
import 'package:kapoow/src/features/library_management/series/ui/controllers/pods.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_book_tabs.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_books_grid.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Screen to display the details of a series
class SeriesScreen extends HookConsumerWidget {
  /// Creates instance of [SeriesScreen] widget
  const SeriesScreen({
    required this.seriesId,
    super.key,
  });

  /// id of the item to get
  final String seriesId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final status = useState<ReadStatus?>(null);
    final seriesAsync = ref.watch(seriesPod(seriesId));
    final paginatedPod = seriesBooksPaginatedPod((seriesId, status.value));
    final seriesBooksPageState = ref.watch(paginatedPod);

    return TypicalWhenBuilder(
      asyncValue: seriesAsync,
      dataBuilder: (context, series) => Scaffold(
        appBar: AppBar(
          title: Text(
            series.name,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppPaddings.small),
          child: RefreshIndicator(
            onRefresh: () async {
              await Future.wait([
                ref.refresh(seriesPod(seriesId).future),
                ref.refresh(seriesBooksFirstPagePod(seriesId).future),
                ref.refresh(
                  seriesBooksFirstPagePod(
                    seriesId,
                    readStatus: ReadStatus.read,
                  ).future,
                ),
                ref.refresh(
                  seriesBooksFirstPagePod(
                    seriesId,
                    readStatus: ReadStatus.unread,
                  ).future,
                ),
              ]);
            },
            child: CustomScrollView(
              slivers: [
                // TODO(rurickdev): Split these slivers into single files
                SliverToBoxAdapter(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(AppPaddings.small),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ItemSummary(summary: series.booksMetadata.summary),
                          Divider(
                            color: context.colorScheme.onSurfaceVariant
                                .withOpacity(0.4),
                          ),
                          Wrap(
                            runSpacing: AppPaddings.xSmall,
                            spacing: AppPaddings.xSmall,
                            children: [
                              AppChip(
                                text:
                                    series.metadata.status.value.toLowerCase(),
                              ),
                              if (series.metadata.language.isNotEmpty)
                                AppChip(text: series.metadata.language),

                              // TODO(rurickdev): Use publisher logo?
                              if (series.metadata.publisher.isNotEmpty)
                                AppChip(text: series.metadata.publisher),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                if (series.booksCount == series.booksUnreadCount)
                  SliverToBoxAdapter(
                    child: Advice(
                      icon: PhosphorIcons.bookOpenText(
                        PhosphorIconsStyle.duotone,
                      ),
                      // TODO(rurickdev): [l10n] translate
                      text: 'New to this series? Dive in with the books below '
                          'and begin your journey.',
                    ),
                  ),
                SliverPersistentHeader(
                  pinned: true,
                  delegate: SeriesBookStatusPersistentTabs(
                    onSelectionChanged: (value) => status.value = value.first,
                    series: series,
                    status: {status.value},
                  ),
                ),
                SeriesBooksPaginatedGrid(
                  paginatedState: seriesBooksPageState,
                  nextPageLoader: ref.watch(paginatedPod.notifier).loadNextPage,
                ),
              ].separated(
                mainAxisExtent: AppPaddings.medium,
                sliver: true,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
