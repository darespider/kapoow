import 'package:flutter/material.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/routes/routes_data.dart';

/// Displays the cover image of the passed [series]
class SeriesCover extends StatelessWidget {
  /// Creates a [SeriesCover] instance
  const SeriesCover({
    required this.series,
    super.key,
  });

  /// [Series] to read data from
  final Series series;

  @override
  Widget build(BuildContext context) {
    return CoverImage(
      onTap: () => SeriesRoute(id: series.id).go(context),
      coverUrl: series.thumbnailUri,
      decoration: CoverDecorationData(
        itemsCount: series.booksCount,
        finishedItemsCount: series.booksReadCount,
        finished: series.booksCount == series.booksReadCount,
      ),
    );
  }
}
