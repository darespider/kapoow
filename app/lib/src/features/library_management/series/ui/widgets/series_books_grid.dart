import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_loader.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/paginated_sliver_grid.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Builds a [PaginatedSliverGrid] with the books from the [paginatedState].
/// it will display a "no books" message if the paginated state has no results.
class SeriesBooksPaginatedGrid extends ConsumerWidget {
  /// Creates a [SeriesBooksPaginatedGrid] instance
  const SeriesBooksPaginatedGrid({
    required this.paginatedState,
    required this.nextPageLoader,
    super.key,
  });

  /// Paginated state to get the results from.
  final PaginatedState<Book> paginatedState;

  /// How to load the next page.
  final VoidCallback nextPageLoader;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return PaginatedSliverGrid(
      paginatedState: paginatedState,
      nextPageLoader: nextPageLoader,
      scrollDirection: Axis.vertical,
      initialLoadIndicator: const Center(child: AppLoader()),
      loadingPageIndicator: const CoverCardLoader(),
      emptyIndicator: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              // TODO(rurickdev): [l10n] translate
              'No Books',
              style: context.textTheme.headlineMedium,
            ),
            Assets.images.search.searchEmpty.svg(
              width: 300,
              height: 300,
            ),
          ].separated(mainAxisExtent: AppPaddings.medium),
        ),
      ),
      itemBuilder: (context, book) => BookCover(book: book),
    );
  }
}
