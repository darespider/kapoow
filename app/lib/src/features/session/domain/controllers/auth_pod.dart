import 'dart:async';
import 'dart:convert';

import 'package:kapoow/src/features/rest_api/domain/api_exceptions.dart';
import 'package:kapoow/src/features/rest_api/domain/pods.dart';
import 'package:kapoow/src/features/session/domain/controllers/session_pods.dart';
import 'package:kapoow/src/features/session/domain/models/session_data.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'auth_pod.g.dart';

/// Controller that handles the login and logout of the user
@riverpod
class Auth extends _$Auth {
  @override
  FutureOr<void> build() {}

  /// Deletes the saved session and clears the state
  Future<void> logout() async {
    state = const AsyncLoading();
    await ref.read(sessionRepositoryPod).deleteSession();
    ref.invalidate(sessionAsyncPod);
    state = const AsyncValue.data(null);
  }

  /// tries to do a login calling the `me` endpoint
  /// if succeed then the state is updated with the host and token
  /// if fails then the state will be an error state
  Future<AuthResult> tryLogin({
    required String host,
    required String email,
    required String password,
  }) async {
    state = const AsyncLoading();
    final token = encodeToken(email, password);
    final sessionData = SessionData(
      token: token,
      host: host,
    );
    ref.read(basicTokenPod.notifier).setToken(token);
    ref.read(baseUriPod.notifier).setUri(host);
    try {
      await ref.read(userRepositoryPod).getMeData();
      await ref.read(sessionRepositoryPod).saveSession(sessionData);
    } on UnauthorizedApiException {
      await ref.read(sessionRepositoryPod).deleteSession();
      return UnauthorizedAuth();
    } catch (_) {
      return FailedAuth();
    } finally {
      state = const AsyncValue.data(null);
    }
    return SuccessAuth();
  }
}

/// Sealed class representing the posible auth result
sealed class AuthResult {
  /// Union callbacks
  T when<T>({
    required T Function() success,
    required T Function() unauthorized,
    required T Function() failure,
  }) =>
      switch (this) {
        SuccessAuth() => success(),
        UnauthorizedAuth() => unauthorized(),
        FailedAuth() => failure(),
      };
}

/// Successful auth result. The use could log in
class SuccessAuth extends AuthResult {}

/// The server exist but the user is not authorized.
///
/// Could be wrong credentials
class UnauthorizedAuth extends AuthResult {}

/// Use for any error, could be network, non existing server, wrong credentials,
/// timeout, etc...
class FailedAuth extends AuthResult {}

/// Encodes an [email] and [password] in the format `email:password`
/// in base64
String encodeToken(String email, String password) =>
    base64Encode('$email:$password'.codeUnits);

/// Decodes a base64 [token] and returns a [Record] where the `$0`
/// is the `email` and the `$1` is the `password`
///
/// the token should be encoded like `email:password` otherwise will
/// throw a `StateError`
(String email, String password) decodeToken(String token) {
  final decodeToken = base64Decode(token);
  final emailAndPassword = String.fromCharCodes(decodeToken).split(':');
  if (emailAndPassword.length != 2) {
    throw StateError(
      'Expected a "email:password" token, got instead "$emailAndPassword"',
    );
  }
  final email = emailAndPassword.first;
  final password = emailAndPassword.last;
  return (email, password);
}
