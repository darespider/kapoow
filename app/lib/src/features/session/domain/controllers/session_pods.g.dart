// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session_pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userRepositoryHash() => r'01eeb2d465132de23cfad89c02bd2715d5e4603f';

/// Provides a [UserRepository]
///
/// Copied from [userRepository].
@ProviderFor(userRepository)
final userRepositoryPod = Provider<UserRepository>.internal(
  userRepository,
  name: r'userRepositoryPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef UserRepositoryRef = ProviderRef<UserRepository>;
String _$sessionDaoHash() => r'aea361c91cb8fb25a03bfd1ea523c83e58992cff';

/// Provides a [SessionDao]
///
/// Copied from [sessionDao].
@ProviderFor(sessionDao)
final sessionDaoPod = Provider<SessionDao>.internal(
  sessionDao,
  name: r'sessionDaoPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$sessionDaoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SessionDaoRef = ProviderRef<SessionDao>;
String _$sessionRepositoryHash() => r'ecb2b5927dd52b5f1662f914d59cbd93a6bbb78f';

/// Provides a [SessionRepository]
///
/// Copied from [sessionRepository].
@ProviderFor(sessionRepository)
final sessionRepositoryPod = Provider<SessionRepository>.internal(
  sessionRepository,
  name: r'sessionRepositoryPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sessionRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SessionRepositoryRef = ProviderRef<SessionRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
