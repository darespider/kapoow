/// Model with the host and the user token
class SessionData {
  /// Creates instance of [SessionData]
  const SessionData({
    required this.token,
    required this.host,
  });

  /// Creates in invalid instance of [SessionData]
  const SessionData.invalid()
      : host = '',
        token = '';

  /// Base64 user session token
  final String token;

  /// Server host
  final String host;

  /// returns `true` if the [token] and the [host] are not empty
  bool get valid => (token.isNotEmpty) && (host.isNotEmpty);

  /// returns `true` if the [token] or the [host] are empty
  bool get invalid => !valid;

  /// Creates a new [SessionData] instance from the current one and replaces
  /// the old value with new passed ones.
  ///
  /// Passing `null` will not modify the value
  SessionData copyWith({
    String? token,
    String? host,
  }) =>
      SessionData(
        token: token ?? this.token,
        host: host ?? this.host,
      );
}
