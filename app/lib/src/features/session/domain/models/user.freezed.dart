// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$User {
  String get id => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  Set<String> get roles => throw _privateConstructorUsedError;
  bool get sharedAllLibraries => throw _privateConstructorUsedError;
  Set<String> get sharedLibrariesIds => throw _privateConstructorUsedError;
  Set<String> get labelsAllow => throw _privateConstructorUsedError;
  Set<String> get labelsExclude => throw _privateConstructorUsedError;
  AgeRestriction? get ageRestriction => throw _privateConstructorUsedError;

  /// Create a copy of User
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $UserCopyWith<User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res, User>;
  @useResult
  $Res call(
      {String id,
      String email,
      Set<String> roles,
      bool sharedAllLibraries,
      Set<String> sharedLibrariesIds,
      Set<String> labelsAllow,
      Set<String> labelsExclude,
      AgeRestriction? ageRestriction});

  $AgeRestrictionCopyWith<$Res>? get ageRestriction;
}

/// @nodoc
class _$UserCopyWithImpl<$Res, $Val extends User>
    implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of User
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? email = null,
    Object? roles = null,
    Object? sharedAllLibraries = null,
    Object? sharedLibrariesIds = null,
    Object? labelsAllow = null,
    Object? labelsExclude = null,
    Object? ageRestriction = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      roles: null == roles
          ? _value.roles
          : roles // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      sharedAllLibraries: null == sharedAllLibraries
          ? _value.sharedAllLibraries
          : sharedAllLibraries // ignore: cast_nullable_to_non_nullable
              as bool,
      sharedLibrariesIds: null == sharedLibrariesIds
          ? _value.sharedLibrariesIds
          : sharedLibrariesIds // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      labelsAllow: null == labelsAllow
          ? _value.labelsAllow
          : labelsAllow // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      labelsExclude: null == labelsExclude
          ? _value.labelsExclude
          : labelsExclude // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      ageRestriction: freezed == ageRestriction
          ? _value.ageRestriction
          : ageRestriction // ignore: cast_nullable_to_non_nullable
              as AgeRestriction?,
    ) as $Val);
  }

  /// Create a copy of User
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $AgeRestrictionCopyWith<$Res>? get ageRestriction {
    if (_value.ageRestriction == null) {
      return null;
    }

    return $AgeRestrictionCopyWith<$Res>(_value.ageRestriction!, (value) {
      return _then(_value.copyWith(ageRestriction: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$UserImplCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$$UserImplCopyWith(
          _$UserImpl value, $Res Function(_$UserImpl) then) =
      __$$UserImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String email,
      Set<String> roles,
      bool sharedAllLibraries,
      Set<String> sharedLibrariesIds,
      Set<String> labelsAllow,
      Set<String> labelsExclude,
      AgeRestriction? ageRestriction});

  @override
  $AgeRestrictionCopyWith<$Res>? get ageRestriction;
}

/// @nodoc
class __$$UserImplCopyWithImpl<$Res>
    extends _$UserCopyWithImpl<$Res, _$UserImpl>
    implements _$$UserImplCopyWith<$Res> {
  __$$UserImplCopyWithImpl(_$UserImpl _value, $Res Function(_$UserImpl) _then)
      : super(_value, _then);

  /// Create a copy of User
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? email = null,
    Object? roles = null,
    Object? sharedAllLibraries = null,
    Object? sharedLibrariesIds = null,
    Object? labelsAllow = null,
    Object? labelsExclude = null,
    Object? ageRestriction = freezed,
  }) {
    return _then(_$UserImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      roles: null == roles
          ? _value._roles
          : roles // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      sharedAllLibraries: null == sharedAllLibraries
          ? _value.sharedAllLibraries
          : sharedAllLibraries // ignore: cast_nullable_to_non_nullable
              as bool,
      sharedLibrariesIds: null == sharedLibrariesIds
          ? _value._sharedLibrariesIds
          : sharedLibrariesIds // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      labelsAllow: null == labelsAllow
          ? _value._labelsAllow
          : labelsAllow // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      labelsExclude: null == labelsExclude
          ? _value._labelsExclude
          : labelsExclude // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      ageRestriction: freezed == ageRestriction
          ? _value.ageRestriction
          : ageRestriction // ignore: cast_nullable_to_non_nullable
              as AgeRestriction?,
    ));
  }
}

/// @nodoc

class _$UserImpl implements _User {
  const _$UserImpl(
      {required this.id,
      required this.email,
      required final Set<String> roles,
      required this.sharedAllLibraries,
      required final Set<String> sharedLibrariesIds,
      required final Set<String> labelsAllow,
      required final Set<String> labelsExclude,
      this.ageRestriction})
      : _roles = roles,
        _sharedLibrariesIds = sharedLibrariesIds,
        _labelsAllow = labelsAllow,
        _labelsExclude = labelsExclude;

  @override
  final String id;
  @override
  final String email;
  final Set<String> _roles;
  @override
  Set<String> get roles {
    if (_roles is EqualUnmodifiableSetView) return _roles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_roles);
  }

  @override
  final bool sharedAllLibraries;
  final Set<String> _sharedLibrariesIds;
  @override
  Set<String> get sharedLibrariesIds {
    if (_sharedLibrariesIds is EqualUnmodifiableSetView)
      return _sharedLibrariesIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_sharedLibrariesIds);
  }

  final Set<String> _labelsAllow;
  @override
  Set<String> get labelsAllow {
    if (_labelsAllow is EqualUnmodifiableSetView) return _labelsAllow;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_labelsAllow);
  }

  final Set<String> _labelsExclude;
  @override
  Set<String> get labelsExclude {
    if (_labelsExclude is EqualUnmodifiableSetView) return _labelsExclude;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_labelsExclude);
  }

  @override
  final AgeRestriction? ageRestriction;

  @override
  String toString() {
    return 'User(id: $id, email: $email, roles: $roles, sharedAllLibraries: $sharedAllLibraries, sharedLibrariesIds: $sharedLibrariesIds, labelsAllow: $labelsAllow, labelsExclude: $labelsExclude, ageRestriction: $ageRestriction)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.email, email) || other.email == email) &&
            const DeepCollectionEquality().equals(other._roles, _roles) &&
            (identical(other.sharedAllLibraries, sharedAllLibraries) ||
                other.sharedAllLibraries == sharedAllLibraries) &&
            const DeepCollectionEquality()
                .equals(other._sharedLibrariesIds, _sharedLibrariesIds) &&
            const DeepCollectionEquality()
                .equals(other._labelsAllow, _labelsAllow) &&
            const DeepCollectionEquality()
                .equals(other._labelsExclude, _labelsExclude) &&
            (identical(other.ageRestriction, ageRestriction) ||
                other.ageRestriction == ageRestriction));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      email,
      const DeepCollectionEquality().hash(_roles),
      sharedAllLibraries,
      const DeepCollectionEquality().hash(_sharedLibrariesIds),
      const DeepCollectionEquality().hash(_labelsAllow),
      const DeepCollectionEquality().hash(_labelsExclude),
      ageRestriction);

  /// Create a copy of User
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UserImplCopyWith<_$UserImpl> get copyWith =>
      __$$UserImplCopyWithImpl<_$UserImpl>(this, _$identity);
}

abstract class _User implements User {
  const factory _User(
      {required final String id,
      required final String email,
      required final Set<String> roles,
      required final bool sharedAllLibraries,
      required final Set<String> sharedLibrariesIds,
      required final Set<String> labelsAllow,
      required final Set<String> labelsExclude,
      final AgeRestriction? ageRestriction}) = _$UserImpl;

  @override
  String get id;
  @override
  String get email;
  @override
  Set<String> get roles;
  @override
  bool get sharedAllLibraries;
  @override
  Set<String> get sharedLibrariesIds;
  @override
  Set<String> get labelsAllow;
  @override
  Set<String> get labelsExclude;
  @override
  AgeRestriction? get ageRestriction;

  /// Create a copy of User
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UserImplCopyWith<_$UserImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
