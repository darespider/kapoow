import 'package:kapoow/src/features/rest_api/data/datasources/user/user_datasource.dart';
import 'package:kapoow/src/features/session/data/repositories/user_repository.dart';
import 'package:kapoow/src/features/session/domain/models/user.dart';
import 'package:kapoow/src/features/session/domain/repositories/dto_domain_parser.dart';

/// User repository that gets the books from an api client
class UserRepositoryImp implements UserRepository {
  /// Creates a [UserRepository] and requieres a [UserDatasource]
  const UserRepositoryImp(this._datasource);

  final UserDatasource _datasource;

  @override
  Future<User> getMeData() async {
    final dto = await _datasource.getUserMe();
    return dto.toDomain();
  }

  @override
  Future<User> getUserData(String id) {
    throw UnimplementedError();
  }

  @override
  Future<List<User>> getUsersData() {
    throw UnimplementedError();
  }
}
