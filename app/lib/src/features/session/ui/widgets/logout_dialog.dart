import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/common_widgets/modal_mixin.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// [AlertDialog] questioning the user about login out of the app
class LogoutDialog extends ConsumerWidget with ModalMixin<bool> {
  /// Creates instance of [LogoutDialog] widget
  const LogoutDialog({super.key});

  @override
  bool get fallbackValue => false;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return AlertDialog(
      icon: PhosphorIcon(
        PhosphorIcons.warning(PhosphorIconsStyle.duotone),
        color: context.colorScheme.error,
        size: 36,
      ),
      // TODO(rurickdev): [l10n] translate
      title: const Text('Are you sure you want to log out?'),
      content: const Text(
        'You will need to add your server again when you want to login back.',
      ),
      actionsAlignment: MainAxisAlignment.spaceAround,
      actionsOverflowAlignment: OverflowBarAlignment.center,
      actionsOverflowButtonSpacing: AppPaddings.small,
      actions: [
        FilledButton.icon(
          onPressed: () => context.pop(true),
          label: const Text('yes, logout'),
          icon: PhosphorIcon(PhosphorIcons.signOut(PhosphorIconsStyle.duotone)),
        ),
        OutlinedButton(
          onPressed: () => context.pop(false),
          child: const Text('cancel'),
        ),
      ].wrappedWith(
        (child) => ConstrainedBox(
          constraints: const BoxConstraints(
            maxWidth: 250,
          ),
          child: SizedBox(
            width: double.infinity,
            child: child,
          ),
        ),
      ),
    );
  }
}
