import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_text_field.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/host_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/pods.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Host Input widget that uses `AppTextFiel` widget
/// It validates if the value is a valid host or ip address.
class HostInputField extends ConsumerWidget {
  /// Creates an instance of [HostInputField] widget
  const HostInputField({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final hostController =
        ref.watch(sessionFormPod.select((value) => value.host));

    final translations = context.translations;

    return AppTextField(
      initialValue: hostController.value,
      hint: translations.sessionWidgetsInputHostHint,
      autofillHints: const [AutofillHints.url],
      label: translations.sessionWidgetsInputHost,
      prefixIcon: PhosphorIcon(
        PhosphorIcons.hardDrives(PhosphorIconsStyle.duotone),
      ),
      keyboardType: TextInputType.url,
      validator: (value) {
        if (value == null) return null;
        return hostController.validator(value)?.text(context);
      },
      onChanged: (value) {
        ref.read(sessionFormPod.notifier).setHost(value);
      },
    );
  }
}
