import 'package:kapoow/src/features/session/ui/controllers/input_controllers/email_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/host_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/password_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/port_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/session_form_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the session form state
@riverpod
class SessionForm extends _$SessionForm {
  @override
  SessionFormState build() {
    return SessionFormState();
  }

  /// updates the state for host
  void setHost(String host) {
    state = state.copyWith(host: HostInput.dirty(host));
  }

  /// updates the state for port
  void setPort(String port) {
    state = state.copyWith(port: PortInput.dirty(int.tryParse(port) ?? 0));
  }

  /// updates the state for email
  void setEmail(String email) {
    state = state.copyWith(email: EmailInput.dirty(email));
  }

  /// updates the state for password
  void setPassword(String password) {
    state = state.copyWith(password: PasswordInput.dirty(password));
  }
}
