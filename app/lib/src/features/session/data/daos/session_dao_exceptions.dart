/// Throw when there is a exception in the SessionDao
class SessionDaoException implements Exception {}

/// Throw when failing to retrieve the token
class RetrievingTokenException implements SessionDaoException {}

/// Throw when failing to retrieve the host
class RetrievingHostException implements SessionDaoException {}
