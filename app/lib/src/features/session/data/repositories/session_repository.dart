import 'package:kapoow/src/features/session/domain/models/session_data.dart';

/// Repository to read and save session data from the secure storage
abstract class SessionRepository {
  /// Saves the [SessionData] `token` and `host`
  Future<void> saveSession(SessionData sessionData);

  /// Returns the previously saved local [SessionData] but that doesn't guaranty
  /// that is a valid session, you should always check using [SessionData.valid]
  Future<SessionData> getLocalSession();

  /// Deletes the value with the keys `token` and `host` from the secure storage
  Future<void> deleteSession();
}
