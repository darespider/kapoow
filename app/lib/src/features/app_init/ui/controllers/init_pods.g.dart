// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'init_pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$initHash() => r'0eee468628877e68db2dfb6c27ad6f3827f88069';

/// Pod used to handle the async init process
///
/// Copied from [init].
@ProviderFor(init)
final initPod = AutoDisposeFutureProvider<InitState>.internal(
  init,
  name: r'initPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$initHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef InitRef = AutoDisposeFutureProviderRef<InitState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
