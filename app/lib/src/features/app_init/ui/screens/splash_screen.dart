import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Screen with a circular progress indicator
class SplashScreen extends StatelessWidget {
  /// Creates instance of [SplashScreen] widget
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final colorScheme = context.colorScheme;

    return Scaffold(
      body: SizedBox.expand(
        child: ZoomIn(
          child: DecoratedBox(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  colorScheme.primaryContainer,
                  colorScheme.secondaryContainer,
                ],
              ),
            ),
            child: Column(
              children: [
                Expanded(
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      Assets.images.splash.appIconBlank.svg(
                        height: 200,
                      ),
                      const Center(child: AppLoader()),
                    ],
                  ),
                ),
                const SizedBox(height: AppPaddings.medium),
                Text(
                  context.translations.splashScreenLoading,
                  style: context.textTheme.headlineSmall,
                ),
                const SizedBox(height: AppPaddings.large),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
