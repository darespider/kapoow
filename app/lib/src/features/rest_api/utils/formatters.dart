import 'package:intl/intl.dart';

/// Formats the date as api request
final apiDateFormatter = DateFormat('yyyy-MM-dd');
