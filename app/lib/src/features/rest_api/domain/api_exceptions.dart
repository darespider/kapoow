// TODO(rurickdev): Replace error strings with "keys" to translate display texts

/// Generic Api Exception, it is thrown when an expected error happens on the
/// network layer.
abstract class ApiException implements Exception {
  /// Creates an instance of  [ApiException]
  const ApiException({
    required this.title,
    this.message = '',
    this.error,
    this.stackTrace,
  });

  /// Original error or exception catch and converted to this.
  final Object? error;

  /// Stacktrace of when the error or exception was launched
  final StackTrace? stackTrace;

  /// Message with details of the error
  final String message;

  /// Short general description of the error
  final String title;

  @override
  String toString() => '[$runtimeType]: $error';
}

/// Exception throw when network fails due not having connection
class NoConnectionApiException extends ApiException {
  /// Creates an instance of  [NoConnectionApiException]
  const NoConnectionApiException({
    super.error,
    super.stackTrace,
    super.title = 'Lost in Space!',
    super.message = 'It seems we’ve drifted offline. '
        'Check your connection and let’s get back to the action!',
  });
}

/// Exception throw when network fails due a timeout on the response
class TimeoutApiException extends ApiException {
  /// Creates an instance of  [TimeoutApiException]
  const TimeoutApiException({
    super.error,
    super.stackTrace,
    super.title = 'Uh-oh, Time Ran Out!',
    super.message = 'Our signal got lost in the multiverse. '
        'Try again, or wait a moment and we’ll reconnect.',
  });
}

/// Exception throw when network fails trying to format the response
class FormatApiException<Dto> extends ApiException {
  /// Creates an instance of  [FormatApiException]
  const FormatApiException({
    super.error,
    super.stackTrace,
    super.title = 'Oops, Something’s Out of Place!',
    super.message = 'Looks like we’ve hit a glitch in the matrix. '
        'Refresh and let’s give it another go!',
  });
}

/// Exception throw when network fails and the error is unknown
class UnknownCodeApiException extends ApiException {
  /// Creates an instance of  [UnknownCodeApiException]
  const UnknownCodeApiException({
    required this.httpStatusCode,
    super.error,
    super.stackTrace,
    super.title = 'Mystery Error Alert!',
    super.message = 'We’re not sure what happened there. '
        'Refresh the page or try again in a bit.',
  });

  /// Status code of the response with the unknown error
  final int httpStatusCode;
}

/// Exception throw when network fails and is the server who failed
class InternalServerApiException extends ApiException {
  /// Creates an instance of  [InternalServerApiException]
  const InternalServerApiException({
    super.error,
    super.stackTrace,
    super.title = 'Server’s Taking a Breather!',
    super.message = 'Our servers are catching their breath. '
        'Hang tight, and we’ll be back in action shortly.',
  });
}

/// Exception throw when network fails due a not found resource
class NotFoundApiException extends ApiException {
  /// Creates an instance of  [NotFoundApiException]
  const NotFoundApiException({
    super.error,
    super.stackTrace,
    super.title = 'Page Not Found, Hero!',
    super.message = 'Looks like this content is hiding. '
        'Try searching for something else or check back later.',
  });
}

/// Exception throw when network fails due the user not being authorized to use
/// the resource.
class UnauthorizedApiException extends ApiException {
  /// Creates an instance of  [UnauthorizedApiException]
  const UnauthorizedApiException({
    super.error,
    super.stackTrace,
    super.title = 'Access Denied—You Shall Not Pass!',
    super.message = 'You need the right powers to view this. '
        'Log in or check your access level.',
  });
}

/// Exception throw when network fails due the user has forbidden the use of
/// the resource.
class ForbiddenApiException extends ApiException {
  /// Creates an instance of  [ForbiddenApiException]
  const ForbiddenApiException({
    super.error,
    super.stackTrace,
    super.title = 'Hold Up, That’s Off-Limits!',
    super.message = 'You don’t have permission to enter this secret lair. '
        'Reach out if you think it’s a mistake.',
  });
}

/// Exception throw when network fails due bad data send to the server
class BadRequestApiException extends ApiException {
  /// Creates an instance of  [BadRequestApiException]
  const BadRequestApiException({
    super.error,
    super.stackTrace,
    super.title = 'Hmm, That Didn’t Work!',
    super.message = 'Your request went sideways. '
        'Double-check what you entered and try again.',
  });
}

/// Exception throw when network fails due the server failing due an external
/// dependency
class FailedDependencyApiException extends ApiException {
  /// Creates an instance of  [FailedDependencyApiException]
  const FailedDependencyApiException({
    super.error,
    super.stackTrace,
    super.title = 'Service Unavailable—We Hit a Snag!',
    super.message =
        'A critical service is down. ' 'Hang tight while we work to fix it!',
  });
}

/// Exception throw when network receives a redirect response
class RedirectApiException extends ApiException {
  /// Creates an instance of  [RedirectApiException]
  const RedirectApiException({
    super.error,
    super.stackTrace,
    super.title = 'Looks Like We’ve Moved!',
    super.message = 'This page has packed up and moved elsewhere. '
        'Refresh or head back to explore more.',
  });
}
