import 'dart:async';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';

/// This class responsibility is to do the api request using a [RestApi] object
/// and returning the bytes of the response body as is.
///
/// It should not parse the responses to domain neither use a [http.Client]
/// object directly.
class DownloadsApi {
  /// Created a [DownloadsApi] instance
  DownloadsApi(this._api);

  final RestApi _api;

  /// Runs the an [_api] request and returns the bytes of the body response
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<Uint8List> download(Uri uri) async {
    final response = await _api.get(uri);
    return response.bodyBytes;
  }
}
