import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kapoow/src/features/rest_api/ui/widgets/cached_api_image.dart';

/// Decorated box that loads an image from api
class CachedApiImageDecoratedBox extends StatelessWidget {
  /// Creates a [CachedApiImageDecoratedBox] instance
  const CachedApiImageDecoratedBox({
    required this.relativeUrl,
    super.key,
    this.imageBuilder,
    this.placeholder,
    this.errorWidget,
    this.child,
    this.borderRadius,
    this.fit,
    this.alignment,
  });

  /// Optional builder to further customize the display of the image.
  final ImageWidgetBuilder? imageBuilder;

  /// Widget displayed while the target [relativeUrl] is loading.
  final PlaceholderWidgetBuilder? placeholder;

  /// Widget displayed while the target [relativeUrl] failed loading.
  final LoadingErrorWidgetBuilder? errorWidget;

  /// Border rounded radius for the image
  final BorderRadiusGeometry? borderRadius;

  /// Box fit for the image
  final BoxFit? fit;

  /// Widget to draw over the image
  final Widget? child;

  /// Relative url of image to the api
  final String relativeUrl;

  /// Alignment of the image
  final AlignmentGeometry? alignment;

  @override
  Widget build(BuildContext context) {
    return CachedApiImage(
      imageRelativeUrl: relativeUrl,
      placeholder: placeholder,
      errorWidget: errorWidget,
      imageBuilder: (_, provider) => DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          image: DecorationImage(
            fit: fit ?? BoxFit.cover,
            alignment: alignment ?? Alignment.topCenter,
            image: provider,
          ),
        ),
        child: child,
      ),
    );
  }
}
