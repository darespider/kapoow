// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'path_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

PathDto _$PathDtoFromJson(Map<String, dynamic> json) {
  return _PathDto.fromJson(json);
}

/// @nodoc
mixin _$PathDto {
  String get type => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get path => throw _privateConstructorUsedError;

  /// Serializes this PathDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$PathDtoImpl implements _PathDto {
  const _$PathDtoImpl(
      {required this.type, required this.name, required this.path});

  factory _$PathDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$PathDtoImplFromJson(json);

  @override
  final String type;
  @override
  final String name;
  @override
  final String path;

  @override
  String toString() {
    return 'PathDto(type: $type, name: $name, path: $path)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PathDtoImpl &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.path, path) || other.path == path));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, type, name, path);

  @override
  Map<String, dynamic> toJson() {
    return _$$PathDtoImplToJson(
      this,
    );
  }
}

abstract class _PathDto implements PathDto {
  const factory _PathDto(
      {required final String type,
      required final String name,
      required final String path}) = _$PathDtoImpl;

  factory _PathDto.fromJson(Map<String, dynamic> json) = _$PathDtoImpl.fromJson;

  @override
  String get type;
  @override
  String get name;
  @override
  String get path;
}
