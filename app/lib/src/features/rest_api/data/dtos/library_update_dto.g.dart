// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$LibraryUpdateDtoImpl _$$LibraryUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$LibraryUpdateDtoImpl(
      name: json['name'] as String,
      root: json['root'] as String,
      importComicInfoBook: json['importComicInfoBook'] as bool,
      importComicInfoCollection: json['importComicInfoCollection'] as bool,
      importComicInfoSeries: json['importComicInfoSeries'] as bool,
      importComicInfoReadList: json['importComicInfoReadList'] as bool,
      importEpubBook: json['importEpubBook'] as bool,
      importEpubSeries: json['importEpubSeries'] as bool,
      importLocalArtwork: json['importLocalArtwork'] as bool,
      scanForceModifiedTime: json['scanForceModifiedTime'] as bool,
      scanDeep: json['scanDeep'] as bool,
      repairExtensions: json['repairExtensions'] as bool,
      convertToCbz: json['convertToCbz'] as bool,
    );

Map<String, dynamic> _$$LibraryUpdateDtoImplToJson(
        _$LibraryUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'root': instance.root,
      'importComicInfoBook': instance.importComicInfoBook,
      'importComicInfoCollection': instance.importComicInfoCollection,
      'importComicInfoSeries': instance.importComicInfoSeries,
      'importComicInfoReadList': instance.importComicInfoReadList,
      'importEpubBook': instance.importEpubBook,
      'importEpubSeries': instance.importEpubSeries,
      'importLocalArtwork': instance.importLocalArtwork,
      'scanForceModifiedTime': instance.scanForceModifiedTime,
      'scanDeep': instance.scanDeep,
      'repairExtensions': instance.repairExtensions,
      'convertToCbz': instance.convertToCbz,
    };
