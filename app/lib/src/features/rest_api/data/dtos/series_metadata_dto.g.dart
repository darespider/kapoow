// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'series_metadata_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SeriesMetadataDtoImpl _$$SeriesMetadataDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$SeriesMetadataDtoImpl(
      status: json['status'] as String,
      statusLock: json['statusLock'] as bool,
      title: json['title'] as String,
      titleLock: json['titleLock'] as bool,
      titleSort: json['titleSort'] as String,
      titleSortLock: json['titleSortLock'] as bool,
      summary: json['summary'] as String,
      summaryLock: json['summaryLock'] as bool,
      readingDirection: json['readingDirection'] as String,
      readingDirectionLock: json['readingDirectionLock'] as bool,
      publisher: json['publisher'] as String,
      publisherLock: json['publisherLock'] as bool,
      ageRatingLock: json['ageRatingLock'] as bool,
      language: json['language'] as String,
      languageLock: json['languageLock'] as bool,
      genres: (json['genres'] as List<dynamic>).map((e) => e as String).toSet(),
      genresLock: json['genresLock'] as bool,
      tags: (json['tags'] as List<dynamic>).map((e) => e as String).toSet(),
      tagsLock: json['tagsLock'] as bool,
      created: DateTime.parse(json['created'] as String),
      lastModified: DateTime.parse(json['lastModified'] as String),
      totalBookCountLock: json['totalBookCountLock'] as bool,
      sharingLabels: (json['sharingLabels'] as List<dynamic>)
          .map((e) => e as String)
          .toSet(),
      sharingLabelsLock: json['sharingLabelsLock'] as bool,
      alternateTitlesLock: json['alternateTitlesLock'] as bool? ?? false,
      linksLock: json['linksLock'] as bool? ?? false,
      alternateTitles: (json['alternateTitles'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toSet() ??
          const {},
      links:
          (json['links'] as List<dynamic>?)?.map((e) => e as String).toSet() ??
              const {},
      totalBookCount: (json['totalBookCount'] as num?)?.toInt(),
      ageRating: (json['ageRating'] as num?)?.toInt(),
    );

Map<String, dynamic> _$$SeriesMetadataDtoImplToJson(
        _$SeriesMetadataDtoImpl instance) =>
    <String, dynamic>{
      'status': instance.status,
      'statusLock': instance.statusLock,
      'title': instance.title,
      'titleLock': instance.titleLock,
      'titleSort': instance.titleSort,
      'titleSortLock': instance.titleSortLock,
      'summary': instance.summary,
      'summaryLock': instance.summaryLock,
      'readingDirection': instance.readingDirection,
      'readingDirectionLock': instance.readingDirectionLock,
      'publisher': instance.publisher,
      'publisherLock': instance.publisherLock,
      'ageRatingLock': instance.ageRatingLock,
      'language': instance.language,
      'languageLock': instance.languageLock,
      'genres': instance.genres.toList(),
      'genresLock': instance.genresLock,
      'tags': instance.tags.toList(),
      'tagsLock': instance.tagsLock,
      'created': instance.created.toIso8601String(),
      'lastModified': instance.lastModified.toIso8601String(),
      'totalBookCountLock': instance.totalBookCountLock,
      'sharingLabels': instance.sharingLabels.toList(),
      'sharingLabelsLock': instance.sharingLabelsLock,
      'alternateTitlesLock': instance.alternateTitlesLock,
      'linksLock': instance.linksLock,
      'alternateTitles': instance.alternateTitles.toList(),
      'links': instance.links.toList(),
      'totalBookCount': instance.totalBookCount,
      'ageRating': instance.ageRating,
    };
