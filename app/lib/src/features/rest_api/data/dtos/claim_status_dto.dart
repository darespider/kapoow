// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'claim_status_dto.freezed.dart';
part 'claim_status_dto.g.dart';

@Freezed(copyWith: false)
class ClaimStatusDto with _$ClaimStatusDto {
  const factory ClaimStatusDto({
    required bool isClaimed,
  }) = _ClaimStatusDto;

  factory ClaimStatusDto.fromJson(Map<String, dynamic> json) =>
      _$ClaimStatusDtoFromJson(json);
}
