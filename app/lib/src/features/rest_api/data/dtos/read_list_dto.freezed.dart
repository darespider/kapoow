// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_list_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ReadListDto _$ReadListDtoFromJson(Map<String, dynamic> json) {
  return _ReadListDto.fromJson(json);
}

/// @nodoc
mixin _$ReadListDto {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  bool get ordered => throw _privateConstructorUsedError;
  List<String> get bookIds => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;
  DateTime get lastModifiedDate => throw _privateConstructorUsedError;
  bool get filtered => throw _privateConstructorUsedError;

  /// Serializes this ReadListDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$ReadListDtoImpl implements _ReadListDto {
  const _$ReadListDtoImpl(
      {required this.id,
      required this.name,
      required this.summary,
      required this.ordered,
      required final List<String> bookIds,
      required this.createdDate,
      required this.lastModifiedDate,
      required this.filtered})
      : _bookIds = bookIds;

  factory _$ReadListDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadListDtoImplFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final String summary;
  @override
  final bool ordered;
  final List<String> _bookIds;
  @override
  List<String> get bookIds {
    if (_bookIds is EqualUnmodifiableListView) return _bookIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_bookIds);
  }

  @override
  final DateTime createdDate;
  @override
  final DateTime lastModifiedDate;
  @override
  final bool filtered;

  @override
  String toString() {
    return 'ReadListDto(id: $id, name: $name, summary: $summary, ordered: $ordered, bookIds: $bookIds, createdDate: $createdDate, lastModifiedDate: $lastModifiedDate, filtered: $filtered)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadListDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.ordered, ordered) || other.ordered == ordered) &&
            const DeepCollectionEquality().equals(other._bookIds, _bookIds) &&
            (identical(other.createdDate, createdDate) ||
                other.createdDate == createdDate) &&
            (identical(other.lastModifiedDate, lastModifiedDate) ||
                other.lastModifiedDate == lastModifiedDate) &&
            (identical(other.filtered, filtered) ||
                other.filtered == filtered));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      name,
      summary,
      ordered,
      const DeepCollectionEquality().hash(_bookIds),
      createdDate,
      lastModifiedDate,
      filtered);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadListDtoImplToJson(
      this,
    );
  }
}

abstract class _ReadListDto implements ReadListDto {
  const factory _ReadListDto(
      {required final String id,
      required final String name,
      required final String summary,
      required final bool ordered,
      required final List<String> bookIds,
      required final DateTime createdDate,
      required final DateTime lastModifiedDate,
      required final bool filtered}) = _$ReadListDtoImpl;

  factory _ReadListDto.fromJson(Map<String, dynamic> json) =
      _$ReadListDtoImpl.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  String get summary;
  @override
  bool get ordered;
  @override
  List<String> get bookIds;
  @override
  DateTime get createdDate;
  @override
  DateTime get lastModifiedDate;
  @override
  bool get filtered;
}
