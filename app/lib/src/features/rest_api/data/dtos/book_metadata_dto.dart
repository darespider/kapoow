// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/author_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/web_link_dto.dart';

part 'book_metadata_dto.freezed.dart';
part 'book_metadata_dto.g.dart';

@Freezed(copyWith: false)
class BookMetadataDto with _$BookMetadataDto {
  const factory BookMetadataDto({
    required String title,
    required bool titleLock,
    required String summary,
    required bool summaryLock,
    required String number,
    required bool numberLock,
    required double numberSort,
    required bool numberSortLock,
    required bool releaseDateLock,
    required List<AuthorDto> authors,
    required bool authorsLock,
    required Set<String> tags,
    required bool tagsLock,
    required DateTime created,
    required DateTime lastModified,
    required String isbn,
    required bool isbnLock,
    required bool linksLock,
    @Default([]) List<WebLinkDto> links,
    DateTime? releaseDate,
  }) = _BookMetadataDto;

  factory BookMetadataDto.fromJson(Map<String, dynamic> json) =>
      _$BookMetadataDtoFromJson(json);
}
