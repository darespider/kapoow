// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'claim_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ClaimStatus _$$_ClaimStatusFromJson(Map<String, dynamic> json) =>
    _$_ClaimStatus(
      isClaimed: json['isClaimed'] as bool,
    );

Map<String, dynamic> _$$_ClaimStatusToJson(_$_ClaimStatus instance) =>
    <String, dynamic>{
      'isClaimed': instance.isClaimed,
    };
