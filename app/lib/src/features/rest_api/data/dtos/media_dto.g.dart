// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$MediaDtoImpl _$$MediaDtoImplFromJson(Map<String, dynamic> json) =>
    _$MediaDtoImpl(
      status: json['status'] as String,
      mediaType: json['mediaType'] as String,
      pagesCount: (json['pagesCount'] as num).toInt(),
      comment: json['comment'] as String,
    );

Map<String, dynamic> _$$MediaDtoImplToJson(_$MediaDtoImpl instance) =>
    <String, dynamic>{
      'status': instance.status,
      'mediaType': instance.mediaType,
      'pagesCount': instance.pagesCount,
      'comment': instance.comment,
    };
