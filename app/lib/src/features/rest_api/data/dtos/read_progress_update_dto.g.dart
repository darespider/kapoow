// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_progress_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReadProgressUpdateDtoImpl _$$ReadProgressUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ReadProgressUpdateDtoImpl(
      page: (json['page'] as num?)?.toInt(),
      completed: json['completed'] as bool?,
    );

Map<String, dynamic> _$$ReadProgressUpdateDtoImplToJson(
        _$ReadProgressUpdateDtoImpl instance) =>
    <String, dynamic>{
      'page': instance.page,
      'completed': instance.completed,
    };
