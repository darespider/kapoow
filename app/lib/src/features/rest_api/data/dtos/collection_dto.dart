// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'collection_dto.freezed.dart';
part 'collection_dto.g.dart';

@Freezed(copyWith: false)
class CollectionDto with _$CollectionDto {
  const factory CollectionDto({
    required String id,
    required String name,
    required bool ordered,
    required List<String> seriesIds,
    required DateTime createdDate,
    required DateTime lastModifiedDate,
    required bool filtered,
  }) = _CollectionDto;

  factory CollectionDto.fromJson(Map<String, dynamic> json) =>
      _$CollectionDtoFromJson(json);
}
