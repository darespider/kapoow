// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'read_list_update_dto.freezed.dart';
part 'read_list_update_dto.g.dart';

@Freezed(copyWith: false)
class ReadListUpdateDto with _$ReadListUpdateDto {
  const factory ReadListUpdateDto({
    String? name,
    String? summary,
    List<String>? bookIds,
  }) = _ReadListUpdateDto;

  factory ReadListUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$ReadListUpdateDtoFromJson(json);
}
