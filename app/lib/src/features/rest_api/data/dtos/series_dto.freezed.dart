// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'series_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

SeriesDto _$SeriesDtoFromJson(Map<String, dynamic> json) {
  return _SeriesDto.fromJson(json);
}

/// @nodoc
mixin _$SeriesDto {
  String get id => throw _privateConstructorUsedError;
  String get libraryId => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  DateTime get fileLastModified => throw _privateConstructorUsedError;
  int get booksCount => throw _privateConstructorUsedError;
  int get booksReadCount => throw _privateConstructorUsedError;
  int get booksUnreadCount => throw _privateConstructorUsedError;
  int get booksInProgressCount => throw _privateConstructorUsedError;
  SeriesMetadataDto get metadata => throw _privateConstructorUsedError;
  BookMetadataAggregationDto get booksMetadata =>
      throw _privateConstructorUsedError;
  bool get deleted => throw _privateConstructorUsedError;
  bool get oneshot => throw _privateConstructorUsedError;

  /// Serializes this SeriesDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$SeriesDtoImpl implements _SeriesDto {
  const _$SeriesDtoImpl(
      {required this.id,
      required this.libraryId,
      required this.name,
      required this.url,
      required this.created,
      required this.lastModified,
      required this.fileLastModified,
      required this.booksCount,
      required this.booksReadCount,
      required this.booksUnreadCount,
      required this.booksInProgressCount,
      required this.metadata,
      required this.booksMetadata,
      required this.deleted,
      this.oneshot = false});

  factory _$SeriesDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$SeriesDtoImplFromJson(json);

  @override
  final String id;
  @override
  final String libraryId;
  @override
  final String name;
  @override
  final String url;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final DateTime fileLastModified;
  @override
  final int booksCount;
  @override
  final int booksReadCount;
  @override
  final int booksUnreadCount;
  @override
  final int booksInProgressCount;
  @override
  final SeriesMetadataDto metadata;
  @override
  final BookMetadataAggregationDto booksMetadata;
  @override
  final bool deleted;
  @override
  @JsonKey()
  final bool oneshot;

  @override
  String toString() {
    return 'SeriesDto(id: $id, libraryId: $libraryId, name: $name, url: $url, created: $created, lastModified: $lastModified, fileLastModified: $fileLastModified, booksCount: $booksCount, booksReadCount: $booksReadCount, booksUnreadCount: $booksUnreadCount, booksInProgressCount: $booksInProgressCount, metadata: $metadata, booksMetadata: $booksMetadata, deleted: $deleted, oneshot: $oneshot)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SeriesDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.libraryId, libraryId) ||
                other.libraryId == libraryId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.fileLastModified, fileLastModified) ||
                other.fileLastModified == fileLastModified) &&
            (identical(other.booksCount, booksCount) ||
                other.booksCount == booksCount) &&
            (identical(other.booksReadCount, booksReadCount) ||
                other.booksReadCount == booksReadCount) &&
            (identical(other.booksUnreadCount, booksUnreadCount) ||
                other.booksUnreadCount == booksUnreadCount) &&
            (identical(other.booksInProgressCount, booksInProgressCount) ||
                other.booksInProgressCount == booksInProgressCount) &&
            (identical(other.metadata, metadata) ||
                other.metadata == metadata) &&
            (identical(other.booksMetadata, booksMetadata) ||
                other.booksMetadata == booksMetadata) &&
            (identical(other.deleted, deleted) || other.deleted == deleted) &&
            (identical(other.oneshot, oneshot) || other.oneshot == oneshot));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      libraryId,
      name,
      url,
      created,
      lastModified,
      fileLastModified,
      booksCount,
      booksReadCount,
      booksUnreadCount,
      booksInProgressCount,
      metadata,
      booksMetadata,
      deleted,
      oneshot);

  @override
  Map<String, dynamic> toJson() {
    return _$$SeriesDtoImplToJson(
      this,
    );
  }
}

abstract class _SeriesDto implements SeriesDto {
  const factory _SeriesDto(
      {required final String id,
      required final String libraryId,
      required final String name,
      required final String url,
      required final DateTime created,
      required final DateTime lastModified,
      required final DateTime fileLastModified,
      required final int booksCount,
      required final int booksReadCount,
      required final int booksUnreadCount,
      required final int booksInProgressCount,
      required final SeriesMetadataDto metadata,
      required final BookMetadataAggregationDto booksMetadata,
      required final bool deleted,
      final bool oneshot}) = _$SeriesDtoImpl;

  factory _SeriesDto.fromJson(Map<String, dynamic> json) =
      _$SeriesDtoImpl.fromJson;

  @override
  String get id;
  @override
  String get libraryId;
  @override
  String get name;
  @override
  String get url;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  DateTime get fileLastModified;
  @override
  int get booksCount;
  @override
  int get booksReadCount;
  @override
  int get booksUnreadCount;
  @override
  int get booksInProgressCount;
  @override
  SeriesMetadataDto get metadata;
  @override
  BookMetadataAggregationDto get booksMetadata;
  @override
  bool get deleted;
  @override
  bool get oneshot;
}
