// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'collection_creation_dto.freezed.dart';
part 'collection_creation_dto.g.dart';

@Freezed(copyWith: false)
class CollectionCreationDto with _$CollectionCreationDto {
  const factory CollectionCreationDto({
    required String name,
    required bool ordered,
    required List<String> seriesIds,
  }) = _CollectionCreationDto;

  factory CollectionCreationDto.fromJson(Map<String, dynamic> json) =>
      _$CollectionCreationDtoFromJson(json);
}
