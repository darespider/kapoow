// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'library_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

LibraryUpdateDto _$LibraryUpdateDtoFromJson(Map<String, dynamic> json) {
  return _LibraryUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$LibraryUpdateDto {
  String get name => throw _privateConstructorUsedError;
  String get root => throw _privateConstructorUsedError;
  bool get importComicInfoBook => throw _privateConstructorUsedError;
  bool get importComicInfoCollection => throw _privateConstructorUsedError;
  bool get importComicInfoSeries => throw _privateConstructorUsedError;
  bool get importComicInfoReadList => throw _privateConstructorUsedError;
  bool get importEpubBook => throw _privateConstructorUsedError;
  bool get importEpubSeries => throw _privateConstructorUsedError;
  bool get importLocalArtwork => throw _privateConstructorUsedError;
  bool get scanForceModifiedTime => throw _privateConstructorUsedError;
  bool get scanDeep => throw _privateConstructorUsedError;
  bool get repairExtensions => throw _privateConstructorUsedError;
  bool get convertToCbz => throw _privateConstructorUsedError;

  /// Serializes this LibraryUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$LibraryUpdateDtoImpl implements _LibraryUpdateDto {
  const _$LibraryUpdateDtoImpl(
      {required this.name,
      required this.root,
      required this.importComicInfoBook,
      required this.importComicInfoCollection,
      required this.importComicInfoSeries,
      required this.importComicInfoReadList,
      required this.importEpubBook,
      required this.importEpubSeries,
      required this.importLocalArtwork,
      required this.scanForceModifiedTime,
      required this.scanDeep,
      required this.repairExtensions,
      required this.convertToCbz});

  factory _$LibraryUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$LibraryUpdateDtoImplFromJson(json);

  @override
  final String name;
  @override
  final String root;
  @override
  final bool importComicInfoBook;
  @override
  final bool importComicInfoCollection;
  @override
  final bool importComicInfoSeries;
  @override
  final bool importComicInfoReadList;
  @override
  final bool importEpubBook;
  @override
  final bool importEpubSeries;
  @override
  final bool importLocalArtwork;
  @override
  final bool scanForceModifiedTime;
  @override
  final bool scanDeep;
  @override
  final bool repairExtensions;
  @override
  final bool convertToCbz;

  @override
  String toString() {
    return 'LibraryUpdateDto(name: $name, root: $root, importComicInfoBook: $importComicInfoBook, importComicInfoCollection: $importComicInfoCollection, importComicInfoSeries: $importComicInfoSeries, importComicInfoReadList: $importComicInfoReadList, importEpubBook: $importEpubBook, importEpubSeries: $importEpubSeries, importLocalArtwork: $importLocalArtwork, scanForceModifiedTime: $scanForceModifiedTime, scanDeep: $scanDeep, repairExtensions: $repairExtensions, convertToCbz: $convertToCbz)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LibraryUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.root, root) || other.root == root) &&
            (identical(other.importComicInfoBook, importComicInfoBook) ||
                other.importComicInfoBook == importComicInfoBook) &&
            (identical(other.importComicInfoCollection,
                    importComicInfoCollection) ||
                other.importComicInfoCollection == importComicInfoCollection) &&
            (identical(other.importComicInfoSeries, importComicInfoSeries) ||
                other.importComicInfoSeries == importComicInfoSeries) &&
            (identical(
                    other.importComicInfoReadList, importComicInfoReadList) ||
                other.importComicInfoReadList == importComicInfoReadList) &&
            (identical(other.importEpubBook, importEpubBook) ||
                other.importEpubBook == importEpubBook) &&
            (identical(other.importEpubSeries, importEpubSeries) ||
                other.importEpubSeries == importEpubSeries) &&
            (identical(other.importLocalArtwork, importLocalArtwork) ||
                other.importLocalArtwork == importLocalArtwork) &&
            (identical(other.scanForceModifiedTime, scanForceModifiedTime) ||
                other.scanForceModifiedTime == scanForceModifiedTime) &&
            (identical(other.scanDeep, scanDeep) ||
                other.scanDeep == scanDeep) &&
            (identical(other.repairExtensions, repairExtensions) ||
                other.repairExtensions == repairExtensions) &&
            (identical(other.convertToCbz, convertToCbz) ||
                other.convertToCbz == convertToCbz));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      root,
      importComicInfoBook,
      importComicInfoCollection,
      importComicInfoSeries,
      importComicInfoReadList,
      importEpubBook,
      importEpubSeries,
      importLocalArtwork,
      scanForceModifiedTime,
      scanDeep,
      repairExtensions,
      convertToCbz);

  @override
  Map<String, dynamic> toJson() {
    return _$$LibraryUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _LibraryUpdateDto implements LibraryUpdateDto {
  const factory _LibraryUpdateDto(
      {required final String name,
      required final String root,
      required final bool importComicInfoBook,
      required final bool importComicInfoCollection,
      required final bool importComicInfoSeries,
      required final bool importComicInfoReadList,
      required final bool importEpubBook,
      required final bool importEpubSeries,
      required final bool importLocalArtwork,
      required final bool scanForceModifiedTime,
      required final bool scanDeep,
      required final bool repairExtensions,
      required final bool convertToCbz}) = _$LibraryUpdateDtoImpl;

  factory _LibraryUpdateDto.fromJson(Map<String, dynamic> json) =
      _$LibraryUpdateDtoImpl.fromJson;

  @override
  String get name;
  @override
  String get root;
  @override
  bool get importComicInfoBook;
  @override
  bool get importComicInfoCollection;
  @override
  bool get importComicInfoSeries;
  @override
  bool get importComicInfoReadList;
  @override
  bool get importEpubBook;
  @override
  bool get importEpubSeries;
  @override
  bool get importLocalArtwork;
  @override
  bool get scanForceModifiedTime;
  @override
  bool get scanDeep;
  @override
  bool get repairExtensions;
  @override
  bool get convertToCbz;
}
