// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'library_creation_dto.freezed.dart';
part 'library_creation_dto.g.dart';

@Freezed(copyWith: false)
class LibraryCreationDto with _$LibraryCreationDto {
  const factory LibraryCreationDto({
    required String name,
    required String root,
    required bool importComicInfoBook,
    required bool importComicInfoCollection,
    required bool importComicInfoSeries,
    required bool importComicInfoReadList,
    required bool importEpubBook,
    required bool importEpubSeries,
    required bool importLocalArtwork,
    required bool scanForceModifiedTime,
    required bool scanDeep,
  }) = _LibraryCreationDto;

  factory LibraryCreationDto.fromJson(Map<String, dynamic> json) =>
      _$LibraryCreationDtoFromJson(json);
}
