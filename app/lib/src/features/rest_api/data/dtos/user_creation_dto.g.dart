// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_creation_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UserCreationDtoImpl _$$UserCreationDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$UserCreationDtoImpl(
      email: json['email'] as String,
      password: json['password'] as String,
      roles: (json['roles'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$UserCreationDtoImplToJson(
        _$UserCreationDtoImpl instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'roles': instance.roles,
    };
