// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'collection_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

CollectionUpdateDto _$CollectionUpdateDtoFromJson(Map<String, dynamic> json) {
  return _CollectionUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$CollectionUpdateDto {
  String get name => throw _privateConstructorUsedError;
  bool get ordered => throw _privateConstructorUsedError;
  List<String> get seriesIds => throw _privateConstructorUsedError;

  /// Serializes this CollectionUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$CollectionUpdateDtoImpl implements _CollectionUpdateDto {
  const _$CollectionUpdateDtoImpl(
      {required this.name,
      required this.ordered,
      required final List<String> seriesIds})
      : _seriesIds = seriesIds;

  factory _$CollectionUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$CollectionUpdateDtoImplFromJson(json);

  @override
  final String name;
  @override
  final bool ordered;
  final List<String> _seriesIds;
  @override
  List<String> get seriesIds {
    if (_seriesIds is EqualUnmodifiableListView) return _seriesIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_seriesIds);
  }

  @override
  String toString() {
    return 'CollectionUpdateDto(name: $name, ordered: $ordered, seriesIds: $seriesIds)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CollectionUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.ordered, ordered) || other.ordered == ordered) &&
            const DeepCollectionEquality()
                .equals(other._seriesIds, _seriesIds));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, name, ordered,
      const DeepCollectionEquality().hash(_seriesIds));

  @override
  Map<String, dynamic> toJson() {
    return _$$CollectionUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _CollectionUpdateDto implements CollectionUpdateDto {
  const factory _CollectionUpdateDto(
      {required final String name,
      required final bool ordered,
      required final List<String> seriesIds}) = _$CollectionUpdateDtoImpl;

  factory _CollectionUpdateDto.fromJson(Map<String, dynamic> json) =
      _$CollectionUpdateDtoImpl.fromJson;

  @override
  String get name;
  @override
  bool get ordered;
  @override
  List<String> get seriesIds;
}
