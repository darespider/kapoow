// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/author_update_dto.dart';

part 'book_metadata_update_dto.freezed.dart';
part 'book_metadata_update_dto.g.dart';

@Freezed(copyWith: false)
class BookMetadataUpdateDto with _$BookMetadataUpdateDto {
  const factory BookMetadataUpdateDto({
    String? title,
    bool? titleLock,
    String? summary,
    bool? summaryLock,
    String? number,
    bool? numberLock,
    double? numberSort,
    bool? numberSortLock,
    bool? releaseDateLock,
    bool? authorsLock,
    bool? tagsLock,
    Set<String>? tags,
    AuthorUpdateDto? authors,
    DateTime? releaseDate,
  }) = _BookMetadataUpdateDto;

  factory BookMetadataUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$BookMetadataUpdateDtoFromJson(json);
}
