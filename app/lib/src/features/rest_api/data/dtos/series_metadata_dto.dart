// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'series_metadata_dto.freezed.dart';
part 'series_metadata_dto.g.dart';

@Freezed(copyWith: false)
class SeriesMetadataDto with _$SeriesMetadataDto {
  const factory SeriesMetadataDto({
    required String status,
    required bool statusLock,
    required String title,
    required bool titleLock,
    required String titleSort,
    required bool titleSortLock,
    required String summary,
    required bool summaryLock,
    required String readingDirection,
    required bool readingDirectionLock,
    required String publisher,
    required bool publisherLock,
    required bool ageRatingLock,
    required String language,
    required bool languageLock,
    required Set<String> genres,
    required bool genresLock,
    required Set<String> tags,
    required bool tagsLock,
    required DateTime created,
    required DateTime lastModified,
    required bool totalBookCountLock,
    required Set<String> sharingLabels,
    required bool sharingLabelsLock,
    @Default(false) bool alternateTitlesLock,
    @Default(false) bool linksLock,
    @Default({}) Set<String> alternateTitles,
    @Default({}) Set<String> links,
    int? totalBookCount,
    int? ageRating,
  }) = _SeriesMetadataDto;

  factory SeriesMetadataDto.fromJson(Map<String, dynamic> json) =>
      _$SeriesMetadataDtoFromJson(json);
}
