// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'author_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AuthorUpdateDtoImpl _$$AuthorUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$AuthorUpdateDtoImpl(
      name: json['name'] as String,
      role: json['role'] as String,
    );

Map<String, dynamic> _$$AuthorUpdateDtoImplToJson(
        _$AuthorUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'role': instance.role,
    };
