// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'collection_update_dto.freezed.dart';
part 'collection_update_dto.g.dart';

@Freezed(copyWith: false)
class CollectionUpdateDto with _$CollectionUpdateDto {
  const factory CollectionUpdateDto({
    required String name,
    required bool ordered,
    required List<String> seriesIds,
  }) = _CollectionUpdateDto;

  factory CollectionUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$CollectionUpdateDtoFromJson(json);
}
