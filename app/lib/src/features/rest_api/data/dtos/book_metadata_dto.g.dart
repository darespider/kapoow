// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_metadata_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$BookMetadataDtoImpl _$$BookMetadataDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$BookMetadataDtoImpl(
      title: json['title'] as String,
      titleLock: json['titleLock'] as bool,
      summary: json['summary'] as String,
      summaryLock: json['summaryLock'] as bool,
      number: json['number'] as String,
      numberLock: json['numberLock'] as bool,
      numberSort: (json['numberSort'] as num).toDouble(),
      numberSortLock: json['numberSortLock'] as bool,
      releaseDateLock: json['releaseDateLock'] as bool,
      authors: (json['authors'] as List<dynamic>)
          .map((e) => AuthorDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      authorsLock: json['authorsLock'] as bool,
      tags: (json['tags'] as List<dynamic>).map((e) => e as String).toSet(),
      tagsLock: json['tagsLock'] as bool,
      created: DateTime.parse(json['created'] as String),
      lastModified: DateTime.parse(json['lastModified'] as String),
      isbn: json['isbn'] as String,
      isbnLock: json['isbnLock'] as bool,
      linksLock: json['linksLock'] as bool,
      links: (json['links'] as List<dynamic>?)
              ?.map((e) => WebLinkDto.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      releaseDate: json['releaseDate'] == null
          ? null
          : DateTime.parse(json['releaseDate'] as String),
    );

Map<String, dynamic> _$$BookMetadataDtoImplToJson(
        _$BookMetadataDtoImpl instance) =>
    <String, dynamic>{
      'title': instance.title,
      'titleLock': instance.titleLock,
      'summary': instance.summary,
      'summaryLock': instance.summaryLock,
      'number': instance.number,
      'numberLock': instance.numberLock,
      'numberSort': instance.numberSort,
      'numberSortLock': instance.numberSortLock,
      'releaseDateLock': instance.releaseDateLock,
      'authors': instance.authors,
      'authorsLock': instance.authorsLock,
      'tags': instance.tags.toList(),
      'tagsLock': instance.tagsLock,
      'created': instance.created.toIso8601String(),
      'lastModified': instance.lastModified.toIso8601String(),
      'isbn': instance.isbn,
      'isbnLock': instance.isbnLock,
      'linksLock': instance.linksLock,
      'links': instance.links,
      'releaseDate': instance.releaseDate?.toIso8601String(),
    };
