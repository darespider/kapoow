// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'web_link_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

WebLinkDto _$WebLinkDtoFromJson(Map<String, dynamic> json) {
  return _WebLinkDto.fromJson(json);
}

/// @nodoc
mixin _$WebLinkDto {
  String get label => throw _privateConstructorUsedError;
  Uri get url => throw _privateConstructorUsedError;

  /// Serializes this WebLinkDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$WebLinkDtoImpl implements _WebLinkDto {
  const _$WebLinkDtoImpl({required this.label, required this.url});

  factory _$WebLinkDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$WebLinkDtoImplFromJson(json);

  @override
  final String label;
  @override
  final Uri url;

  @override
  String toString() {
    return 'WebLinkDto(label: $label, url: $url)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WebLinkDtoImpl &&
            (identical(other.label, label) || other.label == label) &&
            (identical(other.url, url) || other.url == url));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, label, url);

  @override
  Map<String, dynamic> toJson() {
    return _$$WebLinkDtoImplToJson(
      this,
    );
  }
}

abstract class _WebLinkDto implements WebLinkDto {
  const factory _WebLinkDto(
      {required final String label, required final Uri url}) = _$WebLinkDtoImpl;

  factory _WebLinkDto.fromJson(Map<String, dynamic> json) =
      _$WebLinkDtoImpl.fromJson;

  @override
  String get label;
  @override
  Uri get url;
}
