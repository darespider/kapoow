// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'library_update_dto.freezed.dart';
part 'library_update_dto.g.dart';

@Freezed(copyWith: false)
class LibraryUpdateDto with _$LibraryUpdateDto {
  const factory LibraryUpdateDto({
    required String name,
    required String root,
    required bool importComicInfoBook,
    required bool importComicInfoCollection,
    required bool importComicInfoSeries,
    required bool importComicInfoReadList,
    required bool importEpubBook,
    required bool importEpubSeries,
    required bool importLocalArtwork,
    required bool scanForceModifiedTime,
    required bool scanDeep,
    required bool repairExtensions,
    required bool convertToCbz,
  }) = _LibraryUpdateDto;

  factory LibraryUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$LibraryUpdateDtoFromJson(json);
}
