// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_metadata_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/media_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_progress_dto.dart';

part 'book_dto.freezed.dart';
part 'book_dto.g.dart';

@Freezed(copyWith: false)
class BookDto with _$BookDto {
  const factory BookDto({
    required String id,
    required String seriesId,
    required String seriesTitle,
    required String libraryId,
    required String name,
    required String url,
    required bool deleted,
    required int number,
    required DateTime created,
    required DateTime lastModified,
    required DateTime fileLastModified,
    required int sizeBytes,
    required String size,
    required MediaDto media,
    required BookMetadataDto metadata,
    required String fileHash,
    required bool oneshot,
    ReadProgressDto? readProgress,
  }) = _BookDto;

  factory BookDto.fromJson(Map<String, dynamic> json) =>
      _$BookDtoFromJson(json);
}
