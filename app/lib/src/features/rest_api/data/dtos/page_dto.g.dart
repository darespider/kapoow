// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'page_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PageDtoImpl _$$PageDtoImplFromJson(Map<String, dynamic> json) =>
    _$PageDtoImpl(
      number: (json['number'] as num).toInt(),
      fileName: json['fileName'] as String,
      mediaType: json['mediaType'] as String,
      width: (json['width'] as num?)?.toInt(),
      height: (json['height'] as num?)?.toInt(),
    );

Map<String, dynamic> _$$PageDtoImplToJson(_$PageDtoImpl instance) =>
    <String, dynamic>{
      'number': instance.number,
      'fileName': instance.fileName,
      'mediaType': instance.mediaType,
      'width': instance.width,
      'height': instance.height,
    };
