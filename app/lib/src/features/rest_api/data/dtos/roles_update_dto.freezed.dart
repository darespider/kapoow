// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'roles_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

RolesUpdateDto _$RolesUpdateDtoFromJson(Map<String, dynamic> json) {
  return _RolesUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$RolesUpdateDto {
  List<String> get roles => throw _privateConstructorUsedError;

  /// Serializes this RolesUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$RolesUpdateDtoImpl implements _RolesUpdateDto {
  const _$RolesUpdateDtoImpl({required final List<String> roles})
      : _roles = roles;

  factory _$RolesUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$RolesUpdateDtoImplFromJson(json);

  final List<String> _roles;
  @override
  List<String> get roles {
    if (_roles is EqualUnmodifiableListView) return _roles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_roles);
  }

  @override
  String toString() {
    return 'RolesUpdateDto(roles: $roles)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RolesUpdateDtoImpl &&
            const DeepCollectionEquality().equals(other._roles, _roles));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_roles));

  @override
  Map<String, dynamic> toJson() {
    return _$$RolesUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _RolesUpdateDto implements RolesUpdateDto {
  const factory _RolesUpdateDto({required final List<String> roles}) =
      _$RolesUpdateDtoImpl;

  factory _RolesUpdateDto.fromJson(Map<String, dynamic> json) =
      _$RolesUpdateDtoImpl.fromJson;

  @override
  List<String> get roles;
}
