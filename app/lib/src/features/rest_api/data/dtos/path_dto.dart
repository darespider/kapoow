// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'path_dto.freezed.dart';
part 'path_dto.g.dart';

@Freezed(copyWith: false)
class PathDto with _$PathDto {
  const factory PathDto({
    required String type,
    required String name,
    required String path,
  }) = _PathDto;

  factory PathDto.fromJson(Map<String, dynamic> json) =>
      _$PathDtoFromJson(json);
}
