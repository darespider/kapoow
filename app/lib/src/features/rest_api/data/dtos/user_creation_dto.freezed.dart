// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_creation_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

UserCreationDto _$UserCreationDtoFromJson(Map<String, dynamic> json) {
  return _UserCreationDto.fromJson(json);
}

/// @nodoc
mixin _$UserCreationDto {
  String get email => throw _privateConstructorUsedError;
  String get password => throw _privateConstructorUsedError;
  List<String> get roles => throw _privateConstructorUsedError;

  /// Serializes this UserCreationDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$UserCreationDtoImpl implements _UserCreationDto {
  const _$UserCreationDtoImpl(
      {required this.email,
      required this.password,
      required final List<String> roles})
      : _roles = roles;

  factory _$UserCreationDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$UserCreationDtoImplFromJson(json);

  @override
  final String email;
  @override
  final String password;
  final List<String> _roles;
  @override
  List<String> get roles {
    if (_roles is EqualUnmodifiableListView) return _roles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_roles);
  }

  @override
  String toString() {
    return 'UserCreationDto(email: $email, password: $password, roles: $roles)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserCreationDtoImpl &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            const DeepCollectionEquality().equals(other._roles, _roles));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, email, password,
      const DeepCollectionEquality().hash(_roles));

  @override
  Map<String, dynamic> toJson() {
    return _$$UserCreationDtoImplToJson(
      this,
    );
  }
}

abstract class _UserCreationDto implements UserCreationDto {
  const factory _UserCreationDto(
      {required final String email,
      required final String password,
      required final List<String> roles}) = _$UserCreationDtoImpl;

  factory _UserCreationDto.fromJson(Map<String, dynamic> json) =
      _$UserCreationDtoImpl.fromJson;

  @override
  String get email;
  @override
  String get password;
  @override
  List<String> get roles;
}
