// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'directory_listing_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

DirectoryListingDto _$DirectoryListingDtoFromJson(Map<String, dynamic> json) {
  return _DirectoryListingDto.fromJson(json);
}

/// @nodoc
mixin _$DirectoryListingDto {
  String get parent => throw _privateConstructorUsedError;
  List<PathDto> get directories => throw _privateConstructorUsedError;

  /// Serializes this DirectoryListingDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$DirectoryListingDtoImpl implements _DirectoryListingDto {
  const _$DirectoryListingDtoImpl(
      {required this.parent, required final List<PathDto> directories})
      : _directories = directories;

  factory _$DirectoryListingDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$DirectoryListingDtoImplFromJson(json);

  @override
  final String parent;
  final List<PathDto> _directories;
  @override
  List<PathDto> get directories {
    if (_directories is EqualUnmodifiableListView) return _directories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_directories);
  }

  @override
  String toString() {
    return 'DirectoryListingDto(parent: $parent, directories: $directories)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DirectoryListingDtoImpl &&
            (identical(other.parent, parent) || other.parent == parent) &&
            const DeepCollectionEquality()
                .equals(other._directories, _directories));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType, parent, const DeepCollectionEquality().hash(_directories));

  @override
  Map<String, dynamic> toJson() {
    return _$$DirectoryListingDtoImplToJson(
      this,
    );
  }
}

abstract class _DirectoryListingDto implements DirectoryListingDto {
  const factory _DirectoryListingDto(
      {required final String parent,
      required final List<PathDto> directories}) = _$DirectoryListingDtoImpl;

  factory _DirectoryListingDto.fromJson(Map<String, dynamic> json) =
      _$DirectoryListingDtoImpl.fromJson;

  @override
  String get parent;
  @override
  List<PathDto> get directories;
}
