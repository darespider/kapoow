// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_list_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReadListDtoImpl _$$ReadListDtoImplFromJson(Map<String, dynamic> json) =>
    _$ReadListDtoImpl(
      id: json['id'] as String,
      name: json['name'] as String,
      summary: json['summary'] as String,
      ordered: json['ordered'] as bool,
      bookIds:
          (json['bookIds'] as List<dynamic>).map((e) => e as String).toList(),
      createdDate: DateTime.parse(json['createdDate'] as String),
      lastModifiedDate: DateTime.parse(json['lastModifiedDate'] as String),
      filtered: json['filtered'] as bool,
    );

Map<String, dynamic> _$$ReadListDtoImplToJson(_$ReadListDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'summary': instance.summary,
      'ordered': instance.ordered,
      'bookIds': instance.bookIds,
      'createdDate': instance.createdDate.toIso8601String(),
      'lastModifiedDate': instance.lastModifiedDate.toIso8601String(),
      'filtered': instance.filtered,
    };
