import 'dart:typed_data';

import 'package:kapoow/src/features/rest_api/data/datasources/cached_images/cached_images_datasource.dart';

/// Cache Api Datasource that returns mock data
class CachedImagesDatasourceMock implements CachedImagesDatasource {
  @override
  Future<Uint8List?> getCachePageByBookId(String id, {required int page}) {
    // TODO(rurickdev): implement getCachePageByBookId
    throw UnimplementedError();
  }

  @override
  Future<void> putCachePageByBookId(
    String id, {
    required int page,
    required Uint8List bytes,
  }) {
    // TODO(rurickdev): implement putCachePageByBookId
    throw UnimplementedError();
  }
}
