// coverage:ignore-file
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/collections/collections_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';

/// Collections api client that return mock data
class CollectionsDatasourceMock implements CollectionsDatasource {
  @override
  Future<CollectionDto> getCollectionById(String id) {
    // TODO(rurickdev): implement fetchCollectionById
    throw UnimplementedError();
  }

  @override
  Future<PageSeriesDto> getCollectionSeries({
    required String collectionId,
    List<String>? libraryIds,
    List<SeriesStatus>? status,
    ReadStatus? readStatus,
    List<String>? publisher,
    List<String>? language,
    List<String>? genre,
    List<String>? tags,
    List<String>? ageRating,
    List<String>? releaseYears,
    bool? deleted,
    bool? complete,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  }) {
    // TODO(rurickdev): implement fetchCollectionSeries
    throw UnimplementedError();
  }

  @override
  Future<PageCollectionDto> getCollections({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  }) {
    // TODO(rurickdev): implement fetchCollections
    throw UnimplementedError();
  }
}
