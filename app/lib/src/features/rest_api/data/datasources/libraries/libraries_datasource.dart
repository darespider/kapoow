import 'package:kapoow/src/features/rest_api/data/dtos/library_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/json_map.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';

/// Class used to do requests to the collections endpoints
/// it consumes an [RestJsonApi] to do the requests
class LibrariesDatasource {
  /// Creates instance of [LibrariesDatasource]
  const LibrariesDatasource(this._api);

  final RestJsonApi _api;

  /// Runs a `get` request to the `/api/v1/libraries` endpoint
  Future<List<LibraryDto>> getLibraries() async {
    final uri = 'api' / 'v1' / 'libraries';
    return _api.get(uri, dtoParser: LibraryDto.fromJson.list);
  }

  /// Runs a `get` request to the `/api/v1/libraries/{{id}}` endpoint
  Future<LibraryDto> getLibraryById(String id) async {
    final uri = 'api' / 'v1' / 'libraries' / id;
    return _api.get(uri, dtoParser: LibraryDto.fromJson);
  }
}
