// coverage:ignore-file
import 'package:kapoow/src/features/rest_api/data/datasources/libraries/libraries_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/library_dto.dart';

/// Datasource that return mock data
class LibrariesDatasourceMock implements LibrariesDatasource {
  /// Runs a `get` request to the `/api/v1/libraries` endpoint
  @override
  Future<List<LibraryDto>> getLibraries() {
    // TODO(rurickdev): implement getLibraries
    throw UnimplementedError();
  }

  /// Runs a `get` request to the `/api/v1/libraries/{{id}}` endpoint
  @override
  Future<LibraryDto> getLibraryById(String id) async {
    // TODO(rurickdev): implement getLibraryById
    throw UnimplementedError();
  }
}
