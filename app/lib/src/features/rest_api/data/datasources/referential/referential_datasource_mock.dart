// coverage:ignore-file
import 'package:kapoow/src/features/rest_api/data/datasources/referential/referential_datasource.dart';

/// Referential Api Client that returns mock data
class ReferentialDatasourceMock implements ReferentialDatasource {
  @override
  Future<List<String>> getAgeRatings({
    String? collectionId,
    String? libraryId,
  }) {
    // TODO(rurickdev): implement fetchAgeRatings
    throw UnimplementedError();
  }

  @override
  Future<List<String>> getGenres({String? collectionId, String? libraryId}) {
    // TODO(rurickdev): implement fetchGenres
    throw UnimplementedError();
  }

  @override
  Future<List<String>> getLanguages({
    String? collectionId,
    String? libraryId,
  }) {
    // TODO(rurickdev): implement fetchLanguages
    throw UnimplementedError();
  }

  @override
  Future<List<String>> getPublishers({
    String? collectionId,
    String? libraryId,
  }) {
    // TODO(rurickdev): implement fetchPublishers
    throw UnimplementedError();
  }
}
