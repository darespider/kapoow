// coverage:ignore-file
import 'dart:typed_data';

import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/books/books_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/page_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';

/// Books Api Client that returns mock data
class BooksDatasourceMock implements BooksDatasource {
  @override
  Future<void> postAnalyzeBookById(String id) async {}

  @override
  Future<void> deleteBookById(String id) async {}

  @override
  Future<void> postRefreshBookById(String id) async {}

  @override
  Future<BookDto> getBookById(String id) {
    // TODO(rurickdev): implement fetchBookById
    throw UnimplementedError();
  }

  @override
  Future<Uint8List> getBookFileById(String id) {
    // TODO(rurickdev): implement fetchBookById
    throw UnimplementedError();
  }

  @override
  Future<List<PageDto>> getPagesByBookId(String id) {
    // TODO(rurickdev): implement fetchBookPages
    throw UnimplementedError();
  }

  @override
  Future<Uint8List> getPageByBookId(String id, {required int page}) {
    // TODO(rurickdev): implement getBookPageById
    throw UnimplementedError();
  }

  @override
  Future<Uint8List> getPageThumbnailByBookId(String id, {required int page}) {
    // TODO(rurickdev): implement getBookPageById
    throw UnimplementedError();
  }

  @override
  Future<PageBookDto> getBooks({
    String? search,
    List<String>? libraryIds,
    List<MediaStatus>? mediaStatus,
    List<ReadStatus>? readStatus,
    DateTime? releasedAfter,
    List<String>? tags,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? sort,
  }) {
    // TODO(rurickdev): implement fetchBooks
    throw UnimplementedError();
  }

  @override
  Future<PageBookDto> getBooksOnDeck({
    String? libraryId,
    int? page,
    int? pageSize,
  }) {
    // TODO(rurickdev): implement fetchBooksOnDeck
    throw UnimplementedError();
  }

  @override
  Future<BookDto?> getNextBookById(String id) {
    // TODO(rurickdev): implement fetchNextBook
    throw UnimplementedError();
  }

  @override
  Future<BookDto?> getPreviousBookById(String id) {
    // TODO(rurickdev): implement fetchPreviousBook
    throw UnimplementedError();
  }

  @override
  Future<void> patchBookProgressById(
    String id, {
    int? page,
    bool? completed,
  }) async {}
}
