import 'dart:typed_data';

import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/page_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/api_exceptions.dart';
import 'package:kapoow/src/features/rest_api/domain/json_map.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/downloads_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';
import 'package:kapoow/src/features/rest_api/utils/formatters.dart';

/// Class used to do requests to the books endpoints
/// it consumes an [RestJsonApi] to do the requests
class BooksDatasource {
  /// Creates instance of [BooksDatasource]
  const BooksDatasource(this._api, this._downloadsApi);

  final RestJsonApi _api;
  final DownloadsApi _downloadsApi;

  /// Runs a `delete` request to the `/api/v1/books/{{id}}/file`
  /// endpoint
  Future<void> deleteBookById(String id) async {
    final uri = 'api' / 'v1' / 'books' / id / 'file';
    await _api.delete<void, void>(uri);
  }

  /// Runs a `get` request to the `/api/v1/books/{{id}}` endpoint
  Future<BookDto> getBookById(String id) async {
    return _api.get(
      'api' / 'v1' / 'books' / id,
      dtoParser: BookDto.fromJson,
    );
  }

  /// Runs a `get` request to the `/api/v1/books/{{id}}/file` endpoint
  Future<Uint8List> getBookFileById(String id) async {
    return _downloadsApi.download(
      'api' / 'v1' / 'books' / id / 'file',
    );
  }

  /// Runs a `get` request to the `/api/v1/books/{{id}}/pages` endpoint
  Future<List<PageDto>> getPagesByBookId(String id) async {
    return _api.get(
      'api' / 'v1' / 'books' / id / 'pages',
      dtoParser: PageDto.fromJson.list,
    );
  }

  /// Runs a `get` request to the `/api/v1/books/{{id}}/pages` endpoint
  Future<Uint8List> getPageByBookId(String id, {required int page}) async {
    return _downloadsApi.download(
      'api' / 'v1' / 'books' / id / 'pages' / page.toString(),
    );
  }

  /// Runs a `get` request to the `/api/v1/books/{{id}}/pages` endpoint
  Future<Uint8List> getPageThumbnailByBookId(
    String id, {
    required int page,
  }) async {
    return _downloadsApi.download(
      'api' / 'v1' / 'books' / id / 'pages' / page.toString() / 'thumbnail',
    );
  }

  /// Runs a `get` request to the `/api/v1/books` endpoint
  Future<PageBookDto> getBooks({
    String? search,
    List<String>? libraryIds,
    List<MediaStatus>? mediaStatus,
    List<ReadStatus>? readStatus,
    DateTime? releasedAfter,
    List<String>? tags,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? sort,
  }) async {
    final uri = 'api' / 'v1' / 'books' &
        {
          if (search?.isNotEmpty ?? false)
            'search': Uri.encodeQueryComponent(search!),
          if (libraryIds?.isNotEmpty ?? false) 'library_id': libraryIds,
          if (mediaStatus != null)
            'media_status': mediaStatus.map((e) => e.value).toList(),
          if (readStatus != null)
            'read_status': readStatus.map((e) => e.value).toList(),
          if (releasedAfter != null)
            'released_after': apiDateFormatter.format(releasedAfter),
          if (tags != null) 'tags': tags,
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (size != null) 'size': size.toString(),
          if (sort != null) 'sort': sort,
        };

    return _api.get(
      uri,
      dtoParser: PaginationDto.pageBookFromJson,
    );
  }

  /// Runs a `get` request to the `/api/v1/books/ondeck` endpoint
  Future<PageBookDto> getBooksOnDeck({
    String? libraryId,
    int? page,
    int? pageSize,
  }) async {
    final uri = 'api' / 'v1' / 'books' / 'ondeck' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (page != null) 'page': page.toString(),
          if (pageSize != null) 'size': pageSize.toString(),
        };
    return _api.get(uri, dtoParser: PaginationDto.pageBookFromJson);
  }

  /// Runs a `get` request to the `/api/v1/books/{{id}}/next` endpoint
  Future<BookDto?> getNextBookById(String id) async {
    final uri = 'api' / 'v1' / 'books' / id / 'next';

    try {
      final res = await _api.get(uri, dtoParser: BookDto.fromJson);
      return res;
    } on NotFoundApiException {
      return null;
    } catch (e) {
      rethrow;
    }
  }

  /// Runs a `get` request to the `/api/v1/books/{{id}}/previous` endpoint
  Future<BookDto?> getPreviousBookById(String id) async {
    final uri = 'api' / 'v1' / 'books' / id / 'previous';

    try {
      final res = await _api.get(uri, dtoParser: BookDto.fromJson);
      return res;
    } on NotFoundApiException {
      return null;
    } catch (e) {
      rethrow;
    }
  }

  /// Runs a `patch` request to the `/api/v1/books/{{id}}/read-progress`
  /// endpoint
  Future<void> patchBookProgressById(
    String id, {
    int? page,
    bool? completed,
  }) async {
    final uri = 'api' / 'v1' / 'books' / id / 'read-progress';

    return _api.patch<void, void>(
      uri,
      body: <String, dynamic>{
        if (completed != null) 'completed': completed.toString(),
        if (page != null) 'page': page.toString(),
      },
    );
  }

  /// Runs a `post` request to the `/api/v1/books/{{id}}/analyze`
  /// endpoint
  Future<void> postAnalyzeBookById(String id) async {
    final uri = 'api' / 'v1' / 'books' / id / 'analyze';
    await _api.post<void, void>(uri);
  }

  /// Runs a `post` request to the `/api/v1/books/{{id}}/metadata/refresh`
  /// endpoint
  Future<void> postRefreshBookById(String id) async {
    final uri = 'api' / 'v1' / 'books' / id / 'metadata' / 'refresh';
    await _api.post<void, void>(uri);
  }
}
