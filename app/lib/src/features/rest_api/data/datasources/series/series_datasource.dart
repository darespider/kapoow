import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';

/// Class used to do requests to the books endpoints
/// it consumes an [RestJsonApi] to do the requests
class SeriesDatasource {
  /// Creates instance of [SeriesDatasource]
  const SeriesDatasource(this._api);

  final RestJsonApi _api;

  /// Runs a `get` request to the `/api/v1/series/{{id}}/books` endpoint
  Future<PageBookDto> getSeriesBooks({
    required String seriesId,
    MediaStatus? mediaStatus,
    ReadStatus? readStatus,
    List<String>? tags,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
    List<String>? sort,
    List<String>? authors,
  }) async {
    final uri = 'api' / 'v1' / 'series' / seriesId / 'books' &
        {
          if (mediaStatus != null) 'media_status': mediaStatus.value,
          if (readStatus != null) 'read_status': readStatus.value,
          if (tags != null) 'tag': tags,
          if (deleted != null) 'deleted': deleted.toString(),
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (pageSize != null) 'size': pageSize.toString(),
          if (sort != null) 'sort': sort,
          if (authors != null) 'author': authors,
        };
    return _api.get(uri, dtoParser: PaginationDto.pageBookFromJson);
  }

  /// Runs a `get` request to the `/api/v1/series/latest` endpoint
  Future<PageSeriesDto> getLatestSeries({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) async {
    final uri = 'api' / 'v1' / 'series' / 'latest' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (deleted != null) 'deleted': deleted.toString(),
          if (oneshot != null) 'oneshot': oneshot.toString(),
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (pageSize != null) 'size': pageSize.toString(),
        };
    return _api.get(uri, dtoParser: PaginationDto.pageSeriesFromJson);
  }

  /// Runs a `get` request to the `/api/v1/series/new` endpoint
  Future<PageSeriesDto> getNewSeries({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) async {
    final uri = 'api' / 'v1' / 'series' / 'new' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (libraryId != null) 'deleted': deleted.toString(),
          if (oneshot != null) 'oneshot': oneshot.toString(),
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (pageSize != null) 'size': pageSize.toString(),
        };
    return _api.get(uri, dtoParser: PaginationDto.pageSeriesFromJson);
  }

  /// Runs a `get` request to the `/api/v1/series/updated` endpoint
  Future<PageSeriesDto> getUpdatedSeries({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) async {
    final uri = 'api' / 'v1' / 'series' / 'updated' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (libraryId != null) 'deleted': deleted.toString(),
          if (oneshot != null) 'oneshot': oneshot.toString(),
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (pageSize != null) 'size': pageSize.toString(),
        };
    return _api.get(uri, dtoParser: PaginationDto.pageSeriesFromJson);
  }

  /// Runs a `get` request to the `/api/v1/series` endpoint
  Future<PageSeriesDto> getSeries({
    String? search,
    List<String>? libraryIds,
    List<String>? collectionsIds,
    List<SeriesStatus>? seriesStatuses,
    List<ReadStatus>? readStatus,
    List<String>? publishers,
    List<String>? languages,
    List<String>? genres,
    List<String>? tags,
    List<String>? ageRating,
    List<DateTime>? releaseYears,
    bool? deleted,
    bool? completed,
    bool? unpaged,
    String? searchRegex,
    int? page,
    int? size,
    List<String>? sort,
    List<String>? authors,
    bool? oneshot,
  }) async {
    final uri = 'api' / 'v1' / 'series' &
        {
          if (search?.isNotEmpty ?? false)
            'search': Uri.encodeQueryComponent(search!),
          if (libraryIds?.isNotEmpty ?? false) 'library_id': libraryIds,
          if (collectionsIds?.isNotEmpty ?? false)
            'collection_id': collectionsIds,
          if (seriesStatuses?.isNotEmpty ?? false)
            'status': seriesStatuses!.map((status) => status.value).toList(),
          if (readStatus?.isNotEmpty ?? false)
            'read_status': readStatus!.map((status) => status.value).toList(),
          if (publishers?.isNotEmpty ?? false) 'publisher': publishers,
          if (languages?.isNotEmpty ?? false) 'language': languages,
          if (genres?.isNotEmpty ?? false) 'genre': genres,
          if (tags?.isNotEmpty ?? false) 'tag': tags,
          if (ageRating?.isNotEmpty ?? false) 'age_rating': ageRating,
          if (releaseYears?.isNotEmpty ?? false)
            'release_year':
                releaseYears!.map((date) => date.year.toString()).toList(),
          if (deleted != null) 'deleted': deleted.toString(),
          if (oneshot != null) 'oneshot': oneshot.toString(),
          if (completed != null) 'completed': completed.toString(),
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (searchRegex?.isNotEmpty ?? false) 'search_regex': searchRegex,
          if (page != null) 'page': page.toString(),
          if (size != null) 'size': size.toString(),
          if (sort?.isNotEmpty ?? false) 'sort': sort,
          if (authors?.isNotEmpty ?? false) 'author': authors,
        };
    return _api.get(uri, dtoParser: PaginationDto.pageSeriesFromJson);
  }

  /// Runs a `get` request to the `/api/v1/series/{{id}}` endpoint
  Future<SeriesDto> getSeriesById(String id) async {
    final uri = 'api' / 'v1' / 'series' / id;
    return _api.get(uri, dtoParser: SeriesDto.fromJson);
  }
}
