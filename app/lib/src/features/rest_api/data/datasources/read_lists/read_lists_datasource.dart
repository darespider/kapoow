import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_update_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/api_exceptions.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';

/// ReadLists Api Client that return api data
class ReadListsDatasource {
  /// Creates instance of [ReadListsDatasource]
  const ReadListsDatasource(this._api);

  final RestJsonApi _api;

  /// Runs a `get` request to the `/api/v1/readlists` endpoint
  Future<PageReadListDto> getReadLists({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  }) async {
    final uri = 'api' / 'v1' / 'readlists' &
        {
          if (search?.isNotEmpty ?? false)
            'search': Uri.encodeQueryComponent(search!),
          if (libraryIds?.isNotEmpty ?? false) 'library_id': libraryIds,
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (size != null) 'size': size.toString(),
        };
    return _api.get(uri, dtoParser: PaginationDto.pageReadListFromJson);
  }

  /// Runs a `get` request to the `/api/v1/readlists/{{id}}` endpoint
  Future<ReadListDto> getReadListById(String id) async {
    final uri = 'api' / 'v1' / 'readlists' / id;
    return _api.get(uri, dtoParser: ReadListDto.fromJson);
  }

  /// Runs a `get` request to the `/api/v1/readlists/{{id}}/books` endpoint
  Future<PageBookDto> getReadListBooks({
    required String readlistId,
    List<String>? libraryIds,
    List<ReadStatus>? readStatus,
    List<String>? tags,
    List<MediaStatus>? mediaStatus,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  }) async {
    final uri = 'api' / 'v1' / 'readlists' / readlistId / 'books' &
        {
          if (libraryIds?.isNotEmpty ?? false) 'library_id': libraryIds,
          if (mediaStatus != null)
            'media_status': mediaStatus.map((e) => e.value).toList(),
          if (readStatus != null)
            'read_status': readStatus.map((e) => e.value).toList(),
          if (tags != null) 'tag': tags,
          if (deleted != null) 'deleted': deleted.toString(),
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (size != null) 'size': size.toString(),
          if (authors != null) 'author': authors,
        };
    return _api.get(uri, dtoParser: PaginationDto.pageBookFromJson);
  }

  /// Runs a `get` request to the
  /// `/api/v1/readlists/{{id}}/books/{{bookId}}/next` endpoint
  Future<BookDto?> getReadListNextBook(String id, String bookId) async {
    final uri = 'api' / 'v1' / 'readlists' / id / 'books' / bookId / 'next';

    try {
      final result = await _api.get(uri, dtoParser: BookDto.fromJson);
      return result;
    } on NotFoundApiException {
      return null;
    } catch (e) {
      rethrow;
    }
  }

  /// Runs a `get` request to the
  /// `/api/v1/readlists/{{id}}/books/{{bookId}}/previous` endpoint
  Future<BookDto?> getReadListPreviousBook(String id, String bookId) async {
    final uri = 'api' / 'v1' / 'readlists' / id / 'books' / bookId / 'previous';

    try {
      final result = await _api.get(uri, dtoParser: BookDto.fromJson);
      return result;
    } on NotFoundApiException {
      return null;
    } catch (e) {
      rethrow;
    }
  }

  /// Runs a `patch` request to the
  /// `/api/v1/readlists/{{id}}` endpoint
  Future<void> patchReadList(String id, ReadListUpdateDto patchData) async {
    await _api.patch<void, void>(
      'api' / 'v1' / 'readlists' / id,
      body: patchData.toJson(),
    );
  }
}
