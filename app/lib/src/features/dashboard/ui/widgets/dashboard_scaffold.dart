import 'package:flutter/material.dart';
import 'package:kapoow/src/routes/dashboard_routes_data.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Scaffold with a search button and bottom navigation bar that goes to
/// RecommendedRoute
/// LibrariesRoute
/// SettingsRoute
class DashboardScaffold extends StatelessWidget {
  /// Creates a [DashboardScaffold] instance
  const DashboardScaffold({
    required this.body,
    this.bottomBarIndex = 0,
    this.safeAreaTop = true,
    super.key,
  });

  /// Body to display
  final Widget body;

  /// index to show as enable in the bottom nav bar
  ///
  /// 0 -> recommended
  ///
  /// 1 -> libraries
  ///
  /// 2 -> settings
  ///
  /// Any other value will redirect to 0
  final int bottomBarIndex;

  /// If true then there would be a safe area with top enable
  final bool safeAreaTop;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: safeAreaTop,
        child: body,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () => const SearchRoute().go(context),
        child: PhosphorIcon(
          PhosphorIcons.magnifyingGlass(PhosphorIconsStyle.duotone),
        ),
      ),
      bottomNavigationBar: NavigationBar(
        selectedIndex: bottomBarIndex,
        onDestinationSelected: (value) => switch (value) {
          0 => const RecommendedRoute().go(context),
          1 => const LibraryRoute(libraryId: LibraryRoute.allLibrariesId)
              .go(context),
          2 => const SettingsRoute().go(context),
          _ => null,
        },
        labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
        destinations: [
          NavigationDestination(
            icon: PhosphorIcon(
              PhosphorIcons.layout(PhosphorIconsStyle.duotone),
            ),
            selectedIcon: PhosphorIcon(
              PhosphorIcons.layout(PhosphorIconsStyle.fill),
            ),
            // TODO(rurickdev): [l10n] translate
            label: 'Recommended',
          ),
          NavigationDestination(
            icon: PhosphorIcon(
              PhosphorIcons.books(PhosphorIconsStyle.duotone),
            ),
            selectedIcon: PhosphorIcon(
              PhosphorIcons.books(PhosphorIconsStyle.fill),
            ),
            // TODO(rurickdev): [l10n] translate
            label: 'Library',
          ),
          NavigationDestination(
            icon: PhosphorIcon(
              PhosphorIcons.gear(PhosphorIconsStyle.duotone),
            ),
            selectedIcon: PhosphorIcon(
              PhosphorIcons.gear(PhosphorIconsStyle.fill),
            ),
            // TODO(rurickdev): [l10n] translate
            label: 'Settings',
          ),
          // required to prevent overlap with FAB
          const SizedBox(),
        ],
      ),
    );
  }
}
