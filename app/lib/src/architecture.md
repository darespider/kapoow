# Kapoow! Architecture

## Domain Features

- Session
    - Server
    - User
- Read Book
- Change Password
- Rest Api
    - Rest Json Api
    - Dtos
- Library Management
    - Pagination
    - Collections
    - Books
    - Series
    - Read Lists
    - Libraries
