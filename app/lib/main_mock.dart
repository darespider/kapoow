// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/books/books_datasource_mock.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/collections/collections_datasource_mock.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/read_lists/read_lists_datasource_mock.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/referential/referential_datasource_mock.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/series/series_datasource_mock.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/user/user_datasource_mock.dart';
import 'package:kapoow/src/kapoow_app.dart';
import 'package:kapoow/src/utils/logging.dart';
import 'package:logging/logging.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  setupGlobalLogging(Level.FINER);

  // Prevents UncontrolledProviderScope to throw at first frame
  // FlutterError.demangleStackTrace = (StackTrace stack) {
  //   if (stack is stack_trace.Trace) return stack.vmTrace;
  //   if (stack is stack_trace.Chain) return stack.toTrace().vmTrace;
  //   return stack;
  // };

  runApp(
    ProviderScope(
      overrides: [
        // Datasource
        booksDatasourcePod.overrideWith((_) => BooksDatasourceMock()),
        collectionsDatasourcePod
            .overrideWith((_) => CollectionsDatasourceMock()),
        readListsDatasourcePod.overrideWith((_) => ReadListsDatasourceMock()),
        referentialDatasourcePod
            .overrideWith((ref) => ReferentialDatasourceMock()),
        seriesDatasourcePod.overrideWith((_) => SeriesDatasourceMock()),
        userDatasourcePod.overrideWith((_) => UserDatasourceMock()),
      ],
      child: const KapoowApp(),
    ),
  );
}
