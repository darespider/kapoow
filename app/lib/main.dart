// coverage:ignore-file
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/kapoow_app.dart';
import 'package:kapoow/src/utils/logging.dart';
import 'package:logging/logging.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  setupGlobalLogging(kReleaseMode ? Level.OFF : Level.FINE);

  runApp(
    const ProviderScope(
      child: KapoowApp(),
    ),
  );
}
