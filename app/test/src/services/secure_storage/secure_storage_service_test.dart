// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kapoow/src/services/secure_storage/secure_storage_service.dart';
import 'package:mocktail/mocktail.dart';

class MockFlutterSecureStorage extends Mock implements FlutterSecureStorage {}

void main() {
  late FlutterSecureStorage secureStorage;
  late SecureStorageService service;

  setUp(() {
    secureStorage = MockFlutterSecureStorage();
    service = SecureStorageService(secureStorage);

    when(
      () => secureStorage.write(
        key: any(named: 'key'),
        value: any(named: 'value'),
      ),
    ).thenAnswer((_) async {});
    when(
      () => secureStorage.delete(
        key: any(named: 'key'),
      ),
    ).thenAnswer((_) async {});
  });

  group(
    'saveValue',
    () {
      test(
        'Should call the [secureStorage.write]',
        () async {
          await service.saveValue('key', 'value');

          final verified = verify(
            () => secureStorage.write(
              key: captureAny(named: 'key'),
              value: captureAny(named: 'value'),
            ),
          )..called(1);
          expect(verified.captured, ['key', 'value']);
        },
      );
    },
  );

  group(
    'deleteValue',
    () {
      test(
        'Should call the [secureStorage.delete]',
        () async {
          await service.deleteValue('key');

          final verified = verify(
            () => secureStorage.delete(
              key: captureAny(named: 'key'),
            ),
          )..called(1);
          expect(verified.captured, ['key']);
        },
      );
    },
  );

  group(
    'getValue',
    () {
      test(
        'Should call the [secureStorage.read] and return a value when exists',
        () async {
          when(
            () => secureStorage.read(
              key: any(named: 'key'),
            ),
          ).thenAnswer((_) async {
            return 'value';
          });

          final result = await service.getValue('key');

          expect(result, 'value');

          final verified = verify(
            () => secureStorage.read(
              key: captureAny(named: 'key'),
            ),
          )..called(1);

          expect(verified.captured, ['key']);
        },
      );
      test(
        'Should call the [secureStorage.read] and return null when value not '
        'exists',
        () async {
          when(
            () => secureStorage.read(
              key: any(named: 'key'),
            ),
          ).thenAnswer((_) async {
            return null;
          });

          final result = await service.getValue('key');

          expect(result, isNull);

          final verified = verify(
            () => secureStorage.read(
              key: captureAny(named: 'key'),
            ),
          )..called(1);

          expect(verified.captured, ['key']);
        },
      );
    },
  );
}
