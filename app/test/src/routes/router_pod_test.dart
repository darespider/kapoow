// ignore_for_file: lines_longer_than_80_chars
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:kapoow/gen/l10n.gen.dart';
import 'package:kapoow/src/features/app_init/ui/controllers/init_pods.dart';
import 'package:kapoow/src/features/app_init/ui/controllers/init_state.dart';
import 'package:kapoow/src/features/app_init/ui/screens/splash_screen.dart';
import 'package:kapoow/src/features/dashboard/ui/widgets/dashboard_scaffold.dart';
import 'package:kapoow/src/features/rest_api/domain/pods.dart';
import 'package:kapoow/src/features/session/domain/models/session_data.dart';
import 'package:kapoow/src/features/session/ui/screens/input_server_form.dart';
import 'package:kapoow/src/features/session/ui/screens/login_screen_form.dart';
import 'package:kapoow/src/features/session/ui/widgets/onboarding_scaffold.dart';
import 'package:kapoow/src/routes/router_pod.dart';

class FakeHttpClient extends Fake implements Client {}

void main() {
  group(
    'Router',
    () {
      testWidgets(
        'Should show splash screen when initPod is loading',
        (tester) async {
          final initState = Completer<InitState>();
          await _pumpRouterApp(
            tester,
            [
              initPod.overrideWith((ref) => initState.future),
            ],
          );
          final splash = find.byType(SplashScreen);
          expect(splash, findsOneWidget);

          // Required due some timer inside the Splash Screen Animation
          await tester.pump(const Duration(microseconds: 1));
        },
      );
      testWidgets(
        'Should redirect to error init screen when initPod fails',
        (tester) async {
          await _pumpRouterApp(
            tester,
            [
              initPod.overrideWith((ref) => throw Exception()),
            ],
          );
          // TODO(rurickdev): rework the test to find the real init error screen
          final errorScreen = find.text('Error while Kapoow initialize');
          expect(errorScreen, findsOneWidget);
        },
      );
      testWidgets(
        'Should redirect to server login form screen when initPod returns '
        'InitStateNoSession',
        (tester) async {
          await _pumpRouterApp(
            tester,
            [
              initPod.overrideWith((ref) async => const InitStateNoSession()),
            ],
          );
          final splash = find.byType(SplashScreen);
          expect(splash, findsOneWidget);
          await tester.pumpAndSettle();
          final scaffold = find.byType(OnboardingScaffold);
          expect(scaffold, findsOneWidget);
          final serverForm = find.byType(InputServerScreen);
          expect(serverForm, findsOneWidget);
          final loginForm = find.byType(LoginScreen);
          expect(loginForm, findsNothing);
        },
      );
      testWidgets(
        'Should redirect to user login form screen when initPod returns '
        'InitStateInvalid',
        (tester) async {
          await _pumpRouterApp(
            tester,
            [
              initPod.overrideWith((ref) async => const InitStateInvalid()),
            ],
          );
          final splash = find.byType(SplashScreen);
          expect(splash, findsOneWidget);
          await tester.pumpAndSettle();
          final scaffold = find.byType(OnboardingScaffold);
          expect(scaffold, findsOneWidget);
          final serverForm = find.byType(InputServerScreen);
          expect(serverForm, findsNothing);
          final loginForm = find.byType(LoginScreen);
          expect(loginForm, findsOneWidget);
        },
      );
      testWidgets(
        'Should redirect to home screen when initPod returns '
        'InitStateSession',
        (tester) async {
          await _pumpRouterApp(
            tester,
            [
              initPod.overrideWith(
                (ref) async => const InitStateSession(
                  SessionData(token: '132', host: '123'),
                ),
              ),
            ],
          );
          final splash = find.byType(SplashScreen);
          expect(splash, findsOneWidget);
          await tester.pumpAndSettle();

          final dashboard = find.byType(DashboardScaffold);
          expect(dashboard, findsOneWidget);
        },
      );
    },
  );
}

Future<void> _pumpRouterApp(
  WidgetTester tester,
  List<Override> overrides,
) async {
  await tester.pumpWidget(
    ProviderScope(
      overrides: [
        httpClientPod.overrideWithValue(FakeHttpClient()),
        ...overrides,
      ],
      child: Consumer(
        builder: (context, ref, child) => MaterialApp.router(
          routerConfig: ref.watch(goRouterPod),
          localizationsDelegates: Translations.localizationsDelegates,
          supportedLocales: Translations.supportedLocales,
        ),
      ),
    ),
  );
}
