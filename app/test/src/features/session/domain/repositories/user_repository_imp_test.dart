// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/user/user_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';
import 'package:kapoow/src/features/session/domain/models/user.dart';
import 'package:kapoow/src/features/session/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/session/domain/repositories/user_repository_imp.dart';
import 'package:mocktail/mocktail.dart';

class MockUserDatasource extends Mock implements UserDatasource {}

const userDto = UserDto(
  id: '0AE5FYZ03QVA5',
  email: 'email@example.com',
  roles: {
    'USER',
    'ADMIN',
    'FILE_DOWNLOAD',
    'PAGE_STREAMING',
  },
  sharedAllLibraries: true,
  sharedLibrariesIds: {},
  labelsAllow: {},
  labelsExclude: {},
);

void main() {
  late UserDatasource datasource;
  late UserRepositoryImp repository;

  setUp(() {
    datasource = MockUserDatasource();
    repository = UserRepositoryImp(datasource);
  });

  group(
    'getMeData',
    () {
      test(
        'Should call [UserDatasource.getUserMe] once and convert the dto to '
        'a domain object',
        () async {
          when(() => datasource.getUserMe()).thenAnswer((_) async => userDto);

          final user = await repository.getMeData();

          expect(user, isA<User>());
          expect(user, userDto.toDomain());

          verify(() => datasource.getUserMe()).called(1);
        },
      );
    },
  );

  group(
    'getUserData',
    () {
      test(
        'Should call [UserDatasource.getUser] once and convert the dto to '
        'a domain object',
        () async {
          // TODO(rurickdev): uncomment when datasource has the method
          // when(() => datasource.getUser(any()))
          //     .thenAnswer((_) async => userDto);

          final user = await repository.getUserData('1');

          expect(user, isA<User>());
          expect(user, userDto.toDomain());

          // final verified = verify(() => datasource.getUser())..called(1);
          // expect(verified.captured, ['1']);
        },
      );
    },
    skip: "Skip because the datasource doesn't have [getUser] method",
  );

  group(
    'getUsersData',
    () {
      test(
        'Should call [UserDatasource.getUsers] once and convert the dtos to '
        'a domain objects',
        () async {
          // TODO(rurickdev): uncomment when datasource has the method
          // when(() => datasource.getUsers())
          //     .thenAnswer((_) async => [userDto]);

          final users = await repository.getUsersData();

          expect(users, isA<List<User>>());
          expect(users, [userDto.toDomain()]);

          // final verified = verify(() => datasource.getUsers())..called(1);
          // expect(verified.captured, ['1']);
        },
      );
    },
    skip: "Skip because the datasource doesn't have [getUsers] method",
  );
}
