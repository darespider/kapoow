// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:kapoow/src/features/session/data/daos/secure_storage_session_dao.dart';
import 'package:kapoow/src/features/session/data/daos/session_dao_exceptions.dart';
import 'package:kapoow/src/services/secure_storage/secure_storage_service.dart';
import 'package:mocktail/mocktail.dart';

class MockSecureStorageService extends Mock implements SecureStorageService {}

void main() {
  late SecureStorageService storageService;
  late SecureStorageSessionDao dao;

  setUp(() {
    storageService = MockSecureStorageService();
    dao = SecureStorageSessionDao(storageService);

    when(() => storageService.saveValue(any(), any())).thenAnswer((_) async {});
    when(() => storageService.deleteValue(any())).thenAnswer((_) async {});
  });

  group(
    'save',
    () {
      test(
        'Should call the [storageService.saveValue] twice using the right keys',
        () async {
          await dao.save(
            host: 'host',
            token: 'token',
          );

          verify(
            () => storageService.saveValue(
              SecureStorageSessionDao.hostKey,
              'host',
            ),
          ).called(1);

          verify(
            () => storageService.saveValue(
              SecureStorageSessionDao.tokenKey,
              'token',
            ),
          ).called(1);
        },
      );
    },
  );

  group(
    'delete',
    () {
      test(
        'Should call the [storageService.delete] twice using the right keys',
        () async {
          await dao.delete();

          verify(
            () => storageService.deleteValue(SecureStorageSessionDao.tokenKey),
          ).called(1);

          verify(
            () => storageService.deleteValue(SecureStorageSessionDao.hostKey),
          ).called(1);
        },
      );
    },
  );

  group(
    'load',
    () {
      test(
        'Should call the [storageService.getValue] twice using the right keys '
        'and return the token and host',
        () async {
          when(() => storageService.getValue(SecureStorageSessionDao.hostKey))
              .thenAnswer((_) async => 'host');
          when(() => storageService.getValue(SecureStorageSessionDao.tokenKey))
              .thenAnswer((_) async => 'token');

          final (host, token) = await dao.load();

          expect(host, 'host');
          expect(token, 'token');

          verify(
            () => storageService.getValue(SecureStorageSessionDao.tokenKey),
          ).called(1);

          verify(
            () => storageService.getValue(SecureStorageSessionDao.hostKey),
          ).called(1);
        },
      );
      test(
        "Should return nulls when storage don't have values",
        () async {
          when(() => storageService.getValue(SecureStorageSessionDao.hostKey))
              .thenAnswer((_) async => null);
          when(() => storageService.getValue(SecureStorageSessionDao.tokenKey))
              .thenAnswer((_) async => null);

          final (host, token) = await dao.load();

          expect(host, isNull);
          expect(token, isNull);

          verify(
            () => storageService.getValue(SecureStorageSessionDao.tokenKey),
          ).called(1);

          verify(
            () => storageService.getValue(SecureStorageSessionDao.hostKey),
          ).called(1);
        },
      );
      test(
        'Should trow [RetrievingTokenException] when fails to get token',
        () async {
          when(() => storageService.getValue(SecureStorageSessionDao.tokenKey))
              .thenThrow(Exception());

          await expectLater(
            dao.load(),
            throwsA(isA<RetrievingTokenException>()),
          );

          verify(
            () => storageService.getValue(SecureStorageSessionDao.tokenKey),
          ).called(1);

          verifyNever(
            () => storageService.getValue(SecureStorageSessionDao.hostKey),
          );
        },
      );
      test(
        'Should trow [RetrievingHostException] when fails to get host',
        () async {
          when(() => storageService.getValue(SecureStorageSessionDao.tokenKey))
              .thenAnswer((_) async => 'token');
          when(() => storageService.getValue(SecureStorageSessionDao.hostKey))
              .thenThrow(Exception());

          await expectLater(
            dao.load(),
            throwsA(isA<RetrievingHostException>()),
          );

          verify(
            () => storageService.getValue(SecureStorageSessionDao.tokenKey),
          ).called(1);

          verify(
            () => storageService.getValue(SecureStorageSessionDao.hostKey),
          ).called(1);
        },
      );
    },
  );
}
