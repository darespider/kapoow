// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/rest_api/data/datasources/user/user_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../../../fixtures/mocks.dart';
import '../../../../../../fixtures/utils.dart';
import '../../../fixtures/users_dtos.dart';

void main() {
  setUpAll(() {
    registerFallbackValue(Uri());
  });

  late http.Client client;
  late UserDatasource datasource;

  setUp(() {
    client = MockClient();
    datasource = UserDatasource(
      RestJsonApi(
        RestApi(
          client: client,
          apiUrl: Uri(),
          headers: commonHeaders,
        ),
      ),
    );
  });

  group(
    'getUserMe',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v2/users/me.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getUserMe();

        expect(
          client.capturedGet(),
          [
            Uri.parse('api/v2/users/me'),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getUserMe();

        expect(response, isA<UserDto>());
        expect(response, meDto);
      });
    },
  );
}
