// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/rest_api/data/datasources/referential/referential_datasource.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../../../fixtures/mocks.dart';
import '../../../../../../fixtures/utils.dart';
import '../../../fixtures/referential_dtos.dart';

void main() {
  setUpAll(() {
    registerFallbackValue(Uri());
  });

  late http.Client client;
  late ReferentialDatasource datasource;

  setUp(() {
    client = MockClient();
    datasource = ReferentialDatasource(
      RestJsonApi(
        RestApi(
          client: client,
          apiUrl: Uri(),
          headers: commonHeaders,
        ),
      ),
    );
  });

  group(
    'getAgeRatings',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/age-ratings.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getAgeRatings(
          collectionId: '1',
          libraryId: '1',
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/age-ratings?library_id=1&collection_id=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getAgeRatings();

        expect(response, isA<List<String>>());
        expect(response, ageRatingsDto);
      });
    },
  );

  group(
    'getGenres',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/genres.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getGenres(
          collectionId: '1',
          libraryId: '1',
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/genres?library_id=1&collection_id=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getGenres();

        expect(response, isA<List<String>>());
        expect(response, genresDto);
      });
    },
  );

  group(
    'getLanguages',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/languages.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getLanguages(
          collectionId: '1',
          libraryId: '1',
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/languages?library_id=1&collection_id=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getLanguages();

        expect(response, isA<List<String>>());
        expect(response, languagesDto);
      });
    },
  );

  group(
    'getPublishers',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/publishers.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getPublishers(
          collectionId: '1',
          libraryId: '1',
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/publishers?library_id=1&collection_id=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getPublishers();

        expect(response, isA<List<String>>());
        expect(response, publishersDto);
      });
    },
  );
}
