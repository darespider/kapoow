// ignore_for_file: lines_longer_than_80_chars
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/read_lists/read_lists_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_update_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../../../fixtures/mocks.dart';
import '../../../../../../fixtures/utils.dart';
import '../../../fixtures/read_lists_dtos.dart';

void main() {
  setUpAll(() {
    registerFallbackValue(Uri());
  });

  late http.Client client;
  late ReadListsDatasource datasource;

  setUp(() {
    client = MockClient();
    datasource = ReadListsDatasource(
      RestJsonApi(
        RestApi(
          client: client,
          apiUrl: Uri(),
          headers: commonHeaders,
        ),
      ),
    );
  });

  group(
    'getReadLists',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/readlists.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getReadLists(
          libraryIds: ['1'],
          page: 1,
          search: 'search',
          size: 1,
          unpaged: false,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/readlists?search=search&library_id=1&unpaged=false&page=1&size=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getReadLists();

        expect(response, isA<PageReadListDto>());
        expect(response, pageReadListDto);
      });
    },
  );

  group(
    'getReadListById',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/readlists/id.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getReadListById('1');

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/readlists/1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getReadListById('1');

        expect(response, isA<ReadListDto>());
        expect(response, readListDto);
      });
    },
  );

  group(
    'getReadListBooks',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/readlists/id/books.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getReadListBooks(
          readlistId: '1',
          authors: ['1'],
          deleted: false,
          libraryIds: ['1'],
          mediaStatus: [MediaStatus.ready],
          page: 1,
          readStatus: [ReadStatus.unread],
          size: 1,
          tags: ['tag'],
          unpaged: true,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/readlists/1/books?library_id=1&media_status=READY&read_status=UNREAD&tag=tag&deleted=false&unpaged=true&page=1&size=1&author=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getReadListBooks(
          readlistId: '1',
          authors: ['1'],
          deleted: false,
          libraryIds: ['1'],
          mediaStatus: [MediaStatus.ready],
          page: 1,
          readStatus: [ReadStatus.unread],
          size: 1,
          tags: ['tag'],
          unpaged: true,
        );

        expect(response, isA<PageBookDto>());
        expect(response, readListsBooks);
      });
    },
  );

  group(
    'getReadListNextBook',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/readlists/id/books/id/next.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getReadListNextBook('1', '1');

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/readlists/1/books/1/next',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getReadListNextBook('1', '1');

        expect(response, isA<BookDto>());
        expect(response, nextBookDto);
      });

      test('Should return null if NotFoundApiApiException is throw', () async {
        client.mockGet('', HttpStatus.notFound);

        final response = await datasource.getReadListNextBook('1', '1');

        expect(response, isNull);
      });
    },
  );

  group(
    'getReadListPreviousBook',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/readlists/id/books/id/previous.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getReadListPreviousBook('1', '1');

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/readlists/1/books/1/previous',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getReadListPreviousBook('1', '1');

        expect(response, isA<BookDto>());
        expect(response, previousBookDto);
      });

      test('Should return null if NotFoundApiApiException is throw', () async {
        client.mockGet('', HttpStatus.notFound);

        final response = await datasource.getReadListPreviousBook('1', '1');

        expect(response, isNull);
      });
    },
  );

  group(
    'patchReadList',
    () {
      setUp(() {
        client.mockPatch();
      });

      test('Should make the right request', () async {
        await datasource.patchReadList(
          '1',
          const ReadListUpdateDto(
            bookIds: ['1', '2', '3'],
            name: 'read list name',
            summary: 'long summary',
          ),
        );

        expect(
          client.capturedPatch(),
          [
            Uri.parse('api/v1/readlists/1'),
            {'content-type': 'application/json; charset=utf-8'},
            '{"name":"read list name","summary":"long summary","bookIds":["1","2","3"]}',
          ],
        );
      });
    },
  );
}
