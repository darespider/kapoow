// ignore_for_file: lines_longer_than_80_chars
import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';

const meDto = UserDto(
  id: '0AE5FYZ03QVA5',
  email: 'email@example.com',
  roles: {
    'USER',
    'ADMIN',
    'FILE_DOWNLOAD',
    'PAGE_STREAMING',
  },
  sharedAllLibraries: true,
  sharedLibrariesIds: {},
  labelsAllow: {},
  labelsExclude: {},
);
