import 'dart:io';

import 'package:path/path.dart' as path;

String readJsonFixture(String file) =>
    File(path.join('test', 'fixtures', file)).readAsStringSync();

final commonHeaders = {
  HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8',
};
